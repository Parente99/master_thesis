#!/usr/bin/python3.9

# entao vamos fazer um pseudocodigo
# dar load do fichiro de som

import numpy as np
from scipy.signal import butter,filtfilt # Filter requirements.
import librosa as lb
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics import tsaplots

sound_path = '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav'
#sound_path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/real_beats_5s.wav'

# here i load the audio and it turns in to a list
audio, sr = lb.load(sound_path, sr = None)

len_5s = sr * 5
audio = audio [:len_5s]

len_audio = len(audio)
#print(len_audio)

fs = sr       # sample rate, Hz
cutoff = 200      # desired cutoff frequency of the filter, Hz
nyq = 0.5 * fs # Nyquist Frequency
order = 2 # sin wave can be approx represented as quadratic
n = len_audio # total number of samples

def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

filterd_audio = butter_lowpass_filter(audio, cutoff, fs, order)
'''
plt.plot(audio)
plt.plot(filterd_audio)
plt.show()
plt.clf()
'''
plt.subplot(211)
powerSpectrum, freqenciesFound, time, imageAxis = plt.specgram(audio, Fs=fs)
plt.xlabel('Time')
plt.ylabel('Frequency')


plt.subplot(212)
plt.specgram(filterd_audio, Fs=fs)
plt.xlabel('Time')
plt.ylabel('Frequency filtred')
plt.show()
plt.clf()

x = np.arange(0, len(powerSpectrum[40,:]), 1)

plt.plot(x, powerSpectrum[30,:])
plt.show()

print(powerSpectrum)
print(np.shape(powerSpectrum))
print()
print(freqenciesFound)
print()

'''
lags = sm.tsa.acf(audio, nlags=len_audio)
filterd_lags = sm.tsa.acf(filterd_audio, nlags=len_audio)
plt.plot(lags)
plt.plot(filterd_lags)
plt.show()
'''
#!/usr/bin/python3.9

# Objetivos quero por um ficheiro de som e ter como output o bpm
# e so para provar que funciona

import librosa as lb
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics import tsaplots

sound_path = '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav'
#sound_path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/real_beats_5s.wav'

# here i load the audio and it turns in to a list
audio, sr = lb.load(sound_path, sr = None)
len_5s = sr * 5
audio = audio [:len_5s]
len_audio = len(audio)
print(len_audio)
#len_audio = 2016 * 5
#plt.plot(audio)
#plt.show()

lags = sm.tsa.acf(audio, nlags=len_audio)
lags = lags[500:len_5s+500]
plt.plot(lags)
plt.title('correlaçao de atrasos')
plt.xlabel('n de frames de atraso')
plt.ylabel('valor da autocorrelaçao')
plt.savefig('autocorrelation_no_filter_5s.png')

#fig = tsaplots.plot_acf(audio) # otima ideia demora demasiado tempo
#plt.show()
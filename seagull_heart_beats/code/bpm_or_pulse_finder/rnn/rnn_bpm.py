#!/usr/bin/python3.9

# okay neste codigo tenho 20 inputs e vou treinar a rede para ver se consegue distinguir os bpm

import numpy as np
import seaborn as sb
import sklearn as sk
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import BatchNormalization
from keras.layers import Input

path = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/bpm_labled_mfcc_t_10000s_no_noise_no_average.csv'
full_data = np.loadtxt(path, delimiter=',')
print(full_data)
print(full_data.shape)

data = full_data[:, 0:20]
results = full_data[:, 20]
# nao gosto de fazer a divisao assim

number_inputs = data.shape[1]
number_outputs = 1
number_data = data.shape[0]

percent_split = 0.8
value_split = round(percent_split * number_data)

training_data = data[0:value_split, :]
validation_data = data[value_split:, :]
training_results = results[0:value_split]
validation_results = results[value_split:]
# estou a fazer assim a divisao para manter a ordem dos dados

# Entao a camada LSTM tem como input um array 3d entao tenho de fazer isto
training_data = np.expand_dims(training_data, 1)
validation_data = np.expand_dims(validation_data, 1)


print('shape of traning data, validation data, training results, validation results')
print(training_data.shape)
print(validation_data.shape)
print(training_results.shape)
print(validation_results.shape)

# isto cria o modelo #
model = Sequential()
#model.add(Input(20))
model.add(LSTM(128, input_dim=20))
model.add(BatchNormalization())
model.add(Dense(64, activation='relu'))
model.add(BatchNormalization())
model.add(Dense(1, activation='relu'))

model.compile(
    optimizer='adam', # adam SGD
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# isto treina o modelo
history = model.fit(
    x=training_data,
    y=training_results,
    validation_data=(validation_data, validation_results),
    batch_size=32,
    epochs=4,
    verbose=1
)

# agora vou correr os dados de validacao pelo modelo
predictions = model.predict(validation_data)
predictions = np.round(predictions)
print(predictions)
# isto e para passar o array predictions para 0 ou 1 dependendo do valor

# e vou criar a matriz
ConfusionMatrixValidation = confusion_matrix(
    validation_results,
    predictions
)
print(ConfusionMatrixValidation.shape[0])

# agora vou correr os dados de treino pelo modelo
del(predictions)
predictions = model.predict(training_data)
predictions = np.round(predictions)

ConfusionMatrixTraining = confusion_matrix(
    training_results,
    predictions
)

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


# confusion matrix validation
matrix_size = ConfusionMatrixValidation.shape[0]
conf_matr_temp = pd.DataFrame(ConfusionMatrixValidation, range(matrix_size), range(matrix_size))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp) # , annot=True, annot_kws={"size": 10}
plt.title('confusion matrix validation')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

# confusion matrix training
matrix_size = ConfusionMatrixTraining.shape[0]
conf_matr_temp = pd.DataFrame(ConfusionMatrixTraining, range(matrix_size), range(matrix_size))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp) # , annot=True, annot_kws={"size": 10}
plt.title('confusion matrix training')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

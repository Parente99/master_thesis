#!/usr/bin/python3.7

# este codigo vai treinar uma rede com os dados que sairem do cluster algorithm
# quantos são? nem sei depende agora vou fazer com 4 mas depois vou generalizar

import keras
import matplotlib.pyplot
import numpy
import sklearn
from sklearn.model_selection import train_test_split
import seaborn
import pandas

data_and_labels = numpy.loadtxt('/home/parente/Documents/tese/seagull/codes_output_data/labled_reduced_data_1.csv', delimiter=',') # aqui dou o upload dos dados todos
data   = data_and_labels[:,1:] # dados para um lado
labels = data_and_labels[:,0] # labels para o outro estes labels são numeros tenho de passar para one hot array
labels = keras.utils.to_categorical(labels)

number_data = data.shape[0]
number_dimensions = data.shape[1]
number_results = labels.shape[1]

print(data.shape)
print(labels.shape)

TrainingData, ValidationData, TrainingResults, ValidationResults = train_test_split( # divide duma forma aleatoria nos 4 arrays
    data,
    labels,
    test_size=0.2, # percentagem que vai para a validacao
    random_state=26
)


print('*************************************************************************************')
print(number_dimensions)
print(TrainingData.shape)
print(ValidationData.shape)
print(TrainingResults.shape)
print(ValidationResults.shape)

# isto cria o modelo
model = keras.Sequential()
model.add(keras.engine.Input(shape=number_dimensions))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.relu))
model.add(keras.layers.core.Dense(250))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.relu))
model.add(keras.layers.core.Dense(250))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.softmax))
model.add(keras.layers.core.Dense(number_results))

# vai compilar tudo
keras.optimizers.SGD(
    learning_rate=100,
    momentum=1,
    nesterov=True,
    name="SGD"
)

model.compile(
    optimizer='SGD',
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# da o fit
history = model.fit(
    x=TrainingData,
    y=TrainingResults,
    validation_data=(ValidationData, ValidationResults),
    batch_size=20,
    epochs=500,
    verbose=1
)

Predictions = model.predict(ValidationData)
ConfusionMatrixValidation = sklearn.metrics.confusion_matrix(
    ValidationResults.argmax(axis=1),
    Predictions.argmax(axis=1)
)

Predictions = model.predict(TrainingData)
ConfusionMatrixTraining = sklearn.metrics.confusion_matrix(
    TrainingResults.argmax(axis=1),
    Predictions.argmax(axis=1)
)

# summarize history for accuracy
matplotlib.pyplot.plot(history.history['accuracy'])
matplotlib.pyplot.plot(history.history['val_accuracy'])
matplotlib.pyplot.title('model accuracy')
matplotlib.pyplot.ylabel('accuracy')
matplotlib.pyplot.xlabel('epoch')
matplotlib.pyplot.legend(['train', 'test'], loc='upper left')
matplotlib.pyplot.show()

# summarize history for loss
matplotlib.pyplot.plot(history.history['loss'])
matplotlib.pyplot.plot(history.history['val_loss'])
matplotlib.pyplot.title('model loss')
matplotlib.pyplot.ylabel('loss')
matplotlib.pyplot.xlabel('epoch')
matplotlib.pyplot.legend(['train', 'test'], loc='upper left')
matplotlib.pyplot.show()


# confusion matrix training
conf_matr_temp = pandas.DataFrame(ConfusionMatrixTraining, range(number_results), range(number_results))
seaborn.set(font_scale=1) # for label size
seaborn.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
matplotlib.pyplot.title('confusion matrix training')
matplotlib.pyplot.ylabel('expected')
matplotlib.pyplot.xlabel('predicted')
matplotlib.pyplot.show()

# confusion matrix validation
conf_matr_temp = pandas.DataFrame(ConfusionMatrixValidation, range(number_results), range(number_results))
seaborn.set(font_scale=1) # for label size
seaborn.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
matplotlib.pyplot.title('confusion matrix validation')
matplotlib.pyplot.ylabel('expected')
matplotlib.pyplot.xlabel('predicted')
matplotlib.pyplot.show()

Categories = numpy.array([0, 1, 2, 3]) # numero das categorias

NCatValidationResults = numpy.zeros(number_results)
for i in ValidationResults:
    NCatValidationResults = NCatValidationResults + i

NCatTrainingResults = numpy.zeros(number_results)
for i in TrainingResults:
    NCatTrainingResults = NCatTrainingResults + i

NCatTotalResults = numpy.zeros(number_results)
for i in labels:
    NCatTotalResults = NCatTotalResults + i

# number of categories validation data
matplotlib.pyplot.bar(x=Categories, height=NCatValidationResults, width=1)
matplotlib.pyplot.title('number of categories in validation data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatValidationResults)

# number of categories training data
matplotlib.pyplot.bar(x=Categories, height=NCatTrainingResults, width=1)
matplotlib.pyplot.title('number of categories in training data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatTrainingResults)

# number of categories total data
matplotlib.pyplot.bar(x=Categories, height=NCatTotalResults, width=1)
matplotlib.pyplot.title('number of categories in training data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatTotalResults)

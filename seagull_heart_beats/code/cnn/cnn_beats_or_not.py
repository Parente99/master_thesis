#!/usr/bin/python3.9

# este codigo vai treinar uma rede com 60 inputs e 1 output
# vou buscar os batimentos a um ficheiro csv e os 

import tensorflow as tf
#from tensorflow import keras as kr
import keras as kr
from keras.layers import InputLayer, Input
import matplotlib.pyplot as plt
import numpy as np
import sklearn as sk
from sklearn.model_selection import train_test_split
import seaborn as sb
import pandas as pd

def zerioum(data): # passa o array data para 0 ou 1 dependendo do threshold
    data_2=[]
    th = 0.5
    for i in range(len(data)):
        if data[i] < th:
            data_2 = np.append(data_2, 0)
        else:
            data_2 = np.append(data_2, 1)
    return data_2

#path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/csv_maker_to_see_if_cnn_works/array_0.csv'
#path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/data/perfect_beats_t-7200_bpm-150.csv'
#path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/data/fake_beats_var_bpm_noise_t-7200.csv'
path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/data/fake_beats_urban_sound_noise_t-7200.csv'

#path_2 = '/home/parente/Documents/tese/seagull_heart_beats/code/csv_maker_to_see_if_cnn_works/array_1.csv'
path_2 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/data/urban_sounds_var_noise_t-7200.csv'

full_data_1 = np.loadtxt(path_1, delimiter=',')
full_data_2 = np.loadtxt(path_2, delimiter=',')
#print(full_data_1.shape)
#print(full_data_2.shape)

full_data = np.vstack((full_data_1, full_data_2))
#print(full_data.shape)

data = full_data[:, 0:60] # ta correto
results = full_data[:, 60] # ta correto

number_inputs = data.shape[1]
number_outputs = 1
number_data = data.shape[0]

training_data, validation_data, training_results, validation_results = train_test_split(
    data,
    results,
    test_size=0.2
) # ta correto

# isto cria o modelo #
model = kr.Sequential()
model.add(kr.layers.Input(shape=number_inputs))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu))
model.add(kr.layers.core.Dense(20))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu))
model.add(kr.layers.core.Dense(20))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.softmax))
model.add(kr.layers.core.Dense(number_outputs))

# isto define o otimizador
tf.keras.optimizers.SGD(
    learning_rate=0.001,
    momentum=0,
    nesterov=True,
    name="SGD"
)

# esta parte junta o modelo todo
model.compile(
    optimizer='SGD',
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# isto treina o modelo
history = model.fit(
    x=training_data,
    y=training_results,
    validation_data=(validation_data, validation_results),
    batch_size=32,
    epochs=100,
    verbose=1
)

# salva o modelo
model.save("nn_model.h5")

# agora vou correr os dados de validacao pelo modelo
predictions = model.predict(validation_data)
predictions = zerioum(predictions)
# isto e para passar o array predictions para 0 ou 1 dependendo do valor

# e vou criar a matriz
ConfusionMatrixValidation = sk.metrics.confusion_matrix(
    validation_results,
    predictions
)

# agora vou correr os dados de treino pelo modelo
del(predictions)
predictions = model.predict(training_data)
predictions = zerioum(predictions)

ConfusionMatrixTraining = sk.metrics.confusion_matrix(
    training_results,
    predictions
)

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# confusion matrix training
conf_matr_temp = pd.DataFrame(ConfusionMatrixTraining, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix training')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

# confusion matrix validation
conf_matr_temp = pd.DataFrame(ConfusionMatrixValidation, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix validation')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

#!/usr/bin/python3.9

'''
sometimes i forget that i need to have some explation of the code or else im not gonna urderstand wtf am i doing
so in this code i have as input a sound then i pass that sound to mfcc
20 mfcc * n to be percise
i think
then i load the neural network model i trained erlier
now i pass the mfcc through the model and register the outputs
graph that shit

in this code i dont train nothing just see how the network behaves
'''


import numpy as np
import librosa as lb
import matplotlib.pyplot as plt
from keras.models import load_model
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/')
from mfccs_extractor import mfccs_extractor


# da o load do modelo
model = load_model('model_audio_excerpts.h5')


#print('diz qual e o caminho ate ao ficheiro')
#path = str(input())
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/eggs_giulia/egg15/0/FILE2.WAV'

audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate
#print(sample_rate)
data = mfccs_extractor(audio_file, sample_rate) # da me os mfcc

predictions = model.predict(data)

time = 0
beats_confidence = []
noise_confidence = []
line05 = []
for i in predictions:
    beats = i[0] # probabilidade de ser batimento
    noise = i[1] # probabilidade de ser ruido
    beats = float(beats)
    noise = float(noise)

    beats_confidence.append([time, beats])
    noise_confidence.append([time, noise])
    time += 5 # avanca o tempo
    line05.append(0.5) # cria linha de 50% de porbabilidade
    
    print(beats)
    print(noise)

beats_confidence = np.asarray(beats_confidence)
noise_confidence = np.asarray(noise_confidence)

#plt.plot(beats_confidence[:,0], beats_confidence[:,1])
plt.plot(noise_confidence[:,0], noise_confidence[:,1])
#plt.legend(['noise', 'beats'])
plt.plot(beats_confidence[:,0], line05)
plt.title('neural network beat detection')
plt.ylabel('confidence')
plt.ylim(0, 1.1)
plt.xlabel('time (s)')

plt.show()

#print(beats_confidence)
#!/usr/bin/python3.9

'''
okay pequeno pseudo codigo
vou buscar os ficheiros de som a pasta onde estao
inicializo dois np arrays um para o ruido outro para os batimentos
leio os ficheiros um a um
tenho em duas pastas uma para a o noise outra para os beats
sigo uma e depois a outra
dou load do som para um array
faco 15 mfcc * n
tiro as medias e devio paadrao
e ponho com os labels no fim
'''

import tensorflow as tf
#from tensorflow import keras as kr
import keras as kr
from keras.layers import InputLayer, Input
import matplotlib.pyplot as plt
import numpy as np
import sklearn as sk
from sklearn.model_selection import train_test_split
import seaborn as sb
import pandas as pd
import os


import librosa as lb
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from path_searcher import search_files
from normalizer_av0_max1 import normalizer_av0_max1
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/')
from mfccs_extractor import mfccs_extractor

n_mel_coe = 15

# this part calculates the mfcc for the noise
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_10000_samples/noise_quiet' # the path
extension = '.wav' # so its only sound
noise = [] # initializes the aray

paths_to_files = search_files(path, extension) # finds the files in the array
first_loop = False

for i in paths_to_files:
    audio_file, sample_rate = lb.load(i, sr=None) # loads the audio
    audio_file = normalizer_av0_max1(audio_file) # normalizes the audio file

    MFCCS = lb.feature.mfcc(y=audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS 
    MFCCS_mean = np.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
    MFCCS_std = np.std(MFCCS.T, axis=0)  # desvio padrao
    temp_arr = np.concatenate([MFCCS_mean,MFCCS_std]) # puts the std in the means (God's justice)
    temp_arr = np.append(temp_arr, [0,1]) # adds the label to the array

    if first_loop: # verifica se ja passou pelo primeiro loop
        noise = np.vstack((noise, temp_arr)) # se sim entao o array ja esta iniciado e e so adicionar em baixo
    else:
        noise = temp_arr # se nao entao e preciso inicializar
        first_loop = True
    
    #print('shape mfcc ' + str(MFCCS.shape))
    #print('shape temp arr ' + str(temp_arr.shape))
    #print('file ' + str(i))

# this part calculates the mfcc for the beats
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_10000_samples/beats' # the path
extension = '.wav' # so its only sound
beats = [] # initializes the aray

paths_to_files = search_files(path, extension) # finds the files in the array
first_loop = False

for i in paths_to_files:
    audio_file, sample_rate = lb.load(i, sr=None) # loads the audio
    audio_file = normalizer_av0_max1(audio_file) # normalizes the audio file

    MFCCS = lb.feature.mfcc(y=audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS 
    MFCCS_mean = np.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
    MFCCS_std = np.std(MFCCS.T, axis=0)  # desvio padrao
    temp_arr = np.concatenate([MFCCS_mean,MFCCS_std]) # puts the std in the means (God's justice)
    temp_arr = np.append(temp_arr, [1,0]) # adds the label to the array

    if first_loop: # verifica se ja passou pelo primeiro loop
        beats = np.vstack((beats, temp_arr)) # se sim entao o array ja esta iniciado e e so adicionar em baixo
    else:
        beats = temp_arr # se nao entao e preciso inicializar
        first_loop = True
    
    #print('shape mfcc ' + str(MFCCS.shape))
    #print('shape temp arr ' + str(temp_arr.shape))
    #print('file ' + str(i))

#print('shape before triming')
#print(beats.shape)
#print(noise.shape)

n_beats = beats.shape[0]
n_noise = noise.shape[0]

if (n_beats > n_noise):
    beats = beats[:n_noise,:]
if (n_noise > n_beats):
    noise = noise[:n_beats,:]

#print('shape after triming')
#print(beats.shape)
#print(noise.shape)


full_data = np.vstack((noise, beats))

data = full_data[:, :30] # ta 
results = full_data[:, 30:] # ta
print(data)
print(results)

training_data, validation_data, training_results, validation_results = train_test_split(
    data,
    results,
    test_size=0.005
) # ta correto

#print('data shape' + str(data.shape))
#print('results shape' + str(results.shape))
#print(data)
#print(results)

number_inputs = data.shape[1]
number_outputs = results.shape[1]
number_data = data.shape[0]

def zerioum(data): # passa o array data para 0 ou 1 dependendo do threshold
    data_2=[]
    th = 0.5
    for i in range(len(data)):
        if data[i] < th:
            data_2 = np.append(data_2, 0)
        else:
            data_2 = np.append(data_2, 1)
    return data_2

# isto cria o modelo #
model = kr.Sequential()
model.add(kr.layers.Input(shape=number_inputs))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu ))
model.add(kr.layers.core.Dense(256))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu))
model.add(kr.layers.core.Dense(64))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu))
model.add(kr.layers.core.Dense(16))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.sigmoid))
model.add(kr.layers.core.Dense(number_outputs))

# isto define o otimizador
tf.keras.optimizers.SGD(
    learning_rate=0.001,
    momentum=0,
    nesterov=True,
    name="SGD"
)

# esta parte junta o modelo todo
model.compile(
    optimizer='adam',
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# isto treina o modelo
history = model.fit(
    x=training_data,
    y=training_results,
    validation_data=(validation_data, validation_results),
    batch_size=10,
    epochs=50,
    verbose=1
)

model_name = "model_audio_excerpts.h5"
os.remove(model_name) # apaga o modelo para nao haver cenas corrompidas
model.save(model_name) # salva o modelo

# agora vou correr os dados de validacao pelo modelo
predictions = model.predict(validation_data)
predictions = np.round(predictions)
#predictions = zerioum(predictions)
# isto e para passar o array predictions para 0 ou 1 dependendo do valor

# e vou criar a matriz
ConfusionMatrixValidation = sk.metrics.confusion_matrix(
    validation_results.argmax(axis=1),
    predictions.argmax(axis=1)
)

# agora vou correr os dados de treino pelo modelo
del(predictions)
predictions = model.predict(training_data)
predictions = np.round(predictions)
#predictions = zerioum(predictions)

ConfusionMatrixTraining = sk.metrics.confusion_matrix(
    training_results.argmax(axis=1),
    predictions.argmax(axis=1)
)

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# confusion matrix training
conf_matr_temp = pd.DataFrame(ConfusionMatrixTraining, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix training')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

# confusion matrix validation
conf_matr_temp = pd.DataFrame(ConfusionMatrixValidation, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix validation')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()



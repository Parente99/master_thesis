#!/usr/bin/python3.9

'''
okay pequeno pseudo codigo
vou buscar os ficheiros de som a pasta onde estao
inicializo dois np arrays um para o ruido outro para os batimentos
leio os ficheiros um a um
tenho em duas pastas uma para a o noise outra para os beats
sigo uma e depois a outra
dou load do som para um array
faco 15 mfcc * n
tiro as medias e devio paadrao
e ponho com os labels no fim
'''

import tensorflow as tf
#from tensorflow import keras as kr
import keras as kr
from keras.layers import InputLayer, Input
import matplotlib.pyplot as plt
import numpy as np
import sklearn as sk
from sklearn.model_selection import train_test_split
import seaborn as sb
import pandas as pd
import os

import librosa as lb
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from path_searcher import search_files
from normalizer_av0_max1 import normalizer_av0_max1
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/')
from mfccs_extractor import mfccs_extractor



print("shape of audio file, sample rate, beats before label, beats after label, beats")
# this part calculates the mfcc for the beats
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_10000_samples/beats/joint_audio.wav' # the path
audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate
print(audio_file.shape)
print(sample_rate)

beats = mfccs_extractor(audio_file, sample_rate) # da me os mfcc
print(beats.shape)
beats = np.insert(beats, beats.shape[1], 0, axis=1) # acrescenta o label 0 (nao ha beats)
beats = np.insert(beats, beats.shape[1], 1, axis=1) # acrescenta o label 1 (ha beats)
print(beats.shape)
print(beats)



print("shape of audio file, sample rate, noise before label, noise after label, noise")
# this part calculates the mfcc for the noise
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_10000_samples/noise_quiet/joint_audio.wav' # the path
audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate
print(audio_file.shape)
print(sample_rate)

noise = mfccs_extractor(audio_file, sample_rate) # da me os mfcc
print(noise.shape)
noise = np.insert(noise, noise.shape[1], 1, axis=1) # acrescenta o label 1 (ha noise)
noise = np.insert(noise, noise.shape[1], 0, axis=1) # acrescenta o label 0 (nao ha noise)
print(noise.shape)
print(noise)



print("shape of beats, shape of noise, beats after trim, noise after trim")
n_beats = beats.shape[0]
n_noise = noise.shape[0]
print(beats.shape)
print(noise.shape)

if (n_beats > n_noise):
    beats = beats[:n_noise,:]
if (n_noise > n_beats):
    noise = noise[:n_beats,:]

print(beats.shape)
print(noise.shape)

#quit() # ate aqui funciona

print("full data, data shape, results shape, data, label(results)")
full_data = np.vstack((noise, beats))
print(full_data.shape)

data = full_data[:, :30] # ta 
results = full_data[:, 30:] # ta
print(data.shape)
print(results.shape)
print(data)
print(results)


print("trainnig data, validation data, training results, validation results")
training_data, validation_data, training_results, validation_results = train_test_split(
    data,
    results,
    test_size=0.2
) # ta correto

print(training_data.shape)
print(validation_data.shape)
print(training_results.shape)
print(validation_results.shape)

print("inputs, outputs, data")

number_inputs = data.shape[1]
number_outputs = results.shape[1]
number_data = data.shape[0]
print(number_inputs)
print(number_outputs)
print(number_data)

#quit() # ta certo

def zerioum(data): # passa o array data para 0 ou 1 dependendo do threshold
    data_2=[]
    th = 0.5
    for i in range(len(data)):
        if data[i] < th:
            data_2 = np.append(data_2, 0)
        else:
            data_2 = np.append(data_2, 1)
    return data_2

# isto cria o modelo #
model = kr.Sequential()
model.add(kr.layers.Input(shape=number_inputs))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu ))
#model.add(kr.layers.Dropout(0.2))

model.add(kr.layers.core.Dense(100))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.relu))
#model.add(kr.layers.Dropout(0.2))

model.add(kr.layers.core.Dense(50))
model.add(kr.layers.BatchNormalization())
model.add(kr.layers.core.Activation(kr.activations.softmax))
#model.add(kr.layers.Dropout(0.2))

#model.add(kr.layers.core.Dense(64))
#model.add(kr.layers.BatchNormalization())
#model.add(kr.layers.core.Activation(kr.activations.relu))
#model.add(kr.layers.Dropout(0.2))

#model.add(kr.layers.core.Dense(16))
#model.add(kr.layers.BatchNormalization())
#model.add(kr.layers.core.Activation(kr.activations.relu))
#model.add(kr.layers.Dropout(0.2))

model.add(kr.layers.core.Dense(number_outputs))
#model.add(kr.layers.core.Activation(kr.activations.sigmoid))

# isto define o otimizador
tf.keras.optimizers.SGD(
    learning_rate=0.001,
    momentum=0,
    nesterov=True,
    name="SGD"
)

# esta parte junta o modelo todo
model.compile(
    optimizer='SGD',
    loss='mean_absolute_error',  # categorical_crossentropy mean_squared_error mean_absolute_error
    metrics=['accuracy']
)

#callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5) # para parar de treinar o modelo mais cedo

# isto treina o modelo
history = model.fit(
    x=training_data,
    y=training_results,
    validation_data=(validation_data, validation_results),
    batch_size=10,
    epochs=50,
    verbose=1,
    #callbacks=[callback]
)

model_name = "model_audio_excerpts.h5"
os.remove(model_name) # apaga o modelo para nao haver cenas corrompidas
model.save(model_name) # salva o modelo

# agora vou correr os dados de validacao pelo modelo
predictions = model.predict(validation_data)
predictions = np.round(predictions)
print("predictions validation", predictions.shape)
#predictions = zerioum(predictions)
# isto e para passar o array predictions para 0 ou 1 dependendo do valor

# e vou criar a matriz
ConfusionMatrixValidation = sk.metrics.confusion_matrix(
    validation_results.argmax(axis=1),
    predictions.argmax(axis=1)
)

# agora vou correr os dados de treino pelo modelo
del(predictions)
predictions = model.predict(training_data)
predictions = np.round(predictions)
print("predictions traininng", predictions.shape)
#predictions = zerioum(predictions)

ConfusionMatrixTraining = sk.metrics.confusion_matrix(
    training_results.argmax(axis=1),
    predictions.argmax(axis=1)
)

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# confusion matrix training
conf_matr_temp = pd.DataFrame(ConfusionMatrixTraining, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix training')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

# confusion matrix validation
conf_matr_temp = pd.DataFrame(ConfusionMatrixValidation, range(2), range(2))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
plt.title('confusion matrix validation')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()



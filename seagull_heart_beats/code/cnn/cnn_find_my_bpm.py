#!/usr/bin/python3.9

# este codigo vai treinar uma rede com 60 inputs e 1 output
# vou buscar os batimentos a um ficheiro csv e os 

import tensorflow as tf
#from tensorflow import keras as kr
import keras as kr
from keras.layers import InputLayer, Input
import matplotlib.pyplot as plt
import numpy as np
import sklearn as sk
from sklearn.model_selection import train_test_split
import seaborn as sb
import pandas as pd

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import BatchNormalization

path = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/bpm_labled_mfcc_t_100000s_rand_noise.csv'
full_data = np.loadtxt(path, delimiter=',')
print(full_data)
#print(full_data.shape)

data = full_data[:, 0:60] # ta correto
results = full_data[:, 60] # ta correto

# vou por os results entre 0 e 1
#results = results - min(results)
#results = results / max(results)

number_inputs = data.shape[1]
number_outputs = 1
number_data = data.shape[0]

training_data, validation_data, training_results, validation_results = train_test_split(
    data,
    results,
    test_size=0.2
) # ta correto

# isto cria o modelo #
model = Sequential()
model.add(Dense(128, input_dim=60, activation='relu'))
model.add(BatchNormalization())
model.add(Dense(64, activation='relu'))
model.add(BatchNormalization())
model.add(Dense(1, activation='relu'))

# isto define o otimizador
tf.keras.optimizers.SGD(
    learning_rate=0.001,
    momentum=0,
    nesterov=True,
    name="SGD"
)

# esta parte junta o modelo todo
model.compile(
    optimizer='adam', # adam SGD
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# isto treina o modelo
history = model.fit(
    x=training_data,
    y=training_results,
    validation_data=(validation_data, validation_results),
    batch_size=32,
    epochs=100,
    verbose=1
)


# agora vou correr os dados de validacao pelo modelo
predictions = model.predict(validation_data)
predictions = np.round(predictions)
print(predictions)
# isto e para passar o array predictions para 0 ou 1 dependendo do valor

# e vou criar a matriz
ConfusionMatrixValidation = sk.metrics.confusion_matrix(
    validation_results,
    predictions
)
print(ConfusionMatrixValidation.shape[0])

# agora vou correr os dados de treino pelo modelo
del(predictions)
predictions = model.predict(training_data)
predictions = np.round(predictions)

ConfusionMatrixTraining = sk.metrics.confusion_matrix(
    training_results,
    predictions
)

# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


# confusion matrix validation
matrix_size = ConfusionMatrixValidation.shape[0]
conf_matr_temp = pd.DataFrame(ConfusionMatrixValidation, range(matrix_size), range(matrix_size))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp) # , annot=True, annot_kws={"size": 10}
plt.title('confusion matrix validation')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

# confusion matrix training
matrix_size = ConfusionMatrixTraining.shape[0]
conf_matr_temp = pd.DataFrame(ConfusionMatrixTraining, range(matrix_size), range(matrix_size))
sb.set(font_scale=1) # for label size
sb.heatmap(conf_matr_temp) # , annot=True, annot_kws={"size": 10}
plt.title('confusion matrix training')
plt.ylabel('expected')
plt.xlabel('predicted')
plt.show()

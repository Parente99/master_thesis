#!/usr/bin/python3.9

# rede baseada neste tutorial
# https://towardsdatascience.com/recurrent-neural-networks-by-example-in-python-ffd204f99470

# e neste
# https://www.freecodecamp.org/news/the-ultimate-guide-to-recurrent-neural-networks-in-python/


from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Input
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

signal = np.loadtxt("data/signal.csv")
label = np.loadtxt("data/label.csv")
len_data = signal.shape[0]
#print(signal.shape)
#print(label.shape)

len_sentence = 100

highest_multiple = len_data
while highest_multiple % len_sentence != 0: # se nao for divisivel pelo tamanho da frase entao vai subtraindo
    highest_multiple -= 1
    #print(highest_multiple)

signal = signal[:highest_multiple]
label = label[:highest_multiple]
#print(signal.shape)
#print(label.shape)

signal = np.reshape(signal, (-1, len_sentence, 1)) # (n_samples, time_steps, features)
label = np.reshape(label, (-1, len_sentence, 1))
#print(signal.shape)
#print(label.shape)

model = Sequential()

model.add(Input(shape=signal.shape[1:]))
model.add(LSTM(units = 64, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))
model.add(LSTM(units = 64, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))
model.add(LSTM(units = 64, return_sequences=False, dropout=0.1, recurrent_dropout=0.1))
model.add(Dropout(0.1))
model.add(Dense(len_sentence, activation='softmax'))

model.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])

# nao entendo porque e que preciso disto nunca nenhum tutorial precisou disto
signal_squeezed = tf.squeeze(signal, axis=-1)
label_squeezed = tf.squeeze(label, axis=-1)

history = model.fit(signal_squeezed, label_squeezed, batch_size=32, epochs=100, verbose=1)


# summarize history for loss

plt.plot(history.history['loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.show()

plt.plot(history.history['accuracy'])
plt.title('model accuracy')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.show()

#print(signal.shape)
#print(label.shape)

sample_signal = signal[0,:,0] # aqui vou ao sinal tirar uma amostra
sample_signal = np.reshape(sample_signal, (1, -1, 1)) # (n_samples, time_steps, features) # aqui passo para a maneira da rnn poder ler
sample_label = label[0,:,0] # tiro uma amostra do label
#print(sample_signal.shape)
#print(sample_label.shape)

prediction = model.predict(sample_signal) # faco a previsao

#print(sample_signal)
#print(sample_label)
#print(prediction)

# dou reshape para poder fazer os graficos que isto tem muitas dimensoes
sample_signal = np.reshape(sample_signal, (-1))
sample_label = np.reshape(sample_label, (-1))
prediction = np.reshape(prediction, (-1))



plt.plot(sample_signal)
plt.plot(sample_label)
plt.plot(prediction)
plt.title('predicted label vs real')
plt.ylabel('intensity')
plt.xlabel('time')
plt.legend(['signal', 'label', 'label prediction'], loc='upper left')
plt.show()
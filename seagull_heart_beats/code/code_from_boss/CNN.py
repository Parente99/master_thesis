#!/usr/bin/env python
# coding: utf-8

# In[ ]:


Francisco Neves, neves@coimbra.lip.pt


# In[2]:


import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import numpy as np
import wave
import sys

import keras
from keras.models import load_model
from keras.models import Sequential
from keras.layers import Conv1D, Dense, Dropout, Flatten, MaxPooling1D, AveragePooling1D
from keras.utils import np_utils
from keras.optimizers import RMSprop
from math import *


# In[3]:


# just Checking ...

spf = wave.open("/home/neves/notebook/CCMC/fake_beat_t-2000_snr-1000_bpm-100.wav")

# Extract Raw Audio from Wav File
signal = spf.readframes(-1)
signal = np.frombuffer(signal,dtype=np.int16)
fs = spf.getframerate()

# If Stereo
if spf.getnchannels() == 2:
    print("Just mono files")
    sys.exit(0)

n_sec=5
signal = signal[0:fs*n_sec]
signal = signal / max(abs(np.max(signal)),abs(np.min(signal)))

print("sampling rate = ",fs)    
print("debug = ",60./100.)
    
Time = np.linspace(0, len(signal) / fs, num=len(signal))

plt.figure(1)
plt.title("Signal Wave...")
plt.plot(Time, signal)
plt.show()


# ## condition data

# In[13]:


# - save blocks of 5 secs overllaping by 2.5s
# - mimic getting the rate as a average of 15s (5 consecutive blocks)... a trial method

def split(wave_name,bpm,input_stream,output_stream):

    print("processing: ",wave_name)
    spf = wave.open(wave_name)
    
    if spf.getnchannels() == 1: # mono
        
        # Extract Raw Audio from Wav File
        signal = spf.readframes(-1)
        signal = np.frombuffer(signal,dtype=np.int16)
        frate = spf.getframerate()
        
        n_sec = 5
        block_size = frate*n_sec
        block_stride = block_size/2

        block_start=0
        block_end=block_size

        ## maximum number of blocks per file
        nBlock=0
        nBlock_max=500
        
        while (block_end<len(signal)) and (nBlock<nBlock_max):
            block=signal[int(block_start):int(block_end)]
            block = block / max(abs(np.max(block)),abs(np.min(block))) # [-1,1]
            
            for d in block:
                input_stream.write("{} ".format(d))
            input_stream.write("\n")
                
            output_stream.write("{}\n".format(bpm));
            
            block_start+=block_stride
            block_end+=block_stride
            nBlock+=1
            


# In[14]:


# concatenate all data
# see bellow...

inf = open("/home/neves/notebook/CCMC/input.dat","w")
outf = open("/home/neves/notebook/CCMC/ouput.dat","w")

split("/home/neves/notebook/CCMC/fake_beat_t-2000_snr-1000_bpm-90.wav",90,inf,outf);
split("/home/neves/notebook/CCMC/fake_beat_t-2000_snr-1000_bpm-100.wav",100,inf,outf);
split("/home/neves/notebook/CCMC/fake_beat_t-2000_snr-1000_bpm-110.wav",110,inf,outf);
split("/home/neves/notebook/CCMC/fake_beat_t-2000_snr-1000_bpm-120.wav",120,inf,outf);

inf.close()
outf.close()


# In[4]:


# shuffle all data (this is slow and consumes a lot of memory. Better to do only once)
# see bellow...

data = np.loadtxt("/home/neves/notebook/CCMC/input.dat")
results = np.loadtxt("/home/neves/notebook/CCMC/ouput.dat")

data, results = shuffle(data, results)

np.savetxt("/home/neves/notebook/CCMC/input_rnd.dat",data)
np.savetxt("/home/neves/notebook/CCMC/ouput_rnd.dat",results)



# ## train

# In[2]:


get_ipython().run_line_magic('reset', '-f')


# In[5]:


# s1|s2 only

# w11 ... w1n
# w21 ... w2n
# ...
# wm1 ... wmn

data = np.loadtxt("/home/neves/notebook/CCMC/input_rnd.dat")
results = np.loadtxt("/home/neves/notebook/CCMC/ouput_rnd.dat")
results = (results-90.)/30.

lenData = len(data)
lenRes = len(results)

if (lenData!=lenRes):
    raise Exception("data and results must have the same length")

testFrac=0.1
lenTestData = int(round(testFrac*lenData))
lenTestRes = int(round(testFrac*lenRes))


# In[7]:


x_train = data[(lenTestData):]
x_test = data[:(lenTestData)]
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# debug the 1st waveforms.
plt.plot(x_train[0],'r-',x_test[0],'g-')

# rows=number of waveforms, columns=waveform size
(n_waves,wave_size)=x_train.shape
x_train=x_train.reshape(n_waves,wave_size,1)
(n_waves,wave_size)=x_test.shape
x_test=x_test.reshape(n_waves,wave_size,1)

y_train = results[(lenTestRes):]
y_test = results[:(lenTestRes)]

n_outputs = 1


# In[8]:


# Creating histogram 
fig, axs = plt.subplots(1, 1)
# axs.hist(results,30)
axs.hist(y_train,30)
plt.title("results")
plt.xlabel("label")
plt.ylabel("counts");
plt.show()


# In[16]:


cnn = Sequential()

cnn.add(Conv1D(filters=128,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
cnn.add(Conv1D(filters=64,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
cnn.add(MaxPooling1D(pool_size=10,strides=2,padding='same'))

cnn.add(Dropout(rate=0.25))
cnn.add(Flatten());

cnn.add(Dense(64,activation='relu'))
cnn.add(Dense(16,activation='relu'))

cnn.add(Dense(n_outputs,activation='linear'))

# define optimizer and objective, compile cnn
cnn.compile(loss="mse", optimizer="adam",metrics=['mae'])
cnn.summary()


# In[17]:


batch_size = 16
epochs = 20

history = cnn.fit(x_train, y_train,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        validation_data=(x_test, y_test))


# In[18]:


y_pred = cnn.predict(x_test)
plt.plot(y_test,y_pred,'.')


#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from scipy import signal as sgn
import numpy as np
import wave
import sys

import keras
import tensorflow as tf
from keras.models import load_model
from keras.models import Sequential
from keras.layers import Conv1D, Dense, Dropout, Flatten, MaxPooling1D, AveragePooling1D, LSTM, Input
from keras.utils import np_utils
from keras.optimizers import RMSprop
from math import *


# In[2]:


# just Checking ...

spf = wave.open("/home/neves/notebook/CCMC-CNN/fake_beat_t-2000_snr-1000_bpm-100.wav")

# Extract Raw Audio from Wav File
signal = spf.readframes(-1)
signal = np.frombuffer(signal,dtype=np.int16)
fs = spf.getframerate()

# If Stereo
if spf.getnchannels() == 2:
    print("Just mono files")
    sys.exit(0)

n_sec=5
offset=1000
signal = signal[offset:offset+fs*n_sec]
signal = signal / max(abs(np.max(signal)),abs(np.min(signal)))

beat_rate=100
stamp = int(fs*60/beat_rate)
beat=np.empty(len(signal)); beat.fill(0)
for i in range(int(stamp/2), len(beat), stamp):
    beat[i] = 1
    
print("sampling rate = ",fs)    
print("debug = ",60./100.)
    
Time = np.linspace(0, len(signal) / fs, num=len(signal))

plt.figure(1)
plt.title("Signal Wave...")
plt.plot(Time, signal,"-",Time,beat,"-")
plt.show()


# In[2]:


# - save blocks of 5 secs overllaping by 2.5s
# - mimic getting the rate as a average of 15s (5 consecutive blocks)... a trial method

def split(wave_name,bpm,input_stream,output_stream):

    n_sec = 5
    resample=50
    delta=15
    nBlock_max=2000

    print("processing: ",wave_name)
    spf = wave.open(wave_name)
    
    if spf.getnchannels() == 1: # mono
        
        # Extract Raw Audio from Wav File
        signal = spf.readframes(-1)
        signal = np.frombuffer(signal,dtype=np.int16)
        signal = sgn.resample(signal,int(len(signal)/resample))
        frate = spf.getframerate()/resample                

        block_size = frate*n_sec
        block_stride = block_size/2
        stamp_size = frate*60/bpm
        stamp_stride= stamp_size/2
        
        block_end=stamp_size
        beat=np.empty(len(signal)); beat.fill(0)        
        while (block_end<len(beat)):
            stamp=int(block_end-stamp_stride)
            beat[stamp-delta:stamp+delta]=1
            block_end+=stamp_size
            
        nBlock=0
        block_start=0
        block_end=block_size
        ## maximum number of blocks per file
        
        while (block_end<len(signal)) and (nBlock<nBlock_max):
            
            block_sig=signal[int(block_start):int(block_end)]
            block_sig = block_sig / max(abs(np.max(block_sig)),abs(np.min(block_sig))) # [-1,1]
            for d in block_sig:
                input_stream.write("{} ".format(d))
            input_stream.write("\n")

            block_beat=beat[int(block_start):int(block_end)]
            for d in block_beat:
                output_stream.write("{} ".format(d))
            output_stream.write("\n");
            
            block_start+=block_stride
            block_end+=block_stride
            nBlock+=1
            


# In[3]:


# concatenate all data
# see bellow...

inf = open("/home/neves/notebook/CCMC-RNN/input.dat","w")
outf = open("/home/neves/notebook/CCMC-RNN/ouput.dat","w")

split("/home/neves/notebook/CCMC-CNN/fake_beat_t-2000_snr-1000_bpm-90.wav",90,inf,outf);
split("/home/neves/notebook/CCMC-CNN/fake_beat_t-2000_snr-1000_bpm-100.wav",100,inf,outf);
split("/home/neves/notebook/CCMC-CNN/fake_beat_t-2000_snr-1000_bpm-110.wav",110,inf,outf);
split("/home/neves/notebook/CCMC-CNN/fake_beat_t-2000_snr-1000_bpm-120.wav",120,inf,outf);

inf.close()
outf.close()


# In[4]:


# shuffle all data (this is slow and consumes a lot of memory. Better to do only once)
# see bellow...

data = np.loadtxt("/home/neves/notebook/CCMC-RNN/input.dat")
results = np.loadtxt("/home/neves/notebook/CCMC-RNN/ouput.dat")

data, results = shuffle(data, results)

np.savetxt("/home/neves/notebook/CCMC-RNN/input_rnd.dat",data)
np.savetxt("/home/neves/notebook/CCMC-RNN/ouput_rnd.dat",results)


# In[5]:


data = np.loadtxt("/home/neves/notebook/CCMC-RNN/input_rnd.dat")
results = np.loadtxt("/home/neves/notebook/CCMC-RNN/ouput_rnd.dat")

lenData = len(data)
lenRes = len(results)

if (lenData!=lenRes):
    raise Exception("data and results must have the same length")

testFrac=0.1
lenTestData = int(round(testFrac*lenData))
lenTestRes = int(round(testFrac*lenRes))


# In[6]:


x_train = data[(lenTestData):]
x_test = data[:(lenTestData)]
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# rows=number of waveforms, columns=waveform size
(n_waves,wave_size)=x_train.shape
x_train=x_train.reshape(n_waves,wave_size,1)
(n_waves,wave_size)=x_test.shape
x_test=x_test.reshape(n_waves,wave_size,1)

y_train = results[(lenTestRes):]
y_test = results[:(lenTestRes)]

# rows=number of waveforms, columns=waveform size
(n_waves,wave_size)=y_train.shape
y_train=y_train.reshape(n_waves,wave_size,1)
(n_waves,wave_size)=y_test.shape
y_test=y_test.reshape(n_waves,wave_size,1)


# In[7]:


plt.plot(x_train[0],'r-',y_train[0],'g-')


# In[8]:


plt.plot(x_test[0],'r-',y_test[0],'g-')


# In[9]:


# https://stackoverflow.com/questions/62933735/should-cnn-layers-come-before-bi-lstm-or-after
# https://stackoverflow.com/questions/55433649/how-to-combine-lstm-and-cnn-models-in-keras
# https://wiki.tum.de/display/lfdv/Recurrent+Neural+Networks+-+Combination+of+RNN+and+CNN

model = Sequential()

model.add(Conv1D(filters=64,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
model.add(Conv1D(filters=128,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
model.add(MaxPooling1D(pool_size=10,strides=2,padding='same'))
model.add(Dropout(0.1))

#model.add(Input(shape=x_train.shape[1:]))
model.add(LSTM(units = 64, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))
model.add(LSTM(units = 64, return_sequences=True, dropout=0.1, recurrent_dropout=0.1))
model.add(LSTM(units = 64, return_sequences=False, dropout=0.1, recurrent_dropout=0.1))

model.add(Flatten())
model.add(Dense(64,activation='relu'))
model.add(Dense(wave_size,activation='linear'))

model.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])
model.summary()


# In[10]:


x_train_ = tf.squeeze(x_train, axis=-1)
y_train_ = tf.squeeze(y_train, axis=-1)
x_test_ = tf.squeeze(x_test, axis=-1)
y_test_ = tf.squeeze(y_test, axis=-1)


# In[11]:


# 100
history = model.fit(x_train_, y_train_, batch_size=64, epochs=100, verbose=1, validation_data=(x_test_,y_test_))


# In[14]:


# summarize history for loss

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.show()

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.show()


# In[15]:


sample_x = x_train[0,:,0] # aqui vou ao sinal tirar uma amostra
sample_x = np.reshape(sample_x, (1, -1, 1)) # (n_samples, time_steps, features) # aqui passo para a maneira da rnn poder ler
sample_y = y_train[0,:,0] # tiro uma amostra do label
prediction = model.predict(sample_x) # faco a previsao

sample_x = np.reshape(sample_x, (-1))
sample_y = np.reshape(sample_y, (-1))
prediction = np.reshape(prediction, (-1))

plt.plot(sample_x)
plt.plot(sample_y)
plt.plot(prediction)
plt.title('predicted label vs real')
plt.ylabel('intensity')
plt.xlabel('time')
plt.legend(['signal', 'label', 'label prediction'], loc='upper left')
plt.show()


# In[21]:


block=15

sample_x = x_test[block,:,0]
sample_x = np.reshape(sample_x, (1, -1, 1)) # (n_samples, time_steps, features) # aqui passo para a maneira da rnn poder ler
sample_y = y_test[block,:,0] # tiro uma amostra do label
prediction = model.predict(sample_x) # faco a previsao

sample_x = np.reshape(sample_x, (-1))
sample_y = np.reshape(sample_y, (-1))
prediction = np.reshape(prediction, (-1))

plt.plot(sample_x)
plt.plot(sample_y)
plt.plot(prediction)
plt.title('predicted label vs real')
plt.ylabel('intensity')
plt.xlabel('time')
plt.legend(['signal', 'label', 'label prediction'], loc='upper left')
plt.show()


# In[12]:


model.save("/home/neves/notebook/CCMC-RNN/model00")


# In[ ]:





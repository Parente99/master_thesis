#!/usr/bin/python3.9
# https://stackoverflow.com/questions/37999150/how-to-split-a-wav-file-into-multiple-wav-files/62872679

import pandas
from datetime import timedelta

data = pandas.read_excel('N116CE9.xls')
print(data.keys())

print(data['HORA'][0])

var_1 = data['HORA'][0]
var_2 = data['HORA'][1]

print((var_2 - var_1).total_seconds())
print(data)

# File 0 <=> FILE0.out.wav
# o primeiro tempo é o tempo quando o ficheiro inicia
#!/usr/bin/python3.7

# este codigo tem uma funcao onde pomos o tipo de reduçao dados e o numero de componentes e saiem os dados reduzidos
# ainda não estou a conseguir que saia com tempo tmb tenho de corrigir esse erro
# vai agora invocar o codigo para graficos interativos para poder saber a que corresponde cada cluster

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from interactive_graph_2 import temp

def debug():

    print('number of data in complete data.........' + str(len(data)))
    print('number of dimensions in complete data...' + str(len(data[0])))
    print('number of data in reduced data..........' + str(len(reduced_data)))
    print('number of dimensions in reduced data....' + str(len(reduced_data[0])))
    print()

def reduction(reduction_type, data, n_components):

    from sklearn.preprocessing import StandardScaler
    data = StandardScaler().fit_transform(data) # normaliza a data
    reduced_data = 'inicializer'

    if reduction_type == 'pca':
        from sklearn.decomposition import PCA
        reduced_data = PCA(n_components=n_components, random_state=0).fit_transform(data)

    elif reduction_type == 'truncated_svd':
        from sklearn.decomposition import TruncatedSVD
        reduced_data = TruncatedSVD(n_components=n_components).fit_transform(data)

    elif reduction_type == 'isomap':
        from sklearn.manifold import Isomap
        reduced_data = Isomap(n_components=n_components).fit_transform(data)

    elif reduction_type == 'locally_linear_embedding': # é um pouco aleatorio # sempre muito mau
        from sklearn.manifold import LocallyLinearEmbedding
        reduced_data = LocallyLinearEmbedding(n_components=n_components).fit_transform(data)

    else:
        reduced_data = 'something went wrong' # só para não se confundir
        print('wrong maniford type!')

    #debug()

    return reduced_data

def main():
    '''
    data_complete = np.loadtxt('/home/parente/Documents/tese/seagull/codes_output_data/data_giulia_mfcc_60.csv', delimiter=',', fmt='%s')
    types_of_reductions =['pca', 'truncated_svd', 'isomap', 'locally_linear_embedding']
    data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
    labels = [i[0] for i in data_complete]
    '''
    types_of_reductions =['pca', 'truncated_svd', 'isomap', 'locally_linear_embedding']
    df = pd.read_csv('/home/parente/Documents/tese/seagull/codes_output_data/data_giulia_mfcc_60.csv', delimiter=',')
    data_complete_list = [list(row) for row in df.values]
    data_complete = np.asarray(data_complete_list)
    data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
    labels = [i[0] for i in data_complete]
    
    '''
    print (data)
    print (labels)
    '''

    for i in types_of_reductions:
        a = reduction(i, data, 2)
        temp(labels, a[:,0], a[:,1])

if __name__ == "__main__":
   main()

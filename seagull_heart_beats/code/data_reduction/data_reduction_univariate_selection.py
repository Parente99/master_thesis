#!/usr/bin/python3.7

# este codigo vai pegar nos dados tirar as dimensoes menos importantes e plotar duma maneira semi interativa

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from interactive_graph_2 import temp
from sklearn.preprocessing import StandardScaler




df = pd.read_csv('/home/parente/Documents/tese/seagull/codes_output_data/data_giulia_mfcc_60.csv', delimiter=',')
data_complete_list = [list(row) for row in df.values]
data_complete = np.asarray(data_complete_list)
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
labels = [i[0] for i in data_complete]
data = StandardScaler().fit_transform(data) # normaliza a data
reduced_data = 'inicializer'

test = SelectKBest(score_func=f_classif, k=4)

reduced_data = PCA(n_components=n_components, random_state=0).fit_transform(data)
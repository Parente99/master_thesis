#!/usr/bin/python3.9

# peseudo codigo
'''
abrir o ficheiro
por tudo num array 60 * n
dividir o ficheiro nas suas dimensoes
por cada dimensao no seu lugar dum dicionario
calcular as variancias de cada entrada do dicionario e adicionar a este como mais uma caracteristica
ordenar por variancia
escolher as 2 maiores e plotar
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('/home/parente/Documents/tese/codes_output_data/data_giulia_mfcc_60.csv', delimiter=',') # retira como data frame
data_complete_list = [list(row) for row in data.values]
data_complete = np.asarray(data_complete_list) # passa para np array
del(data) # apaga a variavel data para ter a certeza que não ha erros estupidos pode ser inutil mas nao quero correr riscos
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
labels = [i[0] for i in data_complete]

n_dimensions = len(data[0,:])
#print(n_dimensions) # funciona sai 60
dimensions_dic = {} # inicializa o dicionario de dimensoes

for i in range(n_dimensions):
    #print(i)
    data_in_dimension_i_str = data[:,i] # array de strings
    data_in_dimension_i = [float(numeric_string) for numeric_string in data_in_dimension_i_str] # muda para array de ints
    #print(data_in_dimension_i) # funciona
    variance = np.var(data_in_dimension_i)
    dimensions_dic[str(i)] = [data_in_dimension_i, variance]
    print(variance)
    #print(len(data_in_dimension_i)) # ta certo
#print()

max1 = 0
key1 = 0
max2 = 0
key2 = 0

for i in range(n_dimensions):
    temp_var = dimensions_dic[str(i)][1] # especifica a key e o elemento do valeu porque há dois
    #print(temp_var) # ya é mesmo a vaiancia
    if temp_var > max1: # vê qual é o maior maximo
        max1 = temp_var
        key1 = i

del(temp_var) # opa quero garantir que comeca tudo do zero
del(i)
for i in range(n_dimensions):
    print (i)
    temp_var = dimensions_dic[str(i)][1] # especifica a key e o elemento do valeu porque há dois
    if temp_var == max1:
        print('do nothing')
    elif temp_var > max2: # vê qual é o segundo maior maximo porque nem considera o maior
        max2 = temp_var
        key2 = i

print()
print(max1)
print(key1)
print(max2)
print(key2)

## agora para a parte do grafico interativo

x_data = dimensions_dic[str(key1)][0]
y_data = dimensions_dic[str(key2)][0]
    
def onpick(event): # é o detetor do evento carregar no ponto
    ind = event.ind # lista de pontos onde carregaste
    label_pos_x = event.mouseevent.xdata
    label_pos_y = event.mouseevent.ydata
    i = ind[0] # escolho só o primeiro ponto
    label = labels[i]
    print ("index", i, label)
    ax.figure.canvas.draw_idle()

fig = plt.figure()
ax = plt.subplot()
ax.set_ylabel('dimension 2')
ax.set_xlabel('dimension 1')
ax.set_title('most spread out dimensions')

ax.scatter(
    x_data,
    y_data,
    picker=True,
    c='black', 
    s=1
)

fig.canvas.mpl_connect('pick_event', onpick)
plt.plot()
print ("scatterplot done")
plt.show()

#!/usr/bin/python3.7


import os
from mfccs_60_extractor import mfccs_extractor
import numpy as np
import time

start_time = time.time()

path_1 = '/home/parente/Documents/tese/seagull/audio_data/eggs_giulia'
data=[]

for dir_1 in os.listdir(path_1): # especifico o egg

    path_2 = os.path.join(path_1, dir_1)

    for dir_2 in  os.listdir(path_2): # especifico o numero
        path_3 = os.path.join(path_2, dir_2)

        for wav_file in  os.listdir(path_3): # especifico o ficheiro de som
            path_4 = os.path.join(path_3, wav_file)
            if wav_file.endswith(".WAV"):  # verifica se  um ficheiro de som

                print(path_4) # funciona
                mfccs_extractor (path_4, data)
            
        break
    break



np.savetxt('/home/parente/Documents/tese/seagull/codes_output_data/data_giulia_mfcc_60.csv', data, delimiter=',')

execution_time = (time.time() - start_time)
print('Execution time in seconds: ' + str(execution_time))

#!/usr/bin/python3.7


import os
from mfccs_60_extractor import mfccs_extractor
import numpy as np
import time

start_time = time.time()
path = '/home/parente/Documents/tese/seagull/audio_data/eggs_giulia/egg15/0'
data=[]


for wav_file in  os.listdir(path): # especifico o ficheiro de som
    path_1 = os.path.join(path, wav_file)
    if wav_file.endswith(".WAV"):  # verifica se  um ficheiro de som

        print(path_1) # funciona
        mfccs_extractor (path_1, data)

data = list(data) # converte para lista para ver se o erro desaparece
np.savetxt('/home/parente/Documents/tese/seagull/codes_output_data/data_giulia_mfcc_60.csv', data, delimiter=',', fmt='%s')

execution_time = (time.time() - start_time)
print('Execution time in seconds: ' + str(execution_time))

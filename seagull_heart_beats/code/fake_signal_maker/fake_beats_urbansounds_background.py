#!/usr/bin/python3.9

# entao com este codigo o utilizaodr vai ter de dizer o tempo de ficheiro de som que quer e o tipo de ruido
# depois com os 9 batimentos que tenho ele vai fazer um ficheiro de batimentos falsos

# peseudo codigo
'''
utilizador seleciona o tempo
utilizador seleciona o tipo de ruido
abre 9 ficheiros
descobre a frame rate
guarda num np array
com a frame rate ve quantos frames sao precisos para chegar ao tempo pedido
gera ruido com o numero de frames
buscando os arrays do sinal duma forma aleatoria constroi os batimentos cardiacos
esta construcao é feita por adicionar zeros ao array do pulso unico ate ter o bpm que queremos
soma os dois
passa para wav
guarda
'''

import librosa as lb
import soundfile as sf
import numpy as np
import os
import random as rd
from noise_generator import white, pink, blue, brown, violet
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1
sys.path.insert(0, '/home/parente/Documents/tese/urban_sounds/code/fake_sound_maker/')
from fake_sound_maker import fake_sound_maker

def fake_signal_generator(time, bpm):
    path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/single_beats'
    heart_beats = {} # dicionario que vai ter todos os arrays de som
    dic_key = 0 # so para inicializar

    # esta parte poe todso os ficheiros de batimentos cardiacos num dicionario
    for wav_file in  os.listdir(path): # especifico o ficheiro de som

        if wav_file.endswith(".wav"):  # verifica se  um ficheiro de som
            path_1 = os.path.join(path, wav_file)
            audio_file, frame_rate = lb.load(path_1, sr=None)  # isto da me o ficheiro em array e o frame rate, sr=none e para manter o sample rate original do ficheiro
            audio_file = np.array(audio_file) 
            # muda para numpy array caso nao esteja
            audio_file = normalizer_av0_max1(audio_file)
            # normaliza para ficar com media 0 e maximo absoluto 1
            heart_beats[str(dic_key)] = audio_file 
            # adiciona o audio falie a um dicionario
            dic_key += 1



    n_frames = int(frame_rate * time) # numero minimo de frames do ficheiro
    total_frames = 0 # só para inicializar
    fake_beats = []

    # calcular o tempo e frames que cada pulso tem de ter
    pulse_time = 60/bpm # em segundos
    pulse_frames = int(frame_rate * pulse_time) # numero frame que o pulso tem de ter para o ficheiro ter os BPM que qeuremos

    # esta parte cria um ficheiro de som sem ruido. ficherio fake_beats
    while total_frames < n_frames:
        pulse_key = rd.randrange(0,dic_key) # o pulse_key é um aleatrio que o que faz é selecionar um batimanto cardiaco aleatrio da lista deles
        temp_pulse = heart_beats[str(pulse_key)] # busca o beat aleatorio, tipo o array mesmo

        # adiciona zeros a segir ao pulso para chegar ao numero correto de bpm
        len_pulse = len(temp_pulse)
        #print(len_pulse) # numerode samples num pulso
        #print(pulse_frames) # numero de samples que um pulso tem de ter para ter os bpms desejados
        pulse_gap = pulse_frames - len_pulse
        zero = np.zeros(pulse_gap)
        temp_pulse = np.append(temp_pulse, zero) # agora o pulso tem os frames nececarios
        # adiciona ao ficheiro com os batimentos
        fake_beats = np.append(fake_beats, temp_pulse)
        del temp_pulse

        total_frames = len(fake_beats) # neste caso o comprimento do array vai corresponder ao numero de samples/frames estou a usar as duas palavras para representar a mesma coisa
        #print('esta ' + str((total_frames/n_frames)*100) + '% completo')

    # cria o som urbano
    urban_sound, urban_sample_rate = fake_sound_maker(time, 1000)
    # adiciona o som urbano como ruido, vai ter 10% de influencia
    urban_sound = urban_sound * 0.1

    # vamos por os dois arrays com o mesmo tempo
    len_fake_beats = len(fake_beats)
    len_urban_sound = len(urban_sound)
    if (len_fake_beats > len_urban_sound):
        fake_beats = fake_beats[:len_urban_sound] # corta o fake_beats para ter o mesmo tamanho que o urban sounds
    else:
        urban_sound = urban_sound[:len_fake_beats] # same mas ao contrario
    
    fake_beats_and_urban_noise = fake_beats + urban_sound
    
    return fake_beats_and_urban_noise, frame_rate

def main():

    print("insert the minimum time for the fake file (seconds, type integer > 0)")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo
    print("heart rate in bpm (int)")
    bpm = int(input())

    fake_beats_and_urban_noise, frame_rate = fake_signal_generator(time, bpm)
    # okay esta parte vai ser para gerar o nome do ficheiro
    file_name = 'data/fake_beat_urban_noise_t-' + str(time) + '_bpm-' + str(bpm) + '.wav'
    sf.write(file_name, fake_beats_and_urban_noise, frame_rate)


if __name__ == "__main__":
   main()

#!/usr/bin/python3.9

# okay quero criar um batimento cardiaco perfeito
# para isso vou usar so um som o "melhor" deles
# vou ter bpm variavel no entanto nao vou ter ruido
# vou ter duas funcoes uma que
# pega no batimento
# passa par array
# multiplica ate ter o tempo necessario
# exporta um array

# a funcao main vai so guardar esse array como wav

import librosa as lb
import soundfile as sf
import numpy as np
import os
import random as rd
from noise_generator import white, pink, blue, brown, violet

def fake_signal_generator(time, bpm): # nesta funcao vou ter como inputs o tempo que quero de audio e os bpm. como output vou ter um array que e o som e o frame rate desse array. com isto consigo gravar o ficheiro de som.
    path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/single_beats/15_00_04_01.wav' # caminho ate ao fichiero que eu quero
    audio_file, frame_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate
    frame_rate = 2016 # sim e posto direto mas tem mesmo de ser
    # acho que o frame rate que o libroa exporta esa errado. sao 2016 samples por segundo # ya exporta como 22050
    print('frame rate' + str(frame_rate))
    len_pulse = len(audio_file)
    print('comprimento pulso' + str(len_pulse))
    # calcular o tempo e frames que cada pulso tem de ter

    n_frames = int(frame_rate * time) # numero minimo de frames do ficheiro
    print('numero de frames do ficheiro' + str(n_frames))
    total_frames = 0 # só para inicializar
    fake_beats = []

    pulse_time = 60/bpm # em segundos
    pulse_frames = int(frame_rate * pulse_time) # numero frame que o pulso tem de ter para o ficheiro ter os BPM que qeuremos
    print('nuemro de frames num pulso' + str(pulse_frames))

    # esta parte cria um ficheiro de som sem ruido. ficherio fake_beats
    while total_frames < n_frames:
        # adiciona zeros a segir ao pulso para chegar ao numero correto de bpm
        
        #print(len_pulse) # numerode samples num pulso
        #print(pulse_frames) # numero de samples que um pulso tem de ter para ter os bpms desejados
        pulse_gap = pulse_frames - len_pulse
        zero = np.zeros(pulse_gap)
        full_pulse = np.append(audio_file, zero) # agora o pulso tem os frames nececarios

        # adiciona ao ficheiro com os batimentos
        fake_beats = np.append(fake_beats, full_pulse) 
        del full_pulse

        total_frames = len(fake_beats) # neste caso o comprimento do array vai corresponder ao numero de samples/frames estou a usar as duas palavras para representar a mesma coisa
        print('esta ' + str((total_frames/n_frames)*100) + '%% completo')
    
    fake_beats = (2 * (fake_beats - min(fake_beats)) / (max(fake_beats) - min(fake_beats))) - 1 # isto e para normalizar o array enter -1 e 1

    return fake_beats, frame_rate

def main():

    print("insert the minimum time for the fake file (seconds, type integer > 0)")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo
    print("heart rate in bpm (int)")
    bpm = int(input())

    fake_beats_and_noise, frame_rate = fake_signal_generator(time, bpm)
    # okay esta parte vai ser para gerar o nome do ficheiro
    file_name = 'perfect_beat_t-' + str(time) + '_bpm-' + str(bpm) + '.wav'
    sf.write(file_name, fake_beats_and_noise, frame_rate)


if __name__ == "__main__":
   main()

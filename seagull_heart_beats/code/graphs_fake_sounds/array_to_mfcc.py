#!/usr/bin/python3.9

# neste codigo dou som como array e o samplerate
# retiro todos os mfccs 
# e mando isso como output

# inputs: som como array e samplerate
# outputs: mfccs

import librosa
import numpy as np

def mfcc_extractor (data_sound, sr):

    # inicializacoes
    win_len = 6 # em segundos
    hop_len = 3 # em segundos
    n_mel_coe = 30
    first_time = False # ve se o array onde os dados vao ser guardados ja foi inicializado


    i = -1
    while True: # vai iterar ate quebrar opa e feio que fode mas funciona e sem erros
        i += 1
        try:
            # vai arranjar o intervalo de tempo que vai analisar
            start = i * hop_len # tempo de inicio da time slice
            end = (i * hop_len) + win_len # tempo de fim da time slice

            # passando para frames
            start_frame = librosa.time_to_samples(start, sr=sr)
            end_frame = librosa.time_to_samples(end, sr=sr)

            temp_data = data_sound[start_frame:end_frame] # audio da time slice
            # tira os mfccs desse elemnto audio
            MFCCS = librosa.feature.mfcc(y=temp_data, sr=sr, n_mfcc=n_mel_coe)  # da me os MFCCS
            MFCCS_mean = np.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
            MFCCS_std = np.std(MFCCS.T, axis=0)  # desvio padrao
            temp_arr = np.concatenate([MFCCS_mean,MFCCS_std])

            if first_time:
                data_mfcc = np.vstack((data_mfcc, temp_arr))
            else: # inicializa o array de dados
                first_time = True
                data_mfcc = temp_arr

            print(data_mfcc.shape)

        except Exception as e:
            print('erro............. ',str(e))
            print('fim ficheiro audio')
            break # chegou ao final do ficheiro e vai dar um erro do frame pedido ser maior que os frames existentes

    return data_mfcc

# isto e para o codigo correr em vazio
def main():
    path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_beats_63_files/constant_t_snr_bpm/fake_beat_t-2000_snr-100_bpm-90.wav'
    
    from sound_to_array import sound_load
    print('sound load')
    audio_filtred, sr = sound_load(path)
    print('get the mfcc')
    mfcc_data = mfcc_extractor (audio_filtred, sr)

    print('this is the data')
    print(mfcc_data)
    print('this is the shape')
    print(mfcc_data.shape)

if __name__ == "__main__":
   main()
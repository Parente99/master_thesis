#!/usr/bin/python3.9

from sound_to_array import sound_load
from array_to_mfcc import mfcc_extractor
from mfccs_to_2d import dimension_reduction 
import matplotlib.pyplot as plt
import numpy as np

import sys
sys.path.insert(0, '..')
from path_searcher import search_files

path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_beats_63_files/constant_t_snr_bpm'
a = len(path)

fake_files_path = search_files(path, 'wav') # lista com todos os paths dos fake files

for i in fake_files_path:
    name = i[a:]
    print(name)
    print(i)
    audio_filtred, sr = sound_load(i)
    mfcc_data = mfcc_extractor (audio_filtred, sr)
    data_2d = dimension_reduction(mfcc_data)
    print(data_2d[:,0])
    plt.scatter(data_2d[:,0], data_2d[:,1], label=name)

plt.show()
#!/usr/bin/python3.9

# entram dados com 60 dimensoes e saem com 2

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

n_components = 2 # quero isto para iniciar

def dimension_reduction(data): # aqui entram os dados ja com as 60 dimensoes so
    mean_matrix = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/mean_matrix.csv', header=0, index_col=0) # tem de ser com header=None porque se nao o data frame intrepreta a primeira linha como a descricao da coluna
    standard_deviation_matrix = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/standard_deviation_matrix.csv', header=0, index_col=0)
    eigenvector_subset = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/eigenvector_subset.csv', header=0, index_col=0)

    mean_matrix = mean_matrix.to_numpy()
    standard_deviation_matrix = standard_deviation_matrix.to_numpy()
    eigenvector_subset = eigenvector_subset.to_numpy()

    mean_matrix = np.transpose(mean_matrix)
    standard_deviation_matrix = np.transpose(standard_deviation_matrix)

    print(data.shape)
    print(mean_matrix.shape)
    print(standard_deviation_matrix.shape)

    meaned_data = data - mean_matrix
    normalized_data = meaned_data/standard_deviation_matrix # poe o desvio padrao a 1
    data_reduced = np.dot(eigenvector_subset.transpose() , normalized_data.transpose() ).transpose() # esta é a parte da reducao em si onde ha a perda de informacao
    return (data_reduced)


# isto e para o codigo correr em vazio
def main():
    path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_beats_63_files/constant_t_snr_bpm/fake_beat_t-2000_snr-100_bpm-90.wav'
    
    from sound_to_array import sound_load
    audio_filtred, sr = sound_load(path)

    from array_to_mfcc import mfcc_extractor
    mfcc_data = mfcc_extractor (audio_filtred, sr)

    data_reduced = dimension_reduction(mfcc_data)

    print('this is the data')
    print(data_reduced)
    print('this is the shape')
    print(data_reduced.shape)
    
    #plt.scatter(data_reduced[:,0], data_reduced[:,1], s=1, c='black')
    #plt.show() # block=False

if __name__ == "__main__":
   main()

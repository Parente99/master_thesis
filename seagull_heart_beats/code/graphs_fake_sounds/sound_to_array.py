#!/usr/bin/python3.9

# neste codigo dou o load dum ficheiro de som
# transformo tudo num dataframe normal
# passo por um filtro de frequencias
# e mando isso como output
# tambem mando ca para fora a samplerate

# inputs: caminho ate ao ficheiro de som
# outputs: data frame com samplerate

import numpy as np
import math as ma
import librosa as lb
from numpy.fft import fft, ifft
from matplotlib import pyplot as plt

def white_noise_gen():
    mean = 0
    std = 1 
    num_samples = 44100000
    samples = np.random.normal(mean, std, size=num_samples)
    return samples

def filter(audio_normalized, sr): # pa depois faco isto
    return audio_normalized


def sound_load(path):
    low_bound = 50
    high_bound = 200
    
    audio, sr = lb.load(path) # dou load do ficheiro audio
    time_step = 1/sr # intervalo de tempo em segundos entre cada sample

    # okay agora vou normalizar
    mean = sum(audio) / len(audio) # calculo a media
    audio_squared = np.square(audio) # elevo todos os valores ao quadrado
    mean_of_the_squared = sum(audio_squared) / len(audio_squared) # calculo a media dos valores quadrados
    standard_deviation = ma.sqrt(mean_of_the_squared - np.square(mean)) # com estes dois consigo mais facilmente calcular o desvio padrao
    audio_normalized = (audio-mean)/standard_deviation # subtraio a media e divido pelo desvio padrao normalizando os dados

    # white_noise = white_noise_gen()

    # agora vou filtrar o som # talvez no futuro passe isto para uma funcao externa
    audio_filtred = filter(audio_normalized, sr)

    return audio_filtred, sr


# isto e para o codigo correr em vazio
def main():
    path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_beats_63_files/constant_t_snr_bpm/fake_beat_t-2000_snr-50_bpm-90.wav'
    audio, sr = sound_load(path)



    
if __name__ == "__main__":
   main()

# este codigo e para tratar os sons das gaivotas
# vou pegar no som de X min e tirar uma slice de Ys avancar (Y/2)s e tirar outra slice (Y = 5?)
# depois mfccs normais
# array 60*n com 30 medias e 30 desvios padrao 

import librosa
import librosa.display
import numpy
import keras
import os
import matplotlib.pyplot
import time

# inicializacoes
win_len = 5
fraq_hop = 2 # fraçao do tamanho da janela que vai ser o hop len
hop_len = win_len/fraq_hop
n_mel_coe = 30
data = []
path = "/home/parente/Documents/Tese/seagull"  # path ate ao folder para iterar

files_names = os.listdir(path)  # array com todos os nomes de todos os ficheiros
for file_name in files_names:
    if file_name.endswith(".mp3"):  # verifica se e um ficheiro de som mp3
        full_path = os.path.join(path, file_name)  # isto da me o path ate aquilo que eu quero

        # agora para a analise de som
        audio_file, frame_rate = librosa.load(full_path)  # isto da me o ficheiro em array acho e o samplerate
        audio_lenght = librosa.get_duration(y = audio_file, sr = frame_rate) # tempo do ficheiro em segundos
        audio_frames = int(audio_lenght * frame_rate) # dá me o numero total de frames do ficheiro
        win_len_fra = int(win_len * frame_rate) # tamanho da janela em frames
        hop_len_fra = int(hop_len * frame_rate) # tamanho do salto em frames
        #n_iter = int(fraq_hop*(audio_frames/win_len_fra)) # numero de iteracoes para precorrer o ficheiro, sao arredondadas entao e possivel que deixe alguns frmaes de fora no final, mas tmb o que e que sao alguns frames num fichero de 20 min

        #print(audio_file)
        #print(frame_rate)
        #print(audio_lenght)
        #print(audio_frames)
        #print(win_len_fra)
        #print(hop_len_fra)
        #print(n_iter)
        #print("")
        #matplotlib.pyplot.figure(figsize=(15,15))
        #librosa.display.waveplot(audio_file, frame_rate)
        #matplotlib.pyplot.show()

        for i in range(1000000): # e so um numero muito grande para ir iterando
            try:
                # vai arranjar o intervalo do audio que vai analisar
                start_frame = i * hop_len_fra # frame de inicio da time slice
                end_frame = (i * hop_len_fra) + win_len_fra # frame de fim da time slice
                temp_audio_file = audio_file[start_frame:end_frame] # audio da time slice

                # tira osmfccs desse elemnto audio
                MFCCS = librosa.feature.mfcc(y=temp_audio_file, sr=frame_rate, n_mfcc=n_mel_coe)  # da me os MFCCS
                MFCCS_mean = numpy.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
                MFCCS_std = numpy.std(MFCCS.T, axis=0)  # desvio padrao
                temp_arr = numpy.concatenate([MFCCS_mean,MFCCS_std])
                data.append(temp_arr)
            except:
                break # chegou ao final do ficheiro e vai dar um erro do frame pedido ser maior que os frames existentes

            # ploting things

            #librosa.display.specshow(MFCCS)
            #time.sleep(0.5)
            print(full_path)
            #print(MFCCS_mean)
            #print(MFCCS_std)
            #print(temp_arr)
            print(i)
            print("")


data_array = numpy.array(data)  # passa de formato list para formato array
numpy.savetxt('data_1.csv', data_array, delimiter=',')

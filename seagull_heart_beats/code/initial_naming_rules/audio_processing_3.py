#!/usr/bin/python3.7

# exatamente o mesmo que o data_processing_2 faz mas com funcoes mais bonitas
# clonei para nao quebrar o outro
#
# este codigo e para tratar os sons das gaivotas
# vou pegar no som de X min e tirar uma slice de Ys avancar Zs e tirar outra slice (Y > Z)
# depois mfccs normais
# array 61*n com 30 medias e 30 desvios padrao e no inicio o tempo da slice

import librosa
import librosa.display
import numpy
import os
import matplotlib.pyplot
from itertools import count # para o for loop ficar mais bonito

# inicializacoes
win_len = 5
hop_len = 2
n_mel_coe = 30
data = []
path = "/home/parente/Documents/tese/seagull"  # path ate ao folder para iterar
files = ['seagull_3_joint.mp3'] # array com os nomes dos ficheiros # esta parte tem de ser posta manualmente


for file_name in files: # itera entre os documentos que queremos analisar
    full_path = os.path.join(path, file_name)  # isto da me o path ate aquilo que eu quero

    # agora para a analise de som especifica de um ficheiro
    audio_file, frame_rate = librosa.load(full_path)  # isto da me o ficheiro em array acho e o
    print(frame_rate)
    print(len(audio_file))
    i = -1
    while True: # vai iterar ate quebrar
        i += 1
        try:
            # vai arranjar o intervalo de tempo que vai analisar
            start = i * hop_len # tempo de inicio da time slice
            end = (i * hop_len) + win_len # tempo de fim da time slice

            # passando para frames
            start_frame = librosa.time_to_samples(start, sr=frame_rate)
            end_frame = librosa.time_to_samples(end, sr=frame_rate)

            temp_audio_file = audio_file[start_frame:end_frame] # audio da time slice

            # tira osmfccs desse elemnto audio
            MFCCS = librosa.feature.mfcc(y=temp_audio_file, sr=frame_rate, n_mfcc=n_mel_coe)  # da me os MFCCS
            MFCCS_mean = numpy.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
            MFCCS_std = numpy.std(MFCCS.T, axis=0)  # desvio padrao
            temp_arr = numpy.concatenate([MFCCS_mean,MFCCS_std])
            temp_arr = numpy.insert(temp_arr, 0, start) # adiciona o tempo ao inicio do array
            data.append(temp_arr)

            print('time start... ', start)
            print('time end..... ', end)
            print('frame start.. ', start_frame)
            print('frame end.... ', end_frame)
            print('line array... ', temp_arr)
        except:
            break # chegou ao final do ficheiro e vai dar um erro do frame pedido ser maior que os frames existentes
        print(full_path)
        print(i)
        print("")

data_array = numpy.array(data)  # passa de formato list para formato array
numpy.savetxt('data_2.csv', data_array, delimiter=',')

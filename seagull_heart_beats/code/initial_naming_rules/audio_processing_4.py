#!/usr/bin/python3.7

# entao com este quero fazer uma ediçao um pouco diferente dos mfccs
# vou comecar por isolar certas frequencias as mais importantes
# assim espero eliminar muito do ruido logo na ediçao
# com o que restar posso ir logo para os mfccs ou tentar algo diferente

import librosa
import librosa.display
import numpy as np
import os
import matplotlib.pyplot as plt
from itertools import count # para o for loop ficar mais bonito

temp_path = '/home/parente/Documents/tese/seagull/audio_data/eggs_giulia/egg22/0/FILE0.WAV'

audio, sr = librosa.load(temp_path)
freq = librosa.stft(audio) # faz uma fft da cena
freq_pos = np.abs(freq) # tira a parte imaginaria e poe tudo real
freq_db = librosa.amplitude_to_db(freq_pos, ref=np.min) # passa para db onde o valor minimo de intensidade corresponde a 0 db

n_time = len(freq_db[0])
n_freq = len(freq_db)

print('number of time intervals.......' + str(n_time))
print('number of frequency intervals..' + str(n_freq))

# cropper de intensidade
'''
top = 12
low = 0
for i in range(n_time):
    for j in range(n_freq):
        temp = freq_db[j,i]
        if temp > top:
            freq_db[j,i] = 0
        if temp < low:
            freq_db[j,i] = 0
'''
# cropper de frequencias # há 1024 intervalos de frequencias

#freq_db = freq_db[:20,:]



fig, ax = plt.subplots()

img = librosa.display.specshow(freq_db, y_axis='log', x_axis='time', ax=ax)

ax.set_title('Power spectrogram')

fig.colorbar(img, ax=ax) # , format="%+2.0f dB"

plt.savefig('test.svg')

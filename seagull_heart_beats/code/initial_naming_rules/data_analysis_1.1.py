# este codigo vai usar dbscan como clustering algorithm
# nao vai mandar ca para fora graficos porque são 60 dimensoes e eu não sei como representar isso...
# por o tempo no algoritmo pode ter sido um erro (foi)
# mas dá imenso jeito para ver onde estão os dados
# neste algoritmo em vz de ir por uma procura binaria como estava a ir antes vou por varrimentos com steps que variam com a iteraçao

import numpy as np
import random
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

# data

data_complete = np.loadtxt('data_2.csv', delimiter=',')
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
times = [i[0] for i in data_complete]

data_size = len(data)
data_dimensions = len(data[0])
noise_threshold = data_size*(50/100) # numero maximo de pontos de ruido em percentagem
min_sample_size = int(np.sqrt(data_size)) # o valor minimo de dados para ser considerado um conjunto é a sqrt dos numero total de dados. why? porque acho que já ouvi nalgum lado que esse é um valor bem razoavel


# Compute DBSCAN

n_clusters_goal = 3
#n_clusters = 1 # so para inicializar
step = 10 # quanto o raio altera a cada iteraçao # este valor muda durante o loop
radius = 1 # raio da circunferencia que aglomera os pontos

# o clustering algorithm em si
while (True):
    radius = radius + step
    db = DBSCAN(eps=radius, min_samples=min_sample_size).fit(data)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise = list(labels).count(-1)

    print('number of total data..... ', data_size)
    print('number of dimensions..... ', data_dimensions)
    print('size of radius........... ', radius)
    print('number of noise points... ', n_noise)
    print('number of clusters....... ', n_clusters)
    print()

    if (n_noise <= 80): # data_size*(10/100)
        radius = 0 # o raio reinicia
        step = step/2 # os steps reduzem de tamanho par serem mais detalhados

    if (n_clusters == n_clusters_goal and n_noise < noise_threshold):
        break

    # quero ter 3 clusters com o minimo de pontos de ruido
    # um so cluster pode ser no inicio ou no fim das iteracoes
    # posso fazer a seleçao

# agora que já tenho so clusters vou colocar cada um no seu array
# estes arrays vao ter só os indices de cada cluster assim posso corresponder tanto ao array dos dados como ao do tempo
# indices uma ova! isto é um array de booleans com o tamanho da matriz data que diz se o inidice esta ou nao no cluster
cluster_A = labels == 0
cluster_B = labels == 1
cluster_C = labels == 2
cluster_N = labels == -1 # cluster do ruido
#posso fazer assim porque sei que só tenho 3 clusters e quero guardar em 3 arrays diferentes

# agora tirar os tempos em que cada cluster foi adicionado

def info_array_creator(inf, cluster): # info vai ser ou o tempo ou os dados dependendo do que queremos # cluster é referente ao cluster do qual queremos tirar a informaçao
    info_array = [] # inicializa o array do tempo do cluster que queremos
    for i in range(len(cluster)): # circula pleos indices do array que queremos
        if (cluster[i] == True):
            info_array.append(inf[i]) # se estiever no cluster em questao adiciona a matriz
    return info_array

cluster_A_times = info_array_creator(times, cluster_A)
cluster_B_times = info_array_creator(times, cluster_B)
cluster_C_times = info_array_creator(times, cluster_C)
cluster_N_times = info_array_creator(times, cluster_N)

print('cluster A size and times')
print (len(cluster_A_times))
print(cluster_A_times)

print('cluster B size and times')
print (len(cluster_B_times))
print(cluster_B_times)

print('cluster C size and times')
print (len(cluster_C_times))
print(cluster_C_times)

print('cluster N size and times')
print (len(cluster_N_times))
print(cluster_N_times)

# okay agora só para terminar o dia de hoje vou fazer um grafico da cena

def graph_array_creator(num, cluster): # info vai ser ou o tempo ou os dados dependendo do que queremos # cluster é referente ao cluster do qual queremos tirar a informaçao
    info_array = [] # inicializa o array do tempo do cluster que queremos
    for i in range(len(cluster)): # circula pleos indices do array que queremos
        if (cluster[i] == True):
            info_array.append(num) # se estiever no cluster em questao adiciona o numero correspondente
        else:
            info_array.append(None) # se não estiver adiciona zero
    return info_array

graph_cluster_A = graph_array_creator(0, cluster_A)
graph_cluster_B = graph_array_creator(1, cluster_B)
graph_cluster_C = graph_array_creator(2, cluster_C)
graph_cluster_N = graph_array_creator(-1, cluster_N)

plt.plot(graph_cluster_A)
plt.plot(graph_cluster_B)
plt.plot(graph_cluster_C)
plt.plot(graph_cluster_N)

plt.show()

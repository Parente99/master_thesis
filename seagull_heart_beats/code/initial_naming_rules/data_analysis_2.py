# para este codigo vou tentar usar o clustering algorithm mean shift
# ve isto para saberes o que é
# https://towardsdatascience.com/the-5-clustering-algorithms-data-scientists-need-to-know-a36d136ef68
# dados de entrada com 60 dimensoes embora não ssendo nada limitativo

from sklearn.cluster import MeanShift
import numpy as np

# pequeno tratamento da data para separar o tempo das dimensoes
data_complete = np.loadtxt('data_2.csv', delimiter=',')
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
times = [i[0] for i in data_complete] # remoe tudo exceto os valores da primeira coluna

# alguns dados da data
data_size = len(data)
data_dimensions = len(data[0])

# alguns dados do clustering algorithm
n_clusters = 1 # so para inicializar
n_clusters_goal = 6
step = 10 # quanto a bandwidth altera a cada iteraçao # este valor muda durante o loop
current = '' # se cabou de aumentar o raio
previous = '' # se cabou de diminuir o raio
bd = step # entao esta vai ser a bandwidth estou a intrepretar isto como um raio a volta dos pontos moveis e nesse raio ve onde esta a maior concentraçao de pontos e vai para ai
# tal como no data_analysis_1 vou iterar ate acabar com 3 grupos
# neste algoritmo nem vai haver ruido
# e tenho uma grande vantagem  a relaçao é "linear" tipo quanto maior for o raio do bicho menor vai ser o numero de clusters

# o clustering algorithm em si
first_loop = False
while (True):
    cluster = MeanShift(bandwidth=bd).fit(data)
    labels = cluster.labels_
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    #print(labels)
    print('number of clusters....', n_clusters)
    print('radius size...........', bd)



    if (n_clusters < n_clusters_goal):
        bd = bd - step
        current = 'less'
    else:
        bd = bd + step
        current = 'more'

    if (current != previous and first_loop == True):
        step = step/2
    previous = current
    first_loop = True

    if (n_clusters == n_clusters_goal): break


print('number of clusters....', labels)
print('radius size...........', n_clusters)

# este codigo vai usar dbscan como clustering algorithm
# nao vai mandar ca para fora graficos porque são 60 dimensoes e eu não sei como representar isso...
# por o tempo no algoritmo pode ter sido um erro # (((((it was)))))
# mas dá imenso jeito para ver onde estão os dados

# este vai ser baseado no codigo analysis 1 mas neste vou fazer dois graficos que vao representar o ruido e o numero de clusters em funçao do raio
# vou alterra e fazer uma coisa nao vai haver ruido tudo vao ser pontos validos para o numero de clusters depender do raio diretamente

import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

# data
data_complete = np.loadtxt('data_2.csv', delimiter=',')
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
times = [i[0] for i in data_complete]

data_size = len(data)
data_dimensions = len(data[0])
noise_threshold = data_size/2
#min_sample_size = int(np.sqrt(data_size)) # o valor minimo de dados para ser considerado um conjunto é a sqrt dos numero total de dados. why? porque acho que já ouvi nalgum lado que esse é um valor bem razoavel
min_sample_size = 1

# graficos que vou mostrar no final
radii = []
noises = []
clusters = []

# Compute DBSCAN

n_clusters = 1 # so para inicializar

step = 0.001 # quanto o raio altera a cada iteraçao # este valor muda durante o loop
current = '' # se cabou de aumentar o raio
previous = '' # se cabou de diminuir o raio
radius = step # raio da circunferencia que aglomera os pontos
initial_radius = radius

# o clustering algorithm em si
while (True):
    db = DBSCAN(eps=radius, min_samples=min_sample_size).fit(data)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    # print(labels) # para ver qual é o output # dá mesmo um array de numeros cada numero corresponde a um cluster

    # Number of clusters in labels, ignoring noise if present.
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise = list(labels).count(-1)

    radii.append(radius)
    noises.append(n_noise)
    clusters.append(n_clusters)

    print('number of total data..... ', data_size)
    print('number of dimensions..... ', data_dimensions)
    print('size of radius........... ', radius)
    print('number of noise points... ', n_noise)
    print('number of clusters....... ', n_clusters)
    print()

    radius += step

    if (n_clusters == 1):
        break



plt.figure()

plt.subplot(211)
plt.plot(radii, clusters)
plt.title('radii vs clusters')
plt.grid(True)

plt.subplot(212)
plt.plot(radii, noises)
plt.title('radii vs noises')
plt.grid(True)


#plt.show()
plt.savefig('test.png')


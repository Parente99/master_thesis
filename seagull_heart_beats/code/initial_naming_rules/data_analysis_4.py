#!/usr/bin/python3.9

# para este codigo vou tentar usar o clustering algorithm mean shift
# ve isto para saberes o que é
# https://towardsdatascience.com/the-5-clustering-algorithms-data-scientists-need-to-know-a36d136ef68
# neste codigo vou iterar por usar o algoritmo de reduçao pca a duas dimensoes e vou apontar para 4 clusters e ver se consigodar um plot disso

from sklearn.cluster import MeanShift
import numpy as np
from data_reduction_several_types import reduction
import matplotlib.pyplot as plt

# pequeno tratamento da data para separar o tempo das dimensoes
data_complete = np.loadtxt('/home/parente/documents/tese/codes_output_data/data_giulia_mfcc_60.csv', delimiter=',')
data = np.delete(data_complete, 0, 1) # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
times = [i[0] for i in data_complete] # remoe tudo exceto os valores da primeira coluna

# alguns dados da data
data_size = len(data)
data_dimensions = len(data[0])
final_dimensions = 2 # numero de dimensoes a seguir a serem reduzidas

# alguns dados do clustering algorithm
n_clusters = 1 # so para inicializar
n_clusters_goal = 6
step_fraq = 2 # fraçao do raio que aumenta ou diminui consoante o que for necessarioS
bd = 12.814453125 # entao esta vai ser a bandwidth estou a intrepretar isto como um raio a volta dos pontos moveis e nesse raio ve onde esta a maior concentraçao de pontos e vai para ai
# tal como no data_analysis_1 vou iterar ate acabar com n_clusters_goal grupos
# neste algoritmo nem vai haver ruido
# e tenho uma grande vantagem  a relaçao é "linear" tipo quanto maior for o raio do bicho menor vai ser o numero de clusters
print('ta a reduzir as dimensoes')
data_reduced = reduction('isomap', data, final_dimensions) # reduz o numero de dimensoes para 2
# so para debug
#plt.scatter(data_reduced[:,0], data_reduced[:,1], c='black', s=1)
#plt.show()

# o clustering algorithm em si
while (True):
    cluster = MeanShift(bandwidth=bd).fit(data_reduced)
    labels = cluster.labels_
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    #print(labels)
    print('number of clusters....', n_clusters)
    print('radius size...........', bd)



    if (n_clusters < n_clusters_goal): # o raio é muito grande
        bd = bd - bd/step_fraq

    elif (n_clusters > n_clusters_goal): # o raio é muito pequeno
        bd = bd + bd/step_fraq

    elif(n_clusters == n_clusters_goal):
        break

    else:
        print('what?')

# para inicializar os arrays dos clusters
cluster_A = np.empty([1,final_dimensions])
cluster_B = np.empty([1,final_dimensions])
cluster_C = np.empty([1,final_dimensions])
cluster_D = np.empty([1,final_dimensions])
cluster_E = np.empty([1,final_dimensions])
cluster_F = np.empty([1,final_dimensions])

for i in range(3,len(labels)):
    lab = labels[i]
    dat = data_reduced[i,:] # ta bem
    '''
    # isto era só par debug
    print(i)
    print(lab)
    print(dat)
    print()
    '''
    if lab == 0:
        #cluster_A.extend(dat)
        cluster_A = np.vstack((cluster_A, dat))
    if lab == 1:
        cluster_B = np.vstack((cluster_B, dat))
    if lab == 2:
        cluster_C = np.vstack((cluster_C, dat))
    if lab == 3:
        cluster_D = np.vstack((cluster_D, dat))
    if lab == 4:
        cluster_E = np.vstack((cluster_E, dat))
    if lab == 5:
        cluster_F = np.vstack((cluster_F, dat))

size = 5
plt.scatter(cluster_A[:,0], cluster_A[:,1], c='black', s=size)
plt.scatter(cluster_B[:,0], cluster_B[:,1], c='red', s=size)
plt.scatter(cluster_C[:,0], cluster_C[:,1], c='navy', s=size)
plt.scatter(cluster_D[:,0], cluster_D[:,1], c='purple', s=size)
plt.scatter(cluster_E[:,0], cluster_E[:,1], c='maroon', s=size)
plt.scatter(cluster_F[:,0], cluster_F[:,1], c='orange', s=size)
plt.show()

# agora a seguir a ver tudo bonito vou fazer um data_reduced mas com labels

labled_data=np.empty([1,final_dimensions+1])

for i in range(3,len(labels)):
    temp = []
    lab = int(labels[i])
    dat = data_reduced[i,:] # ta bem
    temp = np.hstack((lab, dat))
    #print(temp)
    labled_data = np.vstack((labled_data, temp))

#print()
labled_data = np.delete(labled_data, (0), axis=0) # tira a primeira linha que é a vazia
#print(labled_data)


np.savetxt('/home/parente/Documents/tese/seagull/codes_output_data/labled_reduced_data_1.csv', labled_data, delimiter=',')

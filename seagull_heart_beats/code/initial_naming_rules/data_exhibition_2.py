# este codigo vai fazer tudo tratamento e representacao
# ele pega num fichero que se seleciona e depois
# vou pegar no som de X min e tirar uma slice de Ys avancar (Y/2)s e tirar outra slice (Y = 5?)
# depois mfccs normais
# array 60*n com 30 medias e 30 desvios padrao
# depois vai fazer uma representaçao por histograma dos dados
# neste caso os dados vao ser um array n*60 onde os 60 vao ser na verdade 30 medias dos mfccs e 30 desvios padrao
# os dados estão ordenados m-d-m-d-m-d ...

import librosa
import numpy
import keras
import os
import matplotlib.pyplot as plt
import time

###########################################################
######################### tratamento ######################
###########################################################

# inicializacoes
file_name = "silence_1.wav" # poe aqui o nome fichero que queres analisar
win_len = 5
fraq_hop = 2 # fraçao do tamanho da janela que vai ser o hop len
hop_len = win_len/fraq_hop
n_mel_coe = 30
data = []
path = "/home/parente/Documents/tese/seagull"  # path ate ao folder para iterar

full_path = os.path.join(path, file_name)  # isto da me o path ate aquilo que eu quero

# agora para a analise de som
audio_file, frame_rate = librosa.load(full_path)  # isto da me o ficheiro em array acho e o samplerate
audio_lenght = librosa.get_duration(y = audio_file, sr = frame_rate) # tempo do ficheiro em segundos
audio_frames = int(audio_lenght * frame_rate) # dá me o numero total de frames do ficheiro
win_len_fra = int(win_len * frame_rate) # tamanho da janela em frames
hop_len_fra = int(hop_len * frame_rate) # tamanho do salto em frames

for i in range(1000000): # e so um numero muito grande para ir iterando
    try:
        # vai arranjar o intervalo do audio que vai analisar
        start_frame = i * hop_len_fra # frame de inicio da time slice
        end_frame = (i * hop_len_fra) + win_len_fra # frame de fim da time slice
        temp_audio_file = audio_file[start_frame:end_frame] # audio da time slice

        # tira osmfccs desse elemnto audio
        MFCCS = librosa.feature.mfcc(y=temp_audio_file, sr=frame_rate, n_mfcc=n_mel_coe)  # da me os MFCCS
        MFCCS_mean = numpy.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
        MFCCS_std = numpy.std(MFCCS.T, axis=0)  # desvio padrao
        temp_arr = numpy.concatenate([MFCCS_mean,MFCCS_std])
        data.append(temp_arr)
    except:
        break # chegou ao final do ficheiro e vai dar um erro do frame pedido ser maior que os frames existentes

    print(full_path)
    print(i)
    print("")

############################################################
######################## representacao ######################
##############################################################


data = numpy.array(data)
n_bins = 20
n_average = 30
n_stan_dev = 30
n_data = data.shape[0]
# n_data = 100
font_size = 2

# okay isto vai ser o dicionario das medias que vai ter as camadas media (30)
av_dict = {}

# okay isto vai ser o dicionario das medias que vai ter as camadas media (30)
sd_dict = {}

# esta parte prepara os dados isto é poe tudo nos dicionarios
for i in range(n_data): # iteraçao nos dados
    for j in range(n_average): # iteração nas medias

        jj = j*2 # posicão da media j no array
        dat = data[i, jj]

        if (j) in av_dict.keys(): # se já estiver inicializado
            temp = av_dict[j]
            temp = numpy.append(temp, dat)
            av_dict[j] = temp
        else: # se nao estiver inicializado
            av_dict[j] = numpy.array(dat)
        print('data ' + str(i) + ' / average ' + str(j))

    for k in range(n_stan_dev): # iteração nos desvios padrao

        kk = k*2+1 # posicão do desvio padrao k no array
        dat = data[i, kk]

        if (k) in sd_dict.keys(): # se já estiver inicializado
            temp = sd_dict[k]
            temp = numpy.append(temp, dat)
            sd_dict[k] = temp
        else: # se nao estiver inicializado
            sd_dict[k] = numpy.array(dat)
        print('data ' + str(i) + ' / standard deviation ' + str(k))

# esta parte exibe os dicionarios

# tamanho do subplot tipo numero de graficos na figura
y_c = 6
x_c = 5

y=-1
x=0

fig, axs = plt.subplots(x_c, y_c)
fig.suptitle('x -> intensity / y -> occurrences', fontsize=font_size*4)

for av in range(n_average): # interação nas medias tambem vou usar o mesmo valor para os desvios padrao porque são iguais e dá me jeito # é uma fonte de bugs se isso se alterar
    # esta parte "descobre" os valores correspondestes de x e y
    if y < y_c:
        y += 1
    if y == y_c:
        y = 0
        x += 1
    sd = av # só para ficar mais bonito
    temp_av = av_dict[av]
    temp_sd = sd_dict[sd]
    axs[x,y].hist(temp_av, bins = n_bins, histtype='step', stacked=True, fill=False, label="av", linewidth=font_size*0.1) # average
    axs[x,y].hist(temp_sd, bins = n_bins, histtype='step', stacked=True, fill=False, label="sd", linewidth=font_size*0.1) # standard deviation
    axs[x,y].tick_params(labelsize=font_size, length=font_size, width=font_size*0.1)
    axs[x,y].tick_params(axis='x', top=True, bottom=False, labeltop=True, labelbottom=False, pad=0)
    axs[x,y].legend(fontsize=font_size, frameon=False, labelspacing=font_size*0.1, loc=1)
    axs[x,y].set_title('coeficient ' + str(av), fontsize=font_size, pad=0)

plt.savefig(fname = file_name + '.pdf')

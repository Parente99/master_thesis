# este codigo vai fazer uma representaçao em grafico dos dados onde vou representar uma dimencao em funcao de outra e elas vao alterando
# o que as dimensoes represetam importa pouco o que importa é os formatos que podem demonstrar
# vai ser tudo guardado em imagens individuais que vao ser salvadas na diretoria /home/parente/Documents/tese/seagull/codes_output_data/60_dimension_representation
# neste caso os dados vao ser um array n*60 onde os 60 vao ser na verdade 30 medias dos mfccs e 30 desvios padrao
# os dados estão ordenados m-d-m-d-m-d ...

import librosa
import numpy as np
import keras
import os
import matplotlib.pyplot as plt
import time

def debug():

    x_data_size = len(x_data) # dá me o temanho dos dados para garantir que estou a olhar para a coisa certa
    y_data_size = len(y_data)


    print('x index........' + str(x_index))
    print('y index........' + str(y_index))
    print('x data size....' + str(x_data_size))
    print('y data size....' + str(y_data_size))
    print('n dimensions...' + str(n_dimensions))
    print('n data.........' + str(n_data))
    print()


data = np.loadtxt('/home/parente/Documents/tese/seagull/codes_output_data/data_1.csv', delimiter=',')
n_data = len(data)
n_dimensions = len(data[0])
#n_dimensions = 4 # para os testes

x_index = 0
y_index = 0

x_data_size = 2 # so para inicializar
y_data_size = 2 # so para inicializar

for x_index in range(n_dimensions):
    for y_index in range(x_index, n_dimensions): # estes dois ciclos dao os indexs que precisamos # tipo so a diagonal superior da matriz tens de encontrar uma maneira melhor de descrever isto
        x_data = data[:, x_index] # é um numpy array
        y_data = data[:, y_index]
        title = "index X " + str(x_index) + " | index Y " + str(y_index)

        debug()

        fig = plt.figure()
        plt.scatter(x_data, y_data, c='black', s=1) # cria um scater plot
        plt.title(title)
        #plt.show()
        fig.savefig(fname = '/home/parente/Documents/tese/seagull/codes_output_data/60_dimension_representation/' + title + '.svg')
        plt.close('all')

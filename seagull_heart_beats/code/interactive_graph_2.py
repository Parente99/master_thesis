#!/usr/bin/python3.7

import matplotlib.pyplot as plt
from matplotlib.widgets import Button # para dar o clear
from matplotlib.text import Annotation # para dar o label quando se carrega


def temp (labels, x_data, y_data):

    def draw_scatterplot(): # desenha o scatter plot
        ax.scatter(
            x_data,
            y_data,
            picker=True,
            c='black', 
            s=1
        )
    

    def onpick(event): # é o detetor do evento carregar no ponto
        ind = event.ind # lista de pontos onde carregaste

        label_pos_x = event.mouseevent.xdata
        label_pos_y = event.mouseevent.ydata

        i = ind[0] # escolho só o primeiro ponto
        label = labels[i]

        print ("index", i, label)

        ax.figure.canvas.draw_idle()
    '''
    def onclick(event):
        ax.cla()

        draw_scatterplot()

        ax.figure.canvas.draw_idle()
    '''

    fig = plt.figure()
    ax = plt.subplot()

    draw_scatterplot()

    fig.canvas.mpl_connect('pick_event', onpick)

    #ax_clear_all = plt.axes([0.0, 0.0, 0.1, 0.05])
    #button_clear_all = Button(ax_clear_all, 'Clear all')


    #button_clear_all.on_clicked(onclick)

    plt.plot()
    print ("scatterplot done")

    plt.show()

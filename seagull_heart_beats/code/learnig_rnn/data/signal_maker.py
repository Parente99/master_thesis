#!/usr/bin/python3.9

# objetivo ter um gerador de sinal que cria uma onda sawtooth
# os pulsos tem o bpm que lhe digo

import numpy as np
import matplotlib.pyplot as plt

def saw_tooth_generator(lenght, peak = 1): # o comprimento do pulso e em samples
    increase = peak / lenght # defino quanto e que o pulso diminui por sample
    pulse = [] # inicializo o pulso
    temp_sample = peak # comeco no maximo do pulso
    for i in range(lenght): # constro o pulso ate cheagar ao comprimento qeu deve ter
        pulse.append(temp_sample)
        temp_sample -= increase
    
    return(pulse)


def sound_maker(bpm, time, sr = 20):
    # o valor do len_pulse e em segundos e tirei o por analise de um ficheiro audio
    period = 60/bpm
    period_samples = int(period * sr)
    
    sound_array = [0] # inicialisa a lista que vai ser o ficheiro de som
    label_array = [0]

    n_samples = int(time * sr) # numero de samples que o som vai ter
    pulse_samples = int(period_samples / 2) # comprimento do pulso
    zeros_samples = int(period_samples / 2)
    zeros = np.zeros(zeros_samples)
    zeros = list(zeros)
    #print('comprimento total pulso..' + str(period_samples))
    #print('comprimento pulso........' + str(lenght_pulse_samples))
    #print('comprimento zeros........' + str(zeros_samples))
    len_sound_array = 0 # so para inicializar
    
    
    while (len_sound_array < n_samples):
        temp_pulse = saw_tooth_generator(pulse_samples)
        #print(temp_pulse)
        #print(zeros)
        sound_array.extend(temp_pulse)
        sound_array.extend(zeros)
        len_sound_array = len(sound_array)

        label_array.extend(list(np.zeros(pulse_samples))) # zeros durante o pulso
        label_array.append(1) # acrescenta 1 que e o label em sim
        label_array.extend(list(np.zeros(zeros_samples-1)))
        print(len_sound_array)
    
    return sound_array, label_array


def main():

    print('introduz o tempo (s):')
    time = int(input())

    print('introduz o bpm')
    bpm = int(input())

    sound_array, label_array = sound_maker(bpm, time)
    plt.plot(sound_array)
    plt.plot(label_array)
    plt.legend(['signal','label'])

    plt.title("saw tooth signal and label")
    plt.xlabel("samples")
    plt.ylabel("intensity")
    plt.show()

    np.savetxt("signal.csv", sound_array, delimiter =",", fmt ='% s')
    np.savetxt("label.csv", label_array, delimiter =",", fmt ='% s')


if __name__ == "__main__":
   main()

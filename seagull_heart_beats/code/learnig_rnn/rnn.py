#!/usr/bin/python3.9

# objetivo fazer a minha primeira RNN e esperar que seja funcional

import numpy as np
from keras.models import Model, Sequential
from keras.layers import LSTM, Input, TimeDistributed, Dense, Activation, Bidirectional
from signal_maker import sound_maker
from tensorflow.keras.optimizers import Adam
from keras.losses import sparse_categorical_crossentropy
import matplotlib.pyplot as plt
import math as ma

bpm = 120
time = 10000 # s

sound_array, label_array = sound_maker(bpm, time)
sound_array = np.asarray(sound_array)
label_array = np.asarray(label_array)
print(sound_array.shape)

# okay entao agrao tenho de criar o array de entrada da camada LSTM
# entao vou ter um array tridimensional a terceira dimensao vai ter as features isto e o conjunto de valores que definem o ponto. depois vou ter a dimensao dos time steps onde vou ter os conjuntos de valores que definem o ponto temporalemnte a seguir e na primeria dimensao vou ter o batch isto e varios conjuntos de grupos temporais
# vou tentar explicar melhor e ao contrario. 
# vamos imaginar cada 5s de som sao identificados por 20 caracteristicas ponho isto num array.
# depois pego no timeframe a seguir e ponho isso noutro array e repito ate ter vamos dizer 20
# agora tenho um array de 20 elementos onde cada elemnto e um array de 20 elementos
# agora decido ter 20 elementos por batch entao ponho 20 elementos num array e depois fico com n elemntos
# o que e que tenho agora?
# tenho um array n*20*20 n batches de 20 elementos de treino sequenciais que sao constituidos por 20 caracteristicas.

# posso fazer direto um np array com [batch, timesteps, feature]

n_characteristics = 1 # numero de caracteristicas que definem o ficheiro
n_time_elements = 50 # numero de time steps com que vou treimar a rede
# okay eu consigo definir estes dois mas agora tenho de calcular quantos batches consigo por
n_trains = 32 # quero fazer 32 treinos por batch
n_data = len(sound_array) # da me o numero de elementos que tenho no total
n_elements_per_batch = n_characteristics * n_time_elements * n_trains

n_batches = ma.floor(n_data / n_elements_per_batch) # numero de batches
# okay a seguir a isto tudo eu devo ter as dimensoes finais do ficheiro de forma a nao dar erros estupidos de arrays incompletos
print(n_batches)

lstm_data = np.empty([n_batches,n_time_elements,n_characteristics]) # okay isto inicializa o array que eu vou por na rede
data_i = 0 # isto e para inicializar as iteracoes no array do som # esta do lado de fora porque nao dava para por por dentro e assim consigo dar o break mais facilmente para evitar merda no final

for t in range(n_batches):
    for e in range(n_time_elements):
        for c in range(n_characteristics):
            lstm_data[t,e,c] = sound_array[data_i] # defino o valor que quero do sound_array e aloco ao ponto correto da lstm data
            data_i += 1 # passo para o ponto seguinte


print(lstm_data.shape)
'''
plt.plot(lstm_data[0,0,:]) # isto da plot das caracteristicas entao deve ser um ponto
plt.show()
plt.cla()
plt.plot(lstm_data[0,:,0]) # isto da plot das caracteristicas ao longo do tempo entao deve ser o pulso
plt.show()
plt.cla()
plt.plot(lstm_data[:,0,0]) # isto da plot de uma caracterista de um timestep ao longo dos batches entao deve ser uma linha
plt.show()
plt.cla()
'''
# tudo ate aqui funciona, funciona e como era esperado, ta fantstico e maravilhoso.

split_index = int(n_data*0.8)

train_data = sound_array[:split_index]
val_data = sound_array[split_index:]
train_resul = label_array[:split_index]
val_resul = label_array[split_index:]


'''
input_shape = (max_sentence_length, 1)
input_sequence = Input(input_shape, name='InputLayer')
rnn = LSTM(256, return_sequences=True, dropout=0.5, name='RNNLayer')(input_sequence)
logits = TimeDistributed(Dense(1), name='TimeDistributed')(rnn)

model = Model(input_sequence, Activation('softmax')(logits))
model.compile(loss=sparse_categorical_crossentropy,
              optimizer=Adam(1e-2),
              metrics=['accuracy'])


sound_array = sound_array.reshape(*sound_array.shape, 1)
label_array = label_array.reshape(*label_array.shape, 1)

model_results = model.fit(sound_array, label_array, batch_size=30, epochs=10)

'''


sound_array = np.expand_dims(sound_array, 1)

regressor = Sequential()
regressor.add(Input(shape=lstm_data.shape))
regressor.add(LSTM(units = 200, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(LSTM(units = 50, return_sequences = True))
regressor.add(Dropout(0.2))
regressor.add(TimeDistributed( Dense(units = 1)))


regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

regressor.fit(sound_array, label_array, epochs = 100)

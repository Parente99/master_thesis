#!/usr/bin/python3.9

# este vai dar o plot de dados falsos onde sei os bpm
# nao vai ter funcoes

from mfccs_plot_input_sound import mfcc_plot_input_sound
import matplotlib.pyplot as plt


names = [
    'real',
    'fake'
]

files = [
    '/home/parente/Documents/tese/seagull_heart_beats/data/wav/eggs_giulia/egg15/0/FILE10.WAV',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/data/fake_beat_urban_noise_t-7200_bpm-150.wav'
]

n_files = len(files)

for i in range(n_files):
    path = files[i]
    mfcc_plot_input_sound(path)

plt.legend(names)
plt.title('MFCC average and standard deviation as error', fontsize=8)
plt.xlabel('MFCC. 30 avereges and 30 standard deviations', fontsize=8)
plt.ylabel('Value', fontsize=8)
plt.show()
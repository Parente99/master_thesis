#!/usr/bin/python3.9

# entao este vai ser um stand alone code que a unica coisa que vai fazer e a representacao dos mfcc
# honestamente isto e so para satisfazer as vontades do filipe
# entao tenho o som
# passo para mfcc MAS nao sao varias matrizes e so uma muito longa
# entao fico com uma matriz 30*n
# desta matriz faco o histograma
# repito para cada bpm

import numpy as np
import matplotlib.pyplot as plt
import librosa as lb

names = [
    '100',
    '120',
    '140',
    '160',
    '180',
    '200'
]

files = [
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-100.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-120.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-140.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-160.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-200.wav'
]

n_files = len(files)
n_mel_coe = 30
total_mfcc = []

for i in range(n_files):
    path = files[i]
    audio_file, sample_rate = lb.load(path, sr=None)  
    mfcc = lb.feature.mfcc(y=audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS
    print(mfcc.shape)
    total_mfcc.append(mfcc)

total_mfcc = np.asarray(total_mfcc)
print(total_mfcc.shape)
# okay array 1 = ficheiro 2 = frequencia 3 = tempo # espero

n_bins = 20

for i in range(n_mel_coe):
    for j in range(n_files):
        print(j)
        print(i)
        temp_data = total_mfcc[j,i,:]
        print(temp_data.shape)


        hist, bin_edges = np.histogram(temp_data, bins=n_bins)  # output is two arrays
        # Scatter plot
        # Now we find the center of each bin from the bin edges
        bins_mean = [0.5 * (bin_edges[i] + bin_edges[i+1]) for i in range(len(hist))]
        plt.plot(bins_mean, hist)

    plt.legend(names)
    plt.title('Histogramas do conjunto de frequencias ' + str(i) + ' dos MFCC para cada BPM', fontsize=8)
    plt.xlabel('Valores que a cada conjunto de frequencias toma', fontsize=8)
    plt.ylabel('Numero de ocorrencias', fontsize=8)
    plt.savefig('histogramas_do_conjunto_de_frequencias_' + str(i) + '_dos_mfcc_para_cada_bpm')
    plt.clf()

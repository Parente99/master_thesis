#!/usr/bin/python3.9

# exatamente o mesmo que o data_processing_2 faz mas com funcoes mais bonitas
# clonei para nao quebrar o outro
#
# este codigo e para tratar os sons das gaivotas
# vou pegar no som de X min e tirar uma slice de Ys avancar Zs e tirar outra slice (Y > Z)
# depois mfccs normais
# array 61*n com 30 medias e 30 desvios padrao e no inicio um identificador a dizer a ocalizaçao do ficherio e a slice de tempo em questao

# mas este vai ser tudo com funcoes para poder chamar quando quero
# inputs: caminho ate ao ficheiro de som e np array dos dados. sim este ja precisa de estar iniciado
# outputs: ficheiro data mas com mais dados.

import librosa as lb
import numpy as np
import pandas as pd
import time
import math as ma
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1


def mfccs_extractor(audio_file, sample_rate): # honestamente e mais audio array

    # inicializacoes
    win_len = 10080 # tamanho da janela em samples
    hop_len = int(win_len/2) # stride em samples
    #hop_len = win_len # stride em samples # so para a analise pela rede pre terinada
    n_mel_coe = 15
    first_loop = False
    data=[]
    i = 0 # iterador

    while True:
        #print('comecou a tratar')
        # vai arranjar o intervalo de tempo que vai analisar
        start = i * hop_len # tempo de inicio da time slice
        end = (i * hop_len) + win_len # tempo de fim da time slice
        i = i + 1

        temp_audio_file = audio_file[start:end] # audio da time slice
        if len(temp_audio_file) < win_len: # quando o tamanho do ficheiro temporario for menor que o da janela ja chegou ao fim do ficheiro
            break
        
        temp_audio_file = normalizer_av0_max1(temp_audio_file)

        # tira osmfccs desse elemnto audio
        MFCCS = lb.feature.mfcc(y=temp_audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS 
        MFCCS_mean = np.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
        MFCCS_std = np.std(MFCCS.T, axis=0)  # desvio padrao
        temp_arr = np.concatenate([MFCCS_mean,MFCCS_std])

        if first_loop: # verifica se ja passou pelo primeiro loop
            data = np.vstack((data, temp_arr)) # se sim entao o array ja esta iniciado e e so adicionar em baixo
        else:
            data = temp_arr # se nao entao e preciso inicializar
            first_loop = True
        '''
        print('size temp file... ', len(temp_audio_file))
        print('frame start...... ', start)
        print('frame end........ ', end)
        print('data dimensions.. ', len(temp_arr))
        print('data size........ ', len(data))
        print('shape mfcc....... ', MFCCS.shape)
        print()
        '''
    
    #print('fim ficheiro audio')
    return data


def real_mfccs_extractor(audio_file, sample_rate): # honestamente e mais audio array

    # inicializacoes
    win_len = 10000 # tamanho da janela em samples
    hop_len = int(win_len/1) # stride em samples
    n_mel_coe = 30
    first_loop = False
    data=[]
    i = 0 # iterador

    while True:
        #print('comecou a tratar')
        # vai arranjar o intervalo de tempo que vai analisar
        start = i * hop_len # tempo de inicio da time slice
        end = (i * hop_len) + win_len # tempo de fim da time slice
        i = i + 1

        temp_audio_file = audio_file[start:end] # audio da time slice
        if len(temp_audio_file) < win_len: # quando o tamanho do ficheiro temporario for menor que o da janela ja chegou ao fim do ficheiro
            break
        
        temp_audio_file = normalizer_av0_max1(temp_audio_file)

        # tira osmfccs desse elemnto audio
        MFCCS = lb.feature.mfcc(y=temp_audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS 

        if first_loop: # verifica se ja passou pelo primeiro loop
            data = np.vstack((data, MFCCS)) # se sim entao o array ja esta iniciado e e so adicionar em baixo
        else:
            data = MFCCS # se nao entao e preciso inicializar
            first_loop = True
        '''
        print('size temp file... ', len(temp_audio_file))
        print('frame start...... ', start)
        print('frame end........ ', end)
        print('data dimensions.. ', len(temp_arr))
        print('data size........ ', len(data))
        print('shape mfcc....... ', MFCCS.shape)
        print()
        '''
    
    #print('fim ficheiro audio')
    return data

def mfccs_extractor_pandas(full_path):
    print('ficheiro')
    print(full_path)
    # pontos de divergencia do outro codigo: este vai ser processado em pandas. vai ter 3 labels separados, path ate ao ficheiro, tempo de inicio, tempo de fim. e estes vao estar no final

    # inicializacoes
    win_len = 5 # janela em segundos
    hop_len = 2.5 # quanto avanca em seg
    n_mel_coe = 30
    data_header = ['av1', 'av2', 'av3', 'av4', 'av5', 'av6', 'av7', 'av8', 'av9', 'av10', 'av11', 'av12', 'av13', 'av14', 'av15', 'av16', 'av17', 'av18', 'av19', 'av20', 'av21', 'av22', 'av23', 'av24', 'av25', 'av26', 'av27', 'av28', 'av29', 'av30', 'sd1', 'sd2', 'sd3', 'sd4', 'sd5', 'sd6', 'sd7', 'sd8', 'sd9', 'sd10', 'sd11', 'sd12', 'sd13', 'sd14', 'sd15', 'sd16', 'sd17', 'sd18', 'sd19', 'sd20', 'sd21', 'sd22', 'sd23', 'sd24', 'sd25', 'sd26', 'sd27', 'sd28', 'sd29', 'sd30', 'path_to_file', 'start_time', 'end_time']
    data = pd.DataFrame(columns=data_header) # inicializa o dataframe data

    print('esta a transformar o wav num array')
    audio_file, sample_rate = lb.load(full_path)  # isto da me o ficheiro em array e o frame rate

    i = -1
    while True: # vai iterar ate quebrar
        i += 1
        try:
            print('comecou a tratar')
            # vai arranjar o intervalo de tempo que vai analisar
            start = i * hop_len # tempo de inicio da time slice
            end = (i * hop_len) + win_len # tempo de fim da time slice

            # passando para frames
            start_frame = lb.time_to_samples(start, sr=sample_rate)
            end_frame = lb.time_to_samples(end, sr=sample_rate)
            temp_audio_file = audio_file[start_frame:end_frame] # audio da time slice
            # tira osmfccs desse elemnto audio
            MFCCS = lb.feature.mfcc(y=temp_audio_file, sr=sample_rate, n_mfcc=n_mel_coe)  # da me os MFCCS
            print(MFCCS.shape)
            MFCCS_mean = np.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
            MFCCS_std = np.std(MFCCS.T, axis=0)  # desvio padrao
            temp_arr = np.concatenate([MFCCS_mean,MFCCS_std]) # aqui tenho um numpy array com 30 medias seguidas de 30 desvios padrao

            temp_arr = temp_arr.tolist() # passa para lista
            labels = [full_path, start, end] # cria uma lista dos labels
            temp_arr.extend(labels) # adiciono os lables no final desta lista. agora tenho a row completa
            temp_arr = pd.DataFrame( temp_arr, columns=['0']) # passa para data frame mas como e uma lista entao da uma coluna
            temp_arr = temp_arr.T # que e preciso fazer o transpose # funciona
            temp_arr.columns = data_header
            data = data.append(temp_arr, ignore_index = True) # adiciona tudo ao dataframe data
            print('forma data')
            print(data.shape)

        except Exception as e:
            return data
            del data # apaga a variavel porque tenho medo de fantasmas
            print('erro............. ',str(e))
            print('fim ficheiro audio')
            break # chegou ao final do ficheiro e vai dar um erro do frame pedido ser maior que os frames existentes

def main():
    print('diz qual e o caminho ate ao ficheiro')
    path = str(input())
    print('diz qual e o label (tem de ser um inteiro)')
    label = int(input())
    print('que nome queres dar ao ficheiro')
    name = str(input())
    name = 'data/' + name
    start_time = time.time()

    print('vai abrir o ficheiro audio')
    audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate
    print(sample_rate)

    data = mfccs_extractor(audio_file, sample_rate)
    label_array = np.ones([len(data),1])
    label_array = label_array * label

    data = np.hstack((data, label_array))

    np.savetxt(name + ".csv", data, delimiter=",")

    execution_time = (time.time() - start_time)
    print('Execution time in seconds: ' + str(execution_time))



if __name__ == "__main__":
   main()

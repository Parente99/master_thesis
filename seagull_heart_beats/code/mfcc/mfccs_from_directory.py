#!/usr/bin/python3.9

# Inputs: diretoria que tenha ficheiros wav
# Output: ficheiro csv com os mfccs

import os
from mfccs_extractor import mfccs_extractor_pandas
from path_searcher import search_files
import numpy as np
import pandas as pd
import time

# here you enter the path to the directory
path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/eggs_giulia'
extension = '.WAV'

data_header = ['av1', 'av2', 'av3', 'av4', 'av5', 'av6', 'av7', 'av8', 'av9', 'av10', 'av11', 'av12', 'av13', 'av14', 'av15', 'av16', 'av17', 'av18', 'av19', 'av20', 'av21', 'av22', 'av23', 'av24', 'av25', 'av26', 'av27', 'av28', 'av29', 'av30', 'sd1', 'sd2', 'sd3', 'sd4', 'sd5', 'sd6', 'sd7', 'sd8', 'sd9', 'sd10', 'sd11', 'sd12', 'sd13', 'sd14', 'sd15', 'sd16', 'sd17', 'sd18', 'sd19', 'sd20', 'sd21', 'sd22', 'sd23', 'sd24', 'sd25', 'sd26', 'sd27', 'sd28', 'sd29', 'sd30', 'path_to_file', 'start_time', 'end_time']
full_data = pd.DataFrame(columns=data_header) # inicializa o dataframe full_data

start_time = time.time()

paths_to_files = search_files(path, extension)

for path_to_file in paths_to_files:
    data = mfccs_extractor_pandas (path_to_file)
    full_data = full_data.append(data, ignore_index = True)
    print('forma full data')
    print(full_data.shape)

full_data.to_csv('../data/csv/eggs_guilia.csv')

execution_time = (time.time() - start_time)
print('Execution time in seconds: ' + str(execution_time))

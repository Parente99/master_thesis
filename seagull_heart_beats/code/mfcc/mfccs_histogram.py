#!/usr/bin/python3.9

# Okay a parte dificil deste codigo vai ser a represenctacao
# eu tenho cada som vai dar 60 graficos e eu tenho 6 sons
# entao estamos a falar de 360 graficos
# cada grafico tem de ter 6 histogramas para representar as 60 cenas
# tenho de representar os hitoramas como scater plot

import numpy as np
import matplotlib.pyplot as plt
import librosa as lb
from mfccs_extractor import mfccs_extractor, real_mfccs_extractor

'/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_long_files/noise_quiet/001.wav'
'/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_long_files/beats/007.wav'

path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_long_files/noise_quiet/001.wav'
audio_file, sample_rate = lb.load(path, sr=None)  
# isto da me o ficheiro em array e o frame rate
data = mfccs_extractor(audio_file, sample_rate)
# passa para mfcc
av_data = np.average(data, axis=0)
sd_data = np.std(data, axis=0)
x = np.arange(len(data[0]))

number_of_coeficients = len(data[0])/2 # da me o numero de coeficientes para este caso (medias + desvio padrao)

label_array_numbers = [format(i, '02') for i in range(1, 31)] # da me uma lista de 01 a 30
#print(label_array_numbers)

#label_array_numbers = np.arange(len(data[0])/2) # isto faz um array de 0 ate 29 em principio
#label_array_numbers = label_array_numbers + 1 # adiciona 1 para ser de 1 a 30
#label_array_numbers = list(label_array_numbers) # passa para lista para ser mais facil de trabalhar
#label_array_numbers = [int(item) for item in label_array_numbers] # passa para int para tirar o .0 do fim
#label_array_numbers = [str(item) for item in label_array_numbers] # passa para string para poder adicionar no inicio um identificador

avg_label = 'avg coef '
labels_list_avg = [avg_label + item for item in label_array_numbers] # adiciona o identificador de media no incio
#print(labels_list_avg)


std_label = 'std coef '
labels_list_std = [std_label + item for item in label_array_numbers] # adiciona o identificador de media no incio
#print(labels_list_std)

labels = labels_list_avg + labels_list_std

plt.figure(figsize=(10, 6))
plt.errorbar(x, av_data, yerr=sd_data, fmt="o")
plt.xticks(x, labels, rotation ='vertical', fontsize=8)
plt.title('Features averages. Features standard deviations as error', fontsize=8)
plt.xlabel('Features 30 averages and 30 standard deviations', fontsize=8)
plt.ylabel('Value', fontsize=8)
plt.subplots_adjust(bottom=0.3)
#plt.show()
plt.savefig('mfcc_distribution_plot_noise.jpg')

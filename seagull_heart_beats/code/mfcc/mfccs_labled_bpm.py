#!/usr/bin/python3.9

# okay o que e que este codigo vai fazer
# input um tempo
# output ficheiro csv com 61*n
# 60 dimensoes vao ser as caracteristicas do som e o label e o bpm
# bpm vai ser escolhido aleatoriamente dentro dum range
# tenho de mandar estas duvidas todas a alguem de biologia

import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker') 
# isto e para conseguir invocar o codigo que faz o ficheiro de som
# basicamente adiciona o path ate a pasta do ficheiro que quero a lista de sitios onde procura codigo
from fake_signal_generator import fake_signal_generator
# tou a importar a funcao que me faz o sinal
from mfccs_extractor import mfccs_extractor
# importo a funcao que passa de onda sonora para mfcc
import numpy as np
import math as ma
import random as rd

def mfccs_labled_bpm(time):
    # okay esta vai ser a funcao principal
    first_loop = False
    snr_range = [50, 110] 
    # foram valores mandados um pouco ao calhas
    bpm_range = [100, 200]
    # espectro de bpms por onde pode escolher, so inteiros thou
    noise_type_range = [1, 2, 3, 4, 5]
    sound_sample_time = 30
    # numero de segundos que cada segmento de som com ruido constante vai ter
    n_loop_iterations = ma.ceil(time/sound_sample_time)
    # isto da me o numero de loops que precisam de haver para chegar ao tempo necessario
    # o ceil e para arredondar para cima sempre
    
    for i in range(n_loop_iterations):
        snr = rd.randint(snr_range[0], snr_range[1])
        # escolhe o snr
        bpm = rd.randint(bpm_range[0], bpm_range[1])
        # escolhe o bpm
        noise_type = rd.choice(noise_type_range)
        # escolhe o tipo de ruido
    
        sound_sample, sample_rate = fake_signal_generator(sound_sample_time, snr, bpm, noise_type)
        # okay criou o array de som e a sample rate
        mfcc_temp = mfccs_extractor(sound_sample, sample_rate)
        print()
        # aqui vou passar para mfcc
        # deve ser um output em numpy array
        n_elements = mfcc_temp.shape[0]

        bpm_label = np.ones([n_elements,1]) * bpm
        # isto cria me o array que e o label dos bpms para cada elemento
        # funciona
        mfcc_temp = np.hstack((mfcc_temp, bpm_label))
        
        if first_loop: 
        # verifica se ja passou pelo primeiro loop
            data = np.vstack((data, mfcc_temp)) 
            # se sim entao o array ja esta iniciado e e so adicionar em baixo
        else:
            data = mfcc_temp 
            # se nao entao e preciso inicializar
            first_loop = True
        
        print('sample_rate' + str(sample_rate))
        print('n_elements'+ str(n_elements))
        print('bpm' + str(bpm))
        print('data'+ str(data))
        print('data shape'+ str(data.shape))

        print('esta ' + str(round(i/n_loop_iterations,2)*100) + ' completo')
    
    return(data)

def main():
    print("insert the minimum time for the fake sound")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo
    print('say the name of the file')
    name = input()

    data = mfccs_labled_bpm(time)

    np.savetxt(name + ".csv", data, delimiter=",")

if __name__ == "__main__":
   main()

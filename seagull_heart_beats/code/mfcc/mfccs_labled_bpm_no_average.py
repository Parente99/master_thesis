#!/usr/bin/python3.9

# okay o que e que este codigo vai fazer
# input um tempo
# output ficheiro csv com 21*n
# 20 dimensoes vao ser as caracteristicas do som e o label e o bpm
# bpm vai ser escolhido aleatoriamente dentro dum range
# tenho de mandar estas duvidas todas a alguem de biologia

# mais algumas explicacoes. nao vao haver medias e 60 caracteristicas tiradas de 5s
# vao haver 20 caracteristicas retiradas de 0.0625s
# okay explicando este numero cada pulso tem uma duracao <= 500 frames e eu quero que ele faca 3+ analises por pulso entao
# 1s -> 2016 frames. 1/16s -> 126 frames. 500/126 = 3.9

import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker') 
# isto e para conseguir invocar o codigo que faz o ficheiro de som
# basicamente adiciona o path ate a pasta do ficheiro que quero a lista de sitios onde procura codigo
from fake_signal_generator import fake_signal_generator
# tou a importar a funcao que me faz o sinal

import librosa as lb
import numpy as np
import math as ma
import random as rd
from matplotlib import pyplot as plt
from matplotlib.ticker import MaxNLocator # para os labels dos axis ficarem int

def mfccs_labled_bpm_no_noise(time):
    # okay esta vai ser a funcao principal
    # o tempo e em segundos
    first_loop = False
    bpm_range = [100, 200]
    # espectro de bpms por onde pode escolher, so inteiros thou
    sound_sample_time = 30
    # numero de segundos que cada segmento de som com ruido constante vai ter
    n_loop_iterations = ma.ceil(time/sound_sample_time)
    # isto da me o numero de loops que precisam de haver para chegar ao tempo necessario
    # o ceil e para arredondar para cima sempre
    
    for i in range(n_loop_iterations):
        snr = 1000
        # snr de 1000 que e o equivalente a nao ter ruido
        bpm = rd.randint(bpm_range[0], bpm_range[1])
        # escolhe o bpm
        noise_type = 1
        # so ruido branco para ser constante
    
        sound_sample, sample_rate = fake_signal_generator(sound_sample_time, snr, bpm, noise_type)
        # okay criou o array de som e a sample rate

        # e aqui vou extrair os mfcc
        n_mel_coe = 20
        win_length = 128 # o motivo esta explicado no comentario inicial
        hop_length = 64 # metade do outro
        mfcc = lb.feature.mfcc(y=sound_sample, sr=sample_rate, n_mfcc=n_mel_coe, hop_length=hop_length, win_length=win_length)
        ax = plt.figure().gca()
        ax.imshow(mfcc[:, :80])
        ax.set_title("MFCC")
        ax.set_ylabel("Frequency division")
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_xlabel("Time with a 64 sample stride")
        plt.show()
        mfcc = mfcc.T

        # aqui vou passar para mfcc
        # deve ser um output em numpy array
        n_elements = mfcc.shape[0]
        print(mfcc.shape)

        bpm_label = np.ones([n_elements,1]) * bpm
        # isto cria me o array que e o label dos bpms para cada elemento
        # funciona
        mfcc = np.hstack((mfcc, bpm_label))
        
        if first_loop: 
        # verifica se ja passou pelo primeiro loop
            data = np.vstack((data, mfcc)) 
            # se sim entao o array ja esta iniciado e e so adicionar em baixo
        else:
            data = mfcc 
            # se nao entao e preciso inicializar
            first_loop = True
        
        print('sample_rate' + str(sample_rate))
        print('n_elements'+ str(n_elements))
        print('bpm' + str(bpm))
        print('data'+ str(data))
        print('data shape'+ str(data.shape))

        print('esta ' + str(round(i/n_loop_iterations,2)*100) + ' completo')
    
    return(data)

def main():
    print("insert the minimum time for the fake sound")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo
    print('say the name of the file')
    #name = input()

    data = mfccs_labled_bpm_no_noise(time)

    #np.savetxt(name + ".csv", data, delimiter=",")

if __name__ == "__main__":
   main()

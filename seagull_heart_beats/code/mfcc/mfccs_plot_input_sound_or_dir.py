#!/usr/bin/python3.9

# Okay como e que vou fazer este codigo
# input ficheiro de som
# output histograma dos varios mfccs desse som
# e tmb grafico com o valor medio por mfcc

# pseudo codigo
# entro com um ficheiro de som
# retiro os 60 * n mfcc
# faco a media linha a linha
# faco os histogramas # talvez ainda nao

import librosa as lb
from mfccs_extractor import mfccs_extractor
from mfccs_extractor import real_mfccs_extractor
import numpy as np
import matplotlib.pyplot as plt
from path_searcher import search_files

def mfcc_plot_input_sound(path_to_sound):
	audio_file, sample_rate = lb.load(path_to_sound, sr=None)  
	# isto da me o ficheiro em array e o frame rate

	data = mfccs_extractor(audio_file, sample_rate)
	# passa para mfcc

	av_data = np.average(data, axis=0)
	sd_data = np.std(data, axis=0)
	x = np.arange(len(data[0]))

	plt.errorbar(x, av_data, yerr=sd_data, fmt="o")

def mfcc_only_dir_input(path_to_dir):
	paths_to_files = search_files(path_to_dir, '.WAV')
	first_loop = False	

	for path_to_file in paths_to_files:
		audio_file, sample_rate = lb.load(path_to_file, sr=None)  
		temp_arr = mfccs_extractor(audio_file, sample_rate)
	
		if first_loop: # verifica se ja passou pelo primeiro loop
			data = np.vstack((data, temp_arr)) # se sim entao o array ja esta iniciado e e so adicionar em baixo
		else:
			data = temp_arr # se nao entao e preciso inicializar
			first_loop = True
		print(data.shape)

	av_data = np.average(data, axis=0)
	sd_data = np.std(data, axis=0)
	print(av_data.shape)
	print(sd_data.shape)
	x = np.arange(len(data[0]))

	plt.errorbar(x, av_data, yerr=sd_data, fmt="o")

def main():
	print('path(1) OR dir(2)?')
	choice = int(input())
	if choice == 1:
		print('path to file')
		path = input()
		
		mfcc_plot_input_sound(path)

	if choice == 2:
		print('dis o caminho ate a diretoria')
		path = input()

		mfcc_only_dir_input(path)
	
	plt.title('Averages and standard deviations of the features')
	plt.xlabel('Features')
	plt.ylabel('Quefrency value')
	plt.show()


if __name__ == "__main__":
	main()




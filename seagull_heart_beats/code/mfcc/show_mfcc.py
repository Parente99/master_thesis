#!/usr/bin/python3.9


import matplotlib.pyplot as plt
import numpy as np


path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/perfect_beats_t-7200_bpm-150.csv'
full_data_1 = np.loadtxt(path_1, delimiter=',')
path_2 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/urban_sounds_var_noise_t-7200.csv'
full_data_2 = np.loadtxt(path_2, delimiter=',')

plt.imshow(full_data_1, origin='lower')
plt.show()
plt.imshow(full_data_2, origin='lower')
plt.show()

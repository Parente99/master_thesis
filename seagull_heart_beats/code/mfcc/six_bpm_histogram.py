#!/usr/bin/python3.9

# Okay a parte dificil deste codigo vai ser a represenctacao
# eu tenho cada som vai dar 60 graficos e eu tenho 6 sons
# entao estamos a falar de 360 graficos
# cada grafico tem de ter 6 histogramas para representar as 60 cenas
# tenho de representar os hitoramas como scater plot

import numpy as np
import matplotlib.pyplot as plt
import librosa as lb
from mfccs_extractor import mfccs_extractor, real_mfccs_extractor

names = [
    '100',
    '120',
    '140',
    '160',
    '180',
    '200'
]

files = [
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-100.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-120.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-140.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-160.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-200.wav'
]

n_files = len(files)
n_dimensions = 60
all_files_data = []

for i in range(n_files):
    path = files[i]
    audio_file, sample_rate = lb.load(path, sr=None)  
    # isto da me o ficheiro em array e o frame rate
    data = real_mfccs_extractor(audio_file, sample_rate)
    # passa para mfcc
    all_files_data.append(data)
    # entao isto vai ser um array de arrays de arrays # all_files_data[x,y,z] x-especifica o file y-e o elemento x-e a caracteristica/dimensao # ta correto
all_files_data = np.asarray(all_files_data)
#print(all_files_data.shape)
# para fazer o histograma tenho de andar por todos os elementos
# depois passo para o file seguinte
# depois faco o histograma
# depois passo para a dimensao seguinte

n_bins = 20

for z in range(n_dimensions):
    for x in range(n_files):
        element_array = all_files_data[x,:,z]
        # isto sao todos os elementos da mesma dimensao e mesmo ficheiro
        print(element_array)
        # Usual histogram plot

        hist, bin_edges = np.histogram(element_array, bins=n_bins)  # output is two arrays
        # Scatter plot
        # Now we find the center of each bin from the bin edges
        bins_mean = [0.5 * (bin_edges[i] + bin_edges[i+1]) for i in range(len(hist))]
        plt.plot(bins_mean, hist)

    plt.legend(names)
    plt.title('Histogramas da dimensao ' + str(z) + ' para cada BPM', fontsize=8)
    plt.xlabel('Valores que a dimensao toma', fontsize=8)
    plt.ylabel('Numero de ocorrencias', fontsize=8)
    plt.savefig('histogramas_da_dimensao_' + str(z) + '_para_cada_bpm')
    plt.clf()

#!/usr/bin/python3.9

# este vai dar o plot de dados falsos onde sei os bpm
# nao vai ter funcoes

from mfccs_plot_input_sound import mfcc_plot_input_sound
import matplotlib.pyplot as plt


names = [
    '100',
    '120',
    '140',
    '160',
    '180',
    '200'
]

files = [
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-100.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-120.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-140.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-160.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-200.wav'
]

n_files = len(files)

for i in range(n_files):
    path = files[i]
    mfcc_plot_input_sound(path)

plt.legend(names)
plt.title('MFCC average and standard deviation as error, depending on BPM', fontsize=8)
plt.xlabel('MFCC. 30 avereges and 30 standard deviations', fontsize=8)
plt.ylabel('Value', fontsize=8)
plt.show()
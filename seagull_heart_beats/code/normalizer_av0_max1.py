#!/usr/bin/python3.9

# okay neste codigo vou entrar com um matriz e o output vai ser uma com media 0 e maximo absoluto 1
# como e que faco isso
# okay primeiro e calcular a media e subtrair
# fazer o abs do array
# ver o maximo
# dividir o array sem abs pelo maximo
# podia tmb fazer um max min inverter o min e ver qual dos dois o maior
# nao parece tao bom

import numpy as np
import matplotlib.pyplot as plt
import librosa as lb

def normalizer_av0_max1 (array):
    average = np.average(array)
    # calcula a media
    array = array - average
    # poe o array com media 0
    k = max(np.absolute(array))
    # calcula o fator de normalizacao
    array = array / k
    # normaliza para o max absoluto ser 1
    return array

def main ():

    print('diz qual e o caminho ate ao ficheiro wav')
    path = str(input())

    audio_file, frame_rate = lb.load(path, sr=None)

    x_array = []
    for i in range(len(audio_file)):
        x_array.append(i)

    array = normalizer_av0_max1(audio_file)

    plt.scatter(x_array, audio_file, s=1)
    plt.scatter(x_array, array, s=1)
    plt.legend(["audio" , "normalized"])
    plt.show()

if __name__ == "__main__":
   main()
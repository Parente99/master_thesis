#!/usr/bin/python3.9

# inputs. uma diretoria e uma extensao
# outputs. lista com o path para todos os ficheiros com essa extensao
# funciona recursivamente
# e case sensitive

import os

def search_files(directory, extension): # inputs diretoria e extensao
    file_path_list = []
    for dirpath, dirnames, files in os.walk(directory):
        '''
        print(dirpath) # da o path ate uma diretoria
        print(dirnames) # lista as diretorias dentro dessa
        print(files) # lista os ficheiros dentro dessa
        print()
        '''
        for name in files:
            if name.endswith(extension):
                path = os.path.join(dirpath, name)
                #print(path)
                file_path_list.append(path)
    #print(file_path_list)
    return(file_path_list)

# isto e para o codigo correr em vazio
def main(): # manda ca para fora todos os dados 
    print(search_files('.','py'))

if __name__ == "__main__":
   main()
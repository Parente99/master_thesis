#!/usr/bin/python3.9

# entao qual e o meu objetivo
# ter um codigo em que entro com um array de sons e o output e um grafico com eles todos
# scater e com labels

# primeiro passar de som para mfcc
# passar todos os sons para mfcc
# juntar tudo e encontrar as matrizes do pca
# passar tudo pelo pca
# dar plot do que der
# isto vai ser um belo dum dicionario


import librosa as lb
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/')
from mfccs_extractor import mfccs_extractor
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/pca/')
from get_pca_matrixes_2 import geting_the_matrixes
from pca_through_set_matrixes import dimension_reduction_2

names = [
    '100',
    '120',
    '140',
    '160',
    '180',
    '200'
]

files = [
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-100.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-120.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-140.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-160.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-180.wav',
    '/home/parente/Documents/tese/seagull_heart_beats/code/fake_signal_maker/fake_beat_t-2000_snr-1000_bpm-200.wav'
]

n_files = len(files)
all_data = []
first_loop = False

# calcular matrizes pca
# primeiro cirar um array com todos os dados
for i in range(n_files):
    path = files[i]
    audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate

    mfcc_temp = mfccs_extractor(audio_file, sample_rate)
    # da me os mfccs

    if first_loop: 
    # verifica se ja passou pelo primeiro loop
        all_data = np.vstack((all_data, mfcc_temp)) 
        # se sim entao o array ja esta iniciado e e so adicionar em baixo
    else:
        all_data = mfcc_temp 
        # se nao entao e preciso inicializar
        first_loop = True

mean_matrix, eigenvector_subset, new_reduced_point = geting_the_matrixes(all_data, final_dim = 2)

# agora reduzir e dar plots

plt.figure()
for i in range(n_files):
    path = files[i]
    audio_file, sample_rate = lb.load(path, sr=None)  # isto da me o ficheiro em array e o frame rate

    mfcc_temp = mfccs_extractor(audio_file, sample_rate)

    fake_beats_pca = dimension_reduction_2(mfcc_temp, mean_matrix, eigenvector_subset)


    plt.scatter(fake_beats_pca[:, 0], fake_beats_pca[:, 1], s=1)
    print(names[i])

print('x = ', new_reduced_point[0])
print('y = ', new_reduced_point[1])

plt.legend(names)
plt.show()

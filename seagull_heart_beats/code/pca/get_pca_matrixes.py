#!/usr/bin/python3.9

# entao para este codigo vou extrair as matrizes de media, desvio padrao e covariancia necessarias para fazer a reducao de dimansoes pelo algoritmo pca
# toda a aplicacao e a partir de codigo fonte porque e a unica maneira de o fazer
# embora este codigo esteja preparado para poder fazer tambem a reducao de dimensoes vou deixar isso para outro

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import csv
import os

n_components = 2 # quero isto para iniciar

data_header = ['av1', 'av2', 'av3', 'av4', 'av5', 'av6', 'av7', 'av8', 'av9', 'av10', 'av11', 'av12', 'av13', 'av14', 'av15', 'av16', 'av17', 'av18', 'av19', 'av20', 'av21', 'av22', 'av23', 'av24', 'av25', 'av26', 'av27', 'av28', 'av29', 'av30', 'sd1', 'sd2', 'sd3', 'sd4', 'sd5', 'sd6', 'sd7', 'sd8', 'sd9', 'sd10', 'sd11', 'sd12', 'sd13', 'sd14', 'sd15', 'sd16', 'sd17', 'sd18', 'sd19', 'sd20', 'sd21', 'sd22', 'sd23', 'sd24', 'sd25', 'sd26', 'sd27', 'sd28', 'sd29', 'sd30']
example_point = data_header[:]

data_complete = pd.read_csv('../data/csv/eggs_guilia.csv', index_col=0) # aqui indico o ficeiro csv ao qual vou buscar os dados # com indexes e colum names tal como pandas quer (isto estava me a eliminar o primeiro ponto...)
print(data_complete)
data = data_complete.copy()
del data['path_to_file'] # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
del data['start_time']
del data['end_time']
print(data)
print(data.dtypes)


mean_matrix = data.mean(axis=0) # funciona bem mas agora e uma coluna
print(mean_matrix)
#print(mean_matrix.shape)
standard_deviation_matrix = data.std(axis=0)
print(standard_deviation_matrix)
meaned_data = data - mean_matrix # isto centra os dados em 0 # somehow this fucking works as well
print(meaned_data)
normalized_data = meaned_data/standard_deviation_matrix

covariance_matrix = normalized_data.cov() # convariancias acho que faz sentido
#print(covariance_matrix)

# numpy blob
covariance_matrix.to_numpy()
eigen_values , eigen_vectors = np.linalg.eigh(covariance_matrix) # valores e vetores proprios
# os valore proprios vao nos dar a ordem de relevancia de cada nova dimensao que é determinada pelos vetores proprios

sorted_index = np.argsort(eigen_values)[::-1] # ordem de relevancia dos valores proprios

sorted_eigenvalue = eigen_values[sorted_index] # autoexplicativo
sorted_eigenvalue_0_1 = (sorted_eigenvalue - sorted_eigenvalue.min())/(sorted_eigenvalue.max() - sorted_eigenvalue.min()) # assim fica a variar entre 0 e 1, é mais facil de analizar relevancias
sorted_eigenvectors = eigen_vectors[:,sorted_index]

# reduçao em si
eigenvector_subset = sorted_eigenvectors[:,0:n_components]

eigenvector_subset = pd.DataFrame(eigenvector_subset, columns = ['dim_1','dim_2'], index = example_point) # passa para data frame
#print(eigenvector_subset)

# data_reduced = np.dot(eigenvector_subset.transpose() , normalized_data.transpose() ).transpose() # esta é a parte dareducao em si onde ha a perda de informacao

# descriçao de o que cada eixo significa

mean_matrix_str = mean_matrix.values.tolist() # passa para lista

normalized_example_point = []

 # isto poe o ponto falso de 60 dimensoes na posicão centrada isto é sobre a atuaçao da media de todos os pontos
for i in range(len(example_point)):
    temp_mean = mean_matrix[i]
    temp_sd = standard_deviation_matrix[i]

    if temp_mean < 0:
        temp_mean *= -1
        temp_mean = str(temp_mean)
        normalized_example_dimesion = example_point[i] + ' + ' + temp_mean # como é negativo soma porque a media é subtraida

    else:
        temp_mean = str(temp_mean)
        normalized_example_dimesion = example_point[i] + ' - ' + temp_mean

    normalized_example_dimesion = '( ' + normalized_example_dimesion + ' ) / ' + str(temp_sd) # divisao pelodesvio padrao
    normalized_example_point.append(normalized_example_dimesion)
    del(temp_mean)
    del(normalized_example_dimesion)

# agora o dot product da matriz de covariancias

eigenvector_subset_str = eigenvector_subset.values.tolist() # passa para lista
new_reduced_point = []

for i in range(len(eigenvector_subset_str[0])): # a ordem é a certa
    temp_reduced_dimension = '0' # para iniciar
    for j in range(len(eigenvector_subset_str)):
        temp_reduced_dimension = temp_reduced_dimension + ' + ((' + normalized_example_point[j] + ') * ' + str(eigenvector_subset_str[j][i]) + ')'
    new_reduced_point.append(temp_reduced_dimension)


print('the x cordinate means:')
print(new_reduced_point[0])
print()
print('the y cordinate means:')
print(new_reduced_point[1])
'''
# graficos

fig = plt.figure()
keep_ploting = True
plt.scatter(data_reduced[:,0], data_reduced[:,1], s=1, c='black')
plt.show() # block=False
'''
'''
if os.path.exists("eigenvector_subset.csv"):os.remove('eigenvector_subset.csv') 
if os.path.exists('mean_matrix.csv'):os.remove('mean_matrix.csv') 
if os.path.exists('standard_deviation_matrix.csv'):os.remove('standard_deviation_matrix.csv') 

eigenvector_subset.to_csv('eigenvector_subset.csv')
mean_matrix.to_csv('mean_matrix.csv')
standard_deviation_matrix.to_csv('standard_deviation_matrix.csv')
'''
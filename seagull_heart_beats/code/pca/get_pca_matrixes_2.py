#!/usr/bin/python3.9

# entao para este codigo vou extrair as matrizes de media, desvio padrao e covariancia necessarias para fazer a reducao de dimansoes pelo algoritmo pca
# toda a aplicacao e a partir de codigo fonte porque e a unica maneira de o fazer
# embora este codigo esteja preparado para poder fazer tambem a reducao de dimensoes vou deixar isso para outro

# enta versao 2 vou fazer algumas alteracoes
# primeiro e nao dividir pelo desvio padrao, acho que isso ta me a foder os dados todos
# segundo passar tudo para funcoes que possam ser chamadas porque ya 3 regras do unix
# terceiro e passar para np outra vez # pa eu tentei usar pandas realmente tentei mas nao da e bueda complicado

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import csv
import os


def geting_the_matrixes(data, final_dim = 2): # final dim sao as dimensoes finais data sao os dados ja no formato 60 * n

    example_point = ['av1', 'av2', 'av3', 'av4', 'av5', 'av6', 'av7', 'av8', 'av9', 'av10', 'av11', 'av12', 'av13', 'av14', 'av15', 'av16', 'av17', 'av18', 'av19', 'av20', 'av21', 'av22', 'av23', 'av24', 'av25', 'av26', 'av27', 'av28', 'av29', 'av30', 'sd1', 'sd2', 'sd3', 'sd4', 'sd5', 'sd6', 'sd7', 'sd8', 'sd9', 'sd10', 'sd11', 'sd12', 'sd13', 'sd14', 'sd15', 'sd16', 'sd17', 'sd18', 'sd19', 'sd20', 'sd21', 'sd22', 'sd23', 'sd24', 'sd25', 'sd26', 'sd27', 'sd28', 'sd29', 'sd30']

    mean_matrix = np.mean(data, axis=0) # funciona bem mas agora e uma coluna
    normalized_data = data - mean_matrix # isto centra os dados em 0
    normalized_data = np.transpose(normalized_data) # entao estou a fazer a o tranposto porque acho que o numpy precisa de ter isto assim para calcular a matriz de covariancias

    covariance_matrix = np.cov(normalized_data) # convariancias acho que faz sentido

    eigen_values , eigen_vectors = np.linalg.eigh(covariance_matrix) # valores e vetores proprios
    # os valore proprios vao nos dar a ordem de relevancia de cada nova dimensao que e determinada pelos vetores proprios

    sorted_index = np.argsort(eigen_values)[::-1] # ordem de relevancia dos valores proprios

    sorted_eigenvalue = eigen_values[sorted_index] # autoexplicativo
    sorted_eigenvalue_0_1 = (sorted_eigenvalue - sorted_eigenvalue.min())/(sorted_eigenvalue.max() - sorted_eigenvalue.min()) # assim fica a variar entre 0 e 1, e mais facil de analizar relevancias
    sorted_eigenvectors = eigen_vectors[:,sorted_index]

    # reducao em si
    eigenvector_subset = sorted_eigenvectors[:,0:final_dim]


    # data_reduced = np.dot(eigenvector_subset.transpose() , normalized_data.transpose() ).transpose() # esta e a parte dareducao em si onde ha a perda de informacao

    # descricao de o que cada eixo significa

    mean_matrix_str = list(mean_matrix) # passa para lista

    normalized_example_point = []

     # isto poe o ponto falso de 60 dimensoes na posicao centrada isto e sobre a atuacao da media de todos os pontos
    for i in range(len(example_point)):
        temp_mean = mean_matrix[i]

        if temp_mean < 0:
            temp_mean *= -1
            temp_mean = str(temp_mean)
            normalized_example_dimesion = example_point[i] + ' + ' + temp_mean # como é negativo soma porque a media é subtraida

        else:
            temp_mean = str(temp_mean)
            normalized_example_dimesion = example_point[i] + ' - ' + temp_mean
        
        normalized_example_point.append(normalized_example_dimesion)
        del(temp_mean)
        del(normalized_example_dimesion)

    # agora o dot product da matriz de covariancias

    eigenvector_subset_str = list(eigenvector_subset) # passa para lista
    new_reduced_point = []

    for i in range(len(eigenvector_subset_str[0])): # a ordem é a certa
        temp_reduced_dimension = '0' # para iniciar
        for j in range(len(eigenvector_subset_str)):
            temp_reduced_dimension = temp_reduced_dimension + ' + ((' + normalized_example_point[j] + ') * ' + str(eigenvector_subset_str[j][i]) + ')'
        new_reduced_point.append(temp_reduced_dimension)
    
    return (mean_matrix, eigenvector_subset, new_reduced_point) # isto sao 3 matrizes

# isto e para o codigo correr em vazio
def main():
    data_complete = pd.read_csv('../../data/csv/eggs_guilia.csv', index_col=0) # aqui indico o ficeiro csv ao qual vou buscar os dados # com indexes e colum names tal como pandas quer (isto estava me a eliminar o primeiro ponto...)
    print(data_complete)
    data = data_complete.copy()
    del data['path_to_file'] # pega nos dados com tempo e remove os valores da primeira (0) coluna (1)
    del data['start_time']
    del data['end_time']

    data = data.to_numpy() # passa para um np array
    print(data.shape)
    mean_matrix, eigenvector_subset, new_reduced_point = geting_the_matrixes(data)

    if os.path.exists("eigenvector_subset.csv"):os.remove('eigenvector_subset.csv') 
    if os.path.exists('mean_matrix.csv'):os.remove('mean_matrix.csv') 

    np.savetxt("eigenvector_subset.csv", eigenvector_subset, delimiter=",")
    np.savetxt("mean_matrix.csv", mean_matrix, delimiter=",")
    

if __name__ == "__main__":
   main()
#!/usr/bin/python3.9

# entao este codigo e so para dar o display destes dois ficheiros
# nao vai ser por funcoes por ser tao especifico

from get_pca_matrixes_2 import geting_the_matrixes
from pca_through_set_matrixes import dimension_reduction_2
import numpy as np
import matplotlib.pyplot as plt

path_1 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/fake_beats_var_bpm_noise_t-7200.csv'
path_2 = '/home/parente/Documents/tese/seagull_heart_beats/code/mfcc/urban_sounds_var_noise_t-7200.csv'

# vou dar o load dos ficheiros
fake_beats = np.loadtxt(path_1, delimiter=',')
urba_sound = np.loadtxt(path_2, delimiter=',')

# remove o label
fake_beats = fake_beats[:, 0:60]
urba_sound = urba_sound[:, 0:60] 

# calculo a matrix de reducao
matrix_data = np.vstack((fake_beats, urba_sound))
mean_matrix, eigenvector_subset, new_reduced_point = geting_the_matrixes(matrix_data, final_dim = 2)

fake_beats_pca = dimension_reduction_2(fake_beats, mean_matrix, eigenvector_subset)
urba_sound_pca = dimension_reduction_2(urba_sound, mean_matrix, eigenvector_subset)

print(new_reduced_point)
print(fake_beats_pca)
print(urba_sound_pca)

plt.scatter(fake_beats_pca[:, 0], fake_beats_pca[:, 1], s=1)
plt.scatter(urba_sound_pca[:, 0], urba_sound_pca[:, 1], s=1)
plt.legend(["beats" , "urban"])
plt.show()
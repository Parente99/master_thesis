#!/usr/bin/python3.9

# entram dados com 60 dimensoes e saem com 2

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import csv
import os

n_components = 2 # quero isto para iniciar

def dimension_reduction(data): # aqui entram os dados ja com as 60 dimensoes so
    mean_matrix = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/mean_matrix.csv', header=0, index_col=0) # tem de ser com header=None porque se nao o data frame intrepreta a primeira linha como a descricao da coluna
    mean_matrix = mean_matrix.squeeze() # isto passa de dataframe para series que e necessario para as subtracoes e divisao
    standard_deviation_matrix = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/standard_deviation_matrix.csv', header=0, index_col=0)
    standard_deviation_matrix = standard_deviation_matrix.squeeze()
    eigenvector_subset = pd.read_csv('/home/parente/Documents/tese/seagull_heart_beats/data/csv/pca_guilia/eigenvector_subset.csv', header=0, index_col=0)
    #n_of_points = data.shape[1]

    meaned_data = data.subtract(mean_matrix, axis=1) # isto centra os dados em 0
    normalized_data = meaned_data/standard_deviation_matrix # poe o desvio padrao a 1
    data_reduced = np.dot(eigenvector_subset.transpose() , normalized_data.transpose() ).transpose() # esta é a parte dareducao em si onde ha a perda de informacao
    return (data_reduced)

def dimension_reduction_2(data, mean_matrix, eigenvector_subset): # aqui entram os dados ja com as 60 dimensoes so

    normalized_data = data - mean_matrix # isto centra os dados em 0
    data_reduced = np.dot(eigenvector_subset.transpose() , normalized_data.transpose() ).transpose() # esta é a parte dareducao em si onde ha a perda de informacao
    return (data_reduced)


# isto e para o codigo correr em vazio
def main():
    print('diz qual e o caminho ate aos dados')
    path1 = str(input())
    path2 = str(input())

    data_reduced1 = dimension_reduction(path1)
    data_reduced2 = dimension_reduction(path2)

    # graficos
    fig = plt.figure()
    plt.scatter(data_reduced1[:,0], data_reduced1[:,1], s=1, c='black')
    plt.scatter(data_reduced2[:,0], data_reduced2[:,1], s=1, c='blue')

    plt.show()

if __name__ == "__main__":
   main()

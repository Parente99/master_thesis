#!/usr/bin/python3.9
# coding: utf-8

# entao peseudo codigo
'''
vou tentar fazer um tudo num neste codigo
vou criar o ficheiro de som o label e treinar com isso
para isso tenho de ponto n1 fazer uma porrada de funcoes
funcao de criar o som e label
funcao de treinar com o som e label
nao consigo dividir mais sem sacrificar algo

o label ativa no pulso
e bidirecional para usar os dados do futuro para fazer elacoes sobre o presente

'''
import random as rd
import numpy as np
import librosa as lb
import matplotlib.pyplot as plt
import os
import math as ma

import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1

import keras
import tensorflow as tf
from keras.models import load_model
from keras.models import Sequential
from keras.layers import Conv1D, Dense, Dropout, Flatten, MaxPooling1D, AveragePooling1D, LSTM, Input, Bidirectional
from keras.utils import np_utils


def fake_signal_variable_bpm(time):
	bpm_range = [100, 200]
	sound_sample_time = 7 # numero de segundos que cada segmento de som com ruido constante vai ter
	file_time = 0
	fake_sound = []
	label = []	

	while file_time < time:
		bpm = rd.randint(bpm_range[0], bpm_range[1])
		
		sound_sample, label_sample, sample_rate = fake_signal_generator(sound_sample_time, bpm)
		fake_sound = np.append(fake_sound, sound_sample)
		label = np.append(label, label_sample)
		file_time = len(fake_sound)/sample_rate
		print(str(round((file_time/time)*100, 2)) + '%')
	
	return fake_sound, label


def fake_signal_generator(time, bpm):
	path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/single_beats'
	heart_beats = {} # dicionario que vai ter todos os arrays de som
	dic_key = 0 # so para inicializar

	# esta parte poe todso os ficheiros de batimentos cardiacos num dicionario
	for wav_file in  os.listdir(path): # especifico o ficheiro de som

		if wav_file.endswith(".wav"):  # verifica se  um ficheiro de som
			path_1 = os.path.join(path, wav_file)
			audio_file, frame_rate = lb.load(path_1, sr=None)  # isto da me o ficheiro em array e o frame rate, sr=none e para manter o sample rate original do ficheiro
			audio_file = np.array(audio_file) 
			# muda para numpy array caso nao esteja
			audio_file = normalizer_av0_max1(audio_file)
			# normaliza para ficar com media 0 e maximo absoluto 1
			heart_beats[str(dic_key)] = audio_file 
			# adiciona o audio falie a um dicionario
			dic_key += 1



	n_frames = int(frame_rate * time) # numero minimo de frames do ficheiro
	total_frames = 0 # só para inicializar
	fake_beats = [] # array dos pulsos artificiais
	label = [] # array que identifica quando e um pulso
	
	# calcular o tempo e frames que cada pulso tem de ter
	pulse_time = 60/bpm # em segundos
	pulse_frames = int(frame_rate * pulse_time) # numero frame que o pulso tem de ter para o ficheiro ter os BPM que qeuremos

	# esta parte cria um ficheiro de som sem ruido. ficherio fake_beats
	while total_frames < n_frames:
		pulse_key = rd.randrange(0,dic_key) # o pulse_key é um aleatrio que o que faz é selecionar um batimanto cardiaco aleatrio da lista deles
		temp_pulse = heart_beats[str(pulse_key)] # busca o beat aleatorio, tipo o array mesmo

		# adiciona zeros a segir ao pulso para chegar ao numero correto de bpm
		len_pulse = len(temp_pulse)
		pulse_gap = pulse_frames - len_pulse
		zero = np.zeros(pulse_gap)
		temp_pulse = np.append(temp_pulse, zero) # agora o pulso tem os frames nececarios
		# adiciona ao ficheiro com os batimentos
		fake_beats = np.append(fake_beats, temp_pulse)
		del temp_pulse
		
		total_frames = len(fake_beats) # neste caso o comprimento do array vai corresponder ao numero de samples/frames estou a usar as duas palavras para representar a mesma coisa
		
		# agora a parte do label
		temp_label = np.ones(len_pulse)
		label = np.append(label, temp_label)
		temp_label = np.zeros(pulse_gap)	
		label = np.append(label, temp_label)

		#print('esta ' + str((total_frames/n_frames)*100) + '% completo')
	
	return fake_beats, label, frame_rate # nao envio o sample rate porque nunca e usado nos treinos da rede

print("time in seconds of the file to train NN")
time_of_file = int(input())

signal, label = fake_signal_variable_bpm(time_of_file)
'''
test_len = 4016
plt.plot(signal[0:test_len])
plt.plot(label[0:test_len])
plt.show()
'''
# aqui comeca a nn

test_split = 0.8 # percentagem de divisao
n_data = len(signal) # numero de dados
split_value = int(n_data*test_split) # ponto onde vai ser dividido

# divisoes dos dados em treino e teste e reshape para a rede
wave_size = 5000 # tamanho daquilo que entra na rede
n_dimensions = 1

div = wave_size * n_dimensions
remain = split_value % div
split_value_train = split_value - remain

div = wave_size * n_dimensions
block = n_data - split_value_train
remain = block % div
split_value_test = split_value_train + remain

train_data = signal[:split_value_train]
train_data = train_data.reshape(-1,wave_size,1) # time_steps, n_samples, features

test_data = signal[split_value_test:]
test_data = test_data.reshape(-1,wave_size,1) # time_steps, n_samples, features

train_label = label[:split_value_train]
train_label = train_label.reshape(-1,wave_size,1) # time_steps, n_samples, features

test_label = label[split_value_test:]
test_label = test_label.reshape(-1,wave_size,1) # time_steps, n_samples, features

# RNN em si com uma CNN associada
model = Sequential()

model.add(Conv1D(filters=64,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
model.add(Conv1D(filters=128,kernel_size=10,activation='relu',input_shape=(wave_size,1)))
model.add(MaxPooling1D(pool_size=10,strides=2,padding='same'))
model.add(Dropout(0.1))

#model.add(Input(shape=train_data.shape[1:]))
model.add(Bidirectional(LSTM(units = 640, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
model.add(Bidirectional(LSTM(units = 640, return_sequences=True, dropout=0.1, recurrent_dropout=0.1)))
model.add(Bidirectional(LSTM(units = 640, return_sequences=False, dropout=0.1, recurrent_dropout=0.1)))

model.add(Flatten())
model.add(Dense(640,activation='relu'))
model.add(Dense(wave_size,activation='linear'))

model.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])
model.summary()

# ?
train_data_ = tf.squeeze(train_data, axis=-1)
train_label_ = tf.squeeze(train_label, axis=-1)
test_data_ = tf.squeeze(test_data, axis=-1)
test_label_ = tf.squeeze(test_label, axis=-1)

# treina a rede
history = model.fit(train_data_, train_label_, batch_size=64, epochs=50, verbose=1, validation_data=(test_data_,test_label_))

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.savefig("loss.jpg")

# e accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.savefig("accuracy.jpg")


sample_x = train_data[0,:,0] # aqui vou ao sinal tirar uma amostra
sample_x = np.reshape(sample_x, (1, -1, 1)) # (n_samples, time_steps, features) # aqui passo para a maneira da rnn poder ler
sample_y = train_label[0,:,0] # tiro uma amostra do label
prediction = model.predict(sample_x) # faco a previsao

sample_x = np.reshape(sample_x, (-1))
sample_y = np.reshape(sample_y, (-1))
prediction = np.reshape(prediction, (-1))

plt.plot(sample_x)
plt.plot(sample_y)
plt.plot(prediction)
plt.title('predicted label vs real')
plt.ylabel('intensity')
plt.xlabel('time')
plt.legend(['signal', 'label', 'label prediction'], loc='upper left')
plt.savefig("predictions_1.jpg")
plt.show()


block=15

sample_x = test_data[block,:,0]
sample_x = np.reshape(sample_x, (1, -1, 1)) # (n_samples, time_steps, features) # aqui passo para a maneira da rnn poder ler
sample_y = test_label[block,:,0] # tiro uma amostra do label
prediction = model.predict(sample_x) # faco a previsao

sample_x = np.reshape(sample_x, (-1))
sample_y = np.reshape(sample_y, (-1))
prediction = np.reshape(prediction, (-1))

plt.plot(sample_x)
plt.plot(sample_y)
plt.plot(prediction)
plt.title('predicted label vs real')
plt.ylabel('intensity')
plt.xlabel('time')
plt.legend(['signal', 'label', 'label prediction'], loc='upper left')
plt.savefig("predictions_2.jpg")


# model.save("/home/neves/notebook/CCMC-RNN/model00")






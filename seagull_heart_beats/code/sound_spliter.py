#!/usr/bin/python3.9

# entao este codigo vai carregar todos os ficheiros que sao ou ruido ou batimentos
# juntar num grande
# dividir em segmentos de 10 000 samples
# guardar cada um destes segmentos como ficheiros de som individuais

from path_searcher import search_files
import math as ma
import librosa as lb
import soundfile as sf
import numpy as np

def sound_spliter(path_sound_to_dir, path_to_sample_dir): # a primeira e para os ficheiros audio roiginais e a segunda para os ficheiros audio ja segmentados
    n_samples = 10000 # numero de samples por segmento audio
    extension = '.wav'
    paths_to_files = search_files(path_sound_to_dir, extension)
    full_data = np.empty([1])
    
    for path_to_file in paths_to_files: # itera pelos ficheiros que encontrou
        audio_file, sample_rate = lb.load(path_to_file, sr=None) # da load do ficheiro
        full_data = np.hstack((full_data, audio_file)) # adiciona aos ficheiros que ja tem
        print('forma full data')
        print(audio_file.shape)
        print(full_data.shape)
    
    n_data = len(full_data) # quantidade de dados
    print(n_data)
    try:
        win_len = 10000
        stride = 5000
        i = 0
        while True:
            start = i * stride
            end = start + win_len
            print('start ' + str(start) + ' end ' + str(end))
            temp_sample = full_data[start:end] # e a amostra de ficheiros que quero
            i = i+1 # avanca a iteracao

            n_temp_sample = len(temp_sample)
            if (n_temp_sample < win_len): # chegou ao fim do ficheiro
                break

            print('forma temp_sample ' + str(temp_sample.shape))
            file_name = path_to_sample_dir + 'automatic_split_' + str(i) + '.wav' # cria o nome do ficheoro
            print('index ' + str(i))
            print(file_name)

            sf.write(file_name, temp_sample, sample_rate)
    except Exception as e:
        print(e)

def main():
    # caminho user friendly
    #print('diz qual e o caminho ate ao sitio onde estao os sons')
    #path_sound_to_dir = str(input())
    #print('diz qual e o caminho ate ao sitio onde queres por as samples')

    # caminho hardcoded
    path_sound_to_dir = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_long_files/noise_quiet/'
    path_to_sample_dir = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/fake_segments_10000_samples/noise_quiet/'

    sound_spliter(path_sound_to_dir, path_to_sample_dir)


if __name__ == "__main__":
   main()

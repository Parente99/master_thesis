#!/usr/bin/python3.9

'''
Entao preseudo coddigo
tenho de por numa lista todos os nomes de ficheiros
extrair todos os sons para um array
guardar tudo num dicionario

do
    escolher aleatoriamente X ficheiros
    extrair os espectogramas de X ficheiros
    fazer a media de X espectogramas
    passar de espectograma para som
    guardar como ficheiro
while Y
'''

import librosa as lb
import numpy as np
import os
from scipy.fft import fft, ifft
from matplotlib import pyplot as plt
import soundfile as sf
import random as rd

path = '/home/parente/Documents/tese/seagull_heart_beats/data/wav/single_beats'
heart_beats = {} # dicionario que vai ter todos os arrays de som
dic_key = 0 # so para inicializar

for wav_file in  os.listdir(path): # especifico o ficheiro de som
    if wav_file.endswith(".wav"):  # verifica se  um ficheiro de som
        path_1 = os.path.join(path, wav_file)
        audio_file, frame_rate = lb.load(path_1, sr=None)  # isto da me o ficheiro em array e o frame rate, sr=none e para manter o sample rate original do ficheiro
        audio_file = np.array(audio_file) # muda para numpy array caso nao esteja

        if (len(audio_file) == 500):
            heart_beats[str(dic_key)] = audio_file # adiciona o audio falie a um dicionario
            dic_key += 1
        
        print(dic_key)

n_files_for_avg = 5
n_created_files_goal = 500

def show(file):
    print(file)
    plt.plot(file)
    plt.show()
    plt.clf()

for i in range(n_created_files_goal):
    first_file = True
    for j in range(n_files_for_avg):
        pulse_key = rd.randrange(0,dic_key) # o pulse_key é um aleatrio que o que faz é selecionar um batimanto cardiaco aleatrio da lista deles

        temp_pulse = heart_beats[str(pulse_key)] # busca o beat aleatorio, tipo o array mesmo
        #show(temp_pulse)

        temp_fft = fft(temp_pulse) # faz a fft do pulso
        #show(temp_fft)

        if (first_file): # inicia o array qeu vai ter todos os ficheiros
            first_file = False
            files_fft = temp_fft
        else:
            files_fft = np.vstack((files_fft, temp_fft)) # vai acrescendo valores a matrix de ficheiros
    
    created_file_fft = np.mean(files_fft, axis=0)
    #show(created_file_fft)

    created_file_pulse = ifft(created_file_fft).real
    #show(created_file_pulse)

    file_name = 'shh/fake_' + str(i) + '.wav'
    sf.write(file_name, created_file_pulse, frame_rate)





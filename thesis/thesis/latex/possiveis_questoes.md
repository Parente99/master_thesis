# No Feature model extraction 3 dado o número tão grande de inputs porque e que não adaptou o tamanho das camadas internas da rede?
> para ter um classificador justo. isto e como a rede era usada como classificador de qualidade das características então tinha de ser a mesma para todas as redes. alterar ia invalidar isto.

# Dado o número tão grande de inputs deveria ter mais samples de teste?
> Não houve mais samples de teste para manter o rácio 80 20 que foi usado em todo este trabalho.

# Numa rede neuronal de onde vem a não linearidade?
> A característica que confere as NNs o comportamento não linear que precisamos são as funções de ativação que não devem ser todas lineares.

# Para a deteção de zonas com batimentos cardíacos, considerou usar uma ROC para descobrir o melhor th?
> Nessa parte do trabalho o objetivo estava mais focado em aumentar a distinção entre classes. Isto é aproximar o sinal de 1 e o ruído de 0 para o output da rede. Depois desta otimização e a outra das moving averages estar feita então uma RoC seria usada.

# Para o primeiro gerador de ruído decidiu usar os urban sounds. Por que é que escolheu este dataset em vez de algo com sons do ambiente onde as gaivotas estão?
> O motivo da escolha deste conjunto de dados foi ter um dataset suficientemente diverso para abranger e superar a gama de ruídos presentes nos ficheiros. Isso ajudaria a criar uma rede mais resiliente.

# Na secção ANN model 11 diz que os gráficos de evolução da rede não são relevantes. Por quê?
> Digo que não são relevantes porque são iguais aos anteriores. Nesta altura do trabalho a maioria da informação está gráfico de separação de ruído.

# Porque e que só usou max pooling para a CNN?
> Por falta de tempo. Foram feitas poucas redes e decidi efetuar otimizações noutras áreas. E uma área de maior estudo.

## quanto as redes sparse
> Ser sparse é bom porque temos uma rede com menos parâmetros.

> é observado que as redes sparse generalizam tão bem ou por vezes melhor que uma full connected network.

> o que quero deixar claro é o grande motivo é ter menos parâmetros sem perder poder de generalizar e não aumentar a generalização por si.
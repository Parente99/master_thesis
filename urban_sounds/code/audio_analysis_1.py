# este codigo vai treinar uma rede com 30 medias

import keras
import matplotlib.pyplot
import numpy
import sklearn
from sklearn.model_selection import train_test_split
import seaborn
import pandas

Data = numpy.loadtxt('processed_data_3.csv', delimiter=',')
Results = numpy.loadtxt('results_3.csv', delimiter=',')
NumberInputs = Data.shape[1]

TrainingData, ValidationData, TrainingResults, ValidationResults = train_test_split(
    Data,
    Results,
    test_size=0.1,
    random_state=22
)
print(TrainingData.shape)

# isto cria o modelo # para este como sao muitos dados de entrada estou a fazer um funil
model = keras.Sequential()
model.add(keras.engine.Input(shape=NumberInputs))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.relu))
model.add(keras.layers.core.Dense(250))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.relu))
model.add(keras.layers.core.Dense(250))
model.add(keras.layers.BatchNormalization())
model.add(keras.layers.core.Activation(keras.activations.softmax))
model.add(keras.layers.core.Dense(10))

# vai compilar tudo
keras.optimizers.SGD(
    learning_rate=0.1,
    momentum=0.0,
    nesterov=True,
    name="SGD"
)

model.compile(
    optimizer='SGD',
    loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
    metrics=['accuracy']
)

# da o fit
history = model.fit(
    x=TrainingData,
    y=TrainingResults,
    validation_data=(ValidationData, ValidationResults),
    batch_size=32,
    epochs=8000,
    verbose=1
)

Predictions = model.predict(ValidationData)
ConfusionMatrixValidation = sklearn.metrics.confusion_matrix(
    ValidationResults.argmax(axis=1),
    Predictions.argmax(axis=1)
)

Predictions = model.predict(TrainingData)
ConfusionMatrixTraining = sklearn.metrics.confusion_matrix(
    TrainingResults.argmax(axis=1),
    Predictions.argmax(axis=1)
)

# summarize history for accuracy
matplotlib.pyplot.plot(history.history['accuracy'])
matplotlib.pyplot.plot(history.history['val_accuracy'])
matplotlib.pyplot.title('model accuracy')
matplotlib.pyplot.ylabel('accuracy')
matplotlib.pyplot.xlabel('epoch')
matplotlib.pyplot.legend(['train', 'test'], loc='upper left')
matplotlib.pyplot.show()

# summarize history for loss
matplotlib.pyplot.plot(history.history['loss'])
matplotlib.pyplot.plot(history.history['val_loss'])
matplotlib.pyplot.title('model loss')
matplotlib.pyplot.ylabel('loss')
matplotlib.pyplot.xlabel('epoch')
matplotlib.pyplot.legend(['train', 'test'], loc='upper left')
matplotlib.pyplot.show()

# confusion matrix training
conf_matr_temp = pandas.DataFrame(ConfusionMatrixTraining, range(10), range(10))
seaborn.set(font_scale=1) # for label size
seaborn.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
matplotlib.pyplot.title('confusion matrix training')
matplotlib.pyplot.ylabel('expected')
matplotlib.pyplot.xlabel('predicted')
matplotlib.pyplot.show()

# confusion matrix validation
conf_matr_temp = pandas.DataFrame(ConfusionMatrixValidation, range(10), range(10))
seaborn.set(font_scale=1) # for label size
seaborn.heatmap(conf_matr_temp, annot=True, annot_kws={"size": 10})
matplotlib.pyplot.title('confusion matrix validation')
matplotlib.pyplot.ylabel('expected')
matplotlib.pyplot.xlabel('predicted')
matplotlib.pyplot.show()

Categories = numpy.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

NCatValidationResults = numpy.zeros(10)
for i in ValidationResults:
    NCatValidationResults = NCatValidationResults + i

NCatTrainingResults = numpy.zeros(10)
for i in TrainingResults:
    NCatTrainingResults = NCatTrainingResults + i

NCatTotalResults = numpy.zeros(10)
for i in Results:
    NCatTotalResults = NCatTotalResults + i

# number of categories validation data
matplotlib.pyplot.bar(x=Categories, height=NCatValidationResults, width=1)
matplotlib.pyplot.title('number of categories in validation data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatValidationResults)

# number of categories training data
matplotlib.pyplot.bar(x=Categories, height=NCatTrainingResults, width=1)
matplotlib.pyplot.title('number of categories in training data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatTrainingResults)

# number of categories total data
matplotlib.pyplot.bar(x=Categories, height=NCatTotalResults, width=1)
matplotlib.pyplot.title('number of categories in training data')
matplotlib.pyplot.ylabel('amount')
matplotlib.pyplot.xlabel('categories')
matplotlib.pyplot.show()
print(NCatTotalResults)

# este codigo vai extrair as medias do mfccs e criar um array 30*n

import librosa
import numpy
import keras
import os
import matplotlib.pyplot

# inicializacoes
i = 0
NMelCoe = 30
FullDataSet = []
CorrectResults = []
Path = "/home/parente/PycharmProjects/CNNAudio/UrbanSound8K/audio/fold"  # path ate ao folder para iterar

for j in range(10):
    k = j + 1
    CurrentFolderPath = Path + str(k)
    FilesName = os.listdir(CurrentFolderPath)  # array com todos os nomes de todos os ficheiros
    for FileName in FilesName:
        if FileName.endswith(".wav"):  # verifica se  um ficheiro de som
            FullPath = os.path.join(CurrentFolderPath, FileName)  # isto da me o path ate aquilo que eu quero

            # okay essta parte dame os resultados
            SplittedFileName = FileName.split("-")  # divide o nome do fichero
            CorrectResult = SplittedFileName[1]  # tira aquilo que e o resultado certo
            CorrectResults.append(CorrectResult)

            # agora para a analise de som
            AudioFile, FrameRate = librosa.load(FullPath)  # isto da me o ficheiro em array acho e o samplerate
            MFCCS = librosa.feature.mfcc(y=AudioFile, sr=FrameRate, n_mfcc=NMelCoe)  # da me os MFCCS
            ProcessedMFCCS = numpy.mean(MFCCS.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
            FullDataSet.append(ProcessedMFCCS)
            i += 1
            print(CurrentFolderPath)
            print(i)


CorrectResultsArray = numpy.array(CorrectResults)  # esta em list passa para array de strings
CorrectResultsArray = [int(i) for i in CorrectResultsArray]  # passa str para int
CorrectResultsOneHot = keras.utils.to_categorical(CorrectResultsArray)  # passa para one hot encodind
FullDataSetArray = numpy.array(FullDataSet)  # passa de formato list para formato array

numpy.savetxt('processed_data_1.csv', FullDataSetArray, delimiter=',')
numpy.savetxt('results_1.csv', CorrectResultsOneHot, delimiter=',')

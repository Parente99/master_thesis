# okay neste vou fazer uma matriz 5190*n onde vou por todos os MFCCS
# vou dividir em 30 MFCCS por intervalo temporal
# e 173 intervalos temporais
# entao nem todos os dados tem o mesmo tamanho mas a maioria e dividida em 173 intervalos temporais entao so vou entrar com esses
# porque nao menos? porque nao sei como fazer isso bem que tentei
# a matriz vai ser muito longa mas bora tentar

import librosa
import numpy
import keras
import os
import matplotlib.pyplot

# inicializacoes
i = 0
NMelCoe = 30
FullDataSet = []
CorrectResults = []
Path = "/home/parente/PycharmProjects/CNNAudio/UrbanSound8K/audio/fold"  # path ate ao folder para iterar

for j in range(10):
    k = j + 1
    CurrentFolderPath = Path + str(k)
    FilesName = os.listdir(CurrentFolderPath)  # array com todos os nomes de todos os ficheiros
    for FileName in FilesName:
        if FileName.endswith(".wav"):  # verifica se  um ficheiro de som
            FullPath = os.path.join(CurrentFolderPath, FileName)  # isto da me o path ate aquilo que eu quero

            # okay essta parte dame os resultados
            SplittedFileName = FileName.split("-")  # divide o nome do fichero
            CorrectResult = SplittedFileName[1]  # tira aquilo que e o resultado certo


            # agora para a analise de som
            AudioFile, FrameRate = librosa.load(FullPath)  # isto da me o ficheiro em array acho e o samplerate
            MFCCS = librosa.feature.mfcc(y=AudioFile, sr=FrameRate, n_mfcc=NMelCoe)  # da me os MFCCS

            print(i)
            if(MFCCS.shape[1] == 173):
                temp_arr = MFCCS.flatten()
                FullDataSet.append(temp_arr)
                CorrectResults.append(CorrectResult)
                i += 1
                print(temp_arr.shape)

            print(MFCCS.shape)
            print(FullPath)

CorrectResultsArray = numpy.array(CorrectResults)  # esta em list passa para array de strings
CorrectResultsArray = [int(i) for i in CorrectResultsArray]  # passa str para int
CorrectResultsOneHot = keras.utils.to_categorical(CorrectResultsArray)  # passa para one hot encodind
FullDataSetArray = numpy.array(FullDataSet)  # passa de formato list para formato array

numpy.savetxt('processed_data_3.csv', FullDataSetArray, delimiter=',')
numpy.savetxt('results_3.csv', CorrectResultsOneHot, delimiter=',')

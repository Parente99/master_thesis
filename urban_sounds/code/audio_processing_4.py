# okay neste vou fazer uma matriz 1*250 onde vou por todos os MFCCS
# vou dividir em 25 MFCCS por intervalo temporal e 10 intervalos temporais

import librosa
import numpy
import keras
import os
import matplotlib.pyplot

# inicializacoes
i = 0
n_mel_coe = 25
n_tim_inte = 10 # isto da 10 intervalos de tempo
data = []
results = []
path = "/home/parente/PycharmProjects/CNNAudio/UrbanSound8K/audio/fold"  # path ate ao folder para iterar

for j in range(10):
    k = j + 1
    current_path = path + str(k)
    files_names = os.listdir(current_path)  # array com todos os nomes de todos os ficheiros
    for file_name in files_names:
        full_path = os.path.join(current_path, file_name)  # isto da me o path ate aquilo que eu quero
        name, ext = os.path.splitext(full_path)
        if (ext == ".wav"):  # verifica se  um ficheiro de som

            # okay essta parte dame os resultados
            splitted_file_name = file_name.split("-")  # divide o nome do fichero
            correct_result = splitted_file_name[1]  # tira aquilo que e o resultado certo


            # agora para a analise de som
            audio_file, frame_rate = librosa.load(full_path)  # isto da me o ficheiro em array e o samplerate
            audio_lenght = librosa.get_duration(y = audio_file, sr = frame_rate)
            audio_frames = int(audio_lenght * frame_rate)
            num_fram = int(2*audio_frames/(n_tim_inte-1)) # não sei porque é que precisa do -1.... parte estranha é que consigo explicar um +1 tou bué confuso....
            hop_len = int(num_fram/2)
            MFCCS = librosa.feature.mfcc(y = audio_file, sr = frame_rate, n_mfcc = n_mel_coe, n_fft = num_fram, hop_length = hop_len)  # da me os MFCCS
            # n_fft = numero de frames na janela
            # hop_lenght = numero de frames que avanca para a janela seguinte


            temp_arr = MFCCS.flatten()
            if (MFCCS.shape[1] == n_tim_inte): # isto é só para ter a certeza que tá tudo certo
                data.append(temp_arr)
                results.append(correct_result)
                i += 1
            print(i)
            print(temp_arr.shape)
            print(MFCCS.shape)
            print(correct_result)
            print(full_path)


resultsArray = numpy.array(results)  # esta em list passa para array de strings
resultsArray = [int(i) for i in resultsArray]  # passa str para int
resultsOneHot = keras.utils.to_categorical(resultsArray)  # passa para one hot encodind
dataArray = numpy.array(data)  # passa de formato list para formato array

numpy.savetxt('processed_data_4.csv', dataArray, delimiter=',')
numpy.savetxt('results_4.csv', resultsOneHot, delimiter=',')

# este codigo vai fazer uma representaçao por histograma dos dados
# neste caso os dados vao ser um array n*250 onde os 250 vao ser na verdade 25 mfccs em 10 intervalos temporais
# tempo - x ; mfccs - y

import librosa
import numpy
import keras
import os
import matplotlib.pyplot as plt
import time

data = numpy.loadtxt('processed_data_4.csv', delimiter=',')
results = numpy.loadtxt('results_4.csv', delimiter=',')
results = numpy.argmax(results, axis=-1) # it works muda de one hot encoding para numeros outra vez
n_bins = 20
n_classes = 10
n_time = 10
n_mfccs = 25
n_data = data.shape[0]
n_data = 100

# okay isto vai ser um dicionario de dicionarios que vai ter estas camadas classe, tempo, mfcc (10*10*25)

data_dict = {}

# esta parte prepara os dados isto é poe tudo no dicionario
for i in range(n_data): # iteraçao nos dados
    cat = results[i] # daqui retiramos a categoria
    for j in range(n_time): # iteração nos intervalos de tempo
        for k in range(n_mfccs): # interação nos mfcc
            num = n_time*k+j # entao eu tenho uma linha que é na verdade um array 2d linearizado e esta expressao vai me dar o valor desse array sabendo o tempo e mfcc
            dat = data[i, num]

            if (cat,j,k) in data_dict.keys():
                temp = data_dict[cat,j,k]
                temp = numpy.append(temp, dat)
                print(temp)
                data_dict[cat,j,k] = temp
            else:
                data_dict[cat,j,k] = numpy.array(dat)
            print(i)
            print(cat, j, k)

# esta parte exibe o dicionario
for ti in range(10): # iteração nos intervalos de tempo
    x=-1
    y=0
    fig, axs = plt.subplots(5, 5)
    for mf in range(25): # interação nos mfcc
        # esta parte "descobre" os valores correspondestes de x e y
        if x < 5:
            x += 1
        if x == 5:
            x = 0
            y += 1
        print (x, y)
        for cl in range(10): # iteraçao pelas classes
            temp = data_dict[cl,ti,mf]
            axs[x,y].hist(temp, bins = n_bins, histtype='step', stacked=True, fill=False)

    plt.show()
    plt.close()

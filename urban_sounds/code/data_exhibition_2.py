# este codigo vai fazer uma representaçao por histograma dos dados
# neste caso os dados vao ser um array n*60 onde os 60 vao ser na verdade 30 medias dos mfccs e 30 desvios padrao
# os dados estão ordenados m-d-m-d-m-d ...

import librosa
import numpy
import keras
import os
import matplotlib.pyplot as plt
import time

data = numpy.loadtxt('processed_data_2.csv', delimiter=',')
results = numpy.loadtxt('results_2.csv', delimiter=',')
results = numpy.argmax(results, axis=-1) # it works muda de one hot encoding para numeros outra vez
n_bins = 20
n_classes = 10
n_average = 30
n_stan_dev = 30
n_data = data.shape[0]
#n_data = 100
font_size = 2

# okay isto vai ser o dicionario das medias que vai ter as camadas classe, media (10*30)
av_dict = {}

# okay isto vai ser o dicionario das medias que vai ter as camadas classe, media (10*30)
sd_dict = {}

# esta parte prepara os dados isto é poe tudo nos dicionarios
for i in range(n_data): # iteraçao nos dados
    cat = results[i] # daqui retiramos a categoria
    for j in range(n_average): # iteração nas medias

        jj = j*2 # posicão da media j no array
        dat = data[i, jj]

        if (cat,j) in av_dict.keys(): # se já estiver inicializado
            temp = av_dict[cat,j]
            temp = numpy.append(temp, dat)
            av_dict[cat,j] = temp
        else: # se nao estiver inicializado
            av_dict[cat,j] = numpy.array(dat)
        print('data ' + str(i) + ' / average ' + str(j))

    for k in range(n_stan_dev): # iteração nos desvios padrao

        kk = k*2+1 # posicão do desvio padrao k no array
        dat = data[i, kk]

        if (cat,k) in sd_dict.keys(): # se já estiver inicializado
            temp = sd_dict[cat,k]
            temp = numpy.append(temp, dat)
            sd_dict[cat,k] = temp
        else: # se nao estiver inicializado
            sd_dict[cat,k] = numpy.array(dat)
        print('data ' + str(i) + ' / standard deviation ' + str(k))

# esta parte exibe os dicionarios

# tamanho do subplot tipo numero de graficos na figura
x_c = 6
y_c = 5
for cl in range(n_classes): # iteração nas classes
    print('class ' + str(cl))
    x=-1
    y=0

    fig, axs = plt.subplots(x_c, y_c)
    fig.suptitle('class ' + str(cl) + ' x -> intensity / y -> occurrences', fontsize=font_size*4)
    for av in range(n_average): # interação nas medias tambem vou usar o mesmo valor para os desvios padrao porque são iguais e dá me jeito # é uma fonte de bugs se isso se alterar
        # esta parte "descobre" os valores correspondestes de x e y
        if x < x_c:
            x += 1
        if x == x_c:
            x = 0
            y += 1

        sd = av # só para ficar mais bonito
        temp_av = av_dict[cl,av]
        temp_sd = sd_dict[cl,sd]

        axs[x,y].hist(temp_av, bins = n_bins, histtype='step', stacked=True, fill=False, label="av", linewidth=font_size*0.1) # average
        axs[x,y].hist(temp_sd, bins = n_bins, histtype='step', stacked=True, fill=False, label="sd", linewidth=font_size*0.1) # standard deviation
        axs[x,y].tick_params(labelsize=font_size, length=font_size, width=font_size*0.1)
        axs[x,y].tick_params(axis='x', top=True, bottom=False, labeltop=True, labelbottom=False, pad=0)
        axs[x,y].legend(fontsize=font_size, frameon=False, labelspacing=font_size*0.1, loc=1)
        axs[x,y].set_title('coeficient ' + str(av), fontsize=font_size, pad=0)

    plt.savefig(fname = 'class_' + str(cl) +'.pdf')

#!/usr/bin/python3.9

# objetivos: ponho um tempo, um snr e sai um ficheiro de som com essas caracteristicas
# vai funcionar mais ou menos como o outro


import librosa as lb
import soundfile as sf
import numpy as np
import random as rd
from noise_generator import white, pink, blue, brown, violet
from path_searcher import search_files
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1

path = '/home/parente/Documents/tese/urban_sounds/data/UrbanSound8K/audio'
paths_list = search_files(path, '.wav')
target_sr = 2016 # sample rate que quero que o som final tenha

def fake_sound_maker(time, snr, noise_type = 1): # ponho o default como white noise
    fake_sound = [] # so para inicializar
    n_frames = int(target_sr * time) # numero minimo de frames do ficheiro
    total_frames = 0 # só para inicializar

    while total_frames < n_frames: # loop ate chegar ao tempo que quero
        path_1 = rd.choice(paths_list) # atao aqui escolho o path dum ficheiro aleatrorio
        audio_file, sample_rate = lb.load(path_1, sr=target_sr) # dou o load do ficheiro com a sample rate dos ovos

        audio_file = normalizer_av0_max1(audio_file)
        # normaliza para ficar com media 0 e maximo absoluto 1
        fake_sound = np.append(fake_sound, audio_file)

        #print(sample_rate)

        total_frames = len(fake_sound) # neste caso o comprimento do array vai corresponder ao numero de samples/frames estou a usar as duas palavras para representar a mesma coisa
        
        #print(str(round((total_frames/n_frames)*100, 2)) + '%')

    # gera o ruido 'white', 'pink', 'blue', 'brown', 'violet'
    if noise_type == 1: 
        noise = white(N=total_frames)
    elif noise_type == 2: 
        noise = pink(N=total_frames)
    elif noise_type == 3: 
        noise = blue(N=total_frames)
    elif noise_type == 4: 
        noise = brown(N=total_frames)
    elif noise_type == 5: 
        noise = violet(N=total_frames)
    else: 
        noise = white(N=total_frames)
    
    # agora acertar o SNR vou calcular um k pelo qual preciso de multiplicar no array para isso acontecer
    k = np.sqrt(np.mean(fake_sound**2) / (snr * np.mean(noise**2)))
    noise = k * noise
    fake_sound_and_noise = fake_sound + noise

    # caso queira saber que raio estou a ouvir
    #sf.write('fake_signal.wav', fake_sound, target_sr)
    #sf.write('fake_noise.wav', noise, target_sr)

    return fake_sound_and_noise, sample_rate
    

def main():
    print("insert the minimum time for the fake file (seconds, type integer > 0)")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo
    print ("insert noise type for fake file. noise types: white(1), pink(2), blue(3), brown(4), violet(5)")
    noise_type = int(input())
    print("state SNR")
    snr = int(input())

    fake_sound_and_noise_1, sample_rate = fake_sound_maker(time, snr, noise_type)
    print(sample_rate)

    # okay esta parte vai ser para gerar o nome do ficheiro
    file_name = 'fake_urban_sound_t-' + str(time) + '_snr-' + str(snr) + '.wav'

    sf.write(file_name, fake_sound_and_noise_1, sample_rate) # okay e isto vai me guardar o ficheiro

if __name__ == "__main__":
   main()

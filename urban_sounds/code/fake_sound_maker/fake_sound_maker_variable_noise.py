#!/usr/bin/python3.9

# quero criar um ficheiro de osm que ao longo deste tenha variacoes de ruido tanto da sua intensidade como de tipo
# irei definir low e high bounds para a intensidade do ruido

from fake_sound_maker import fake_sound_maker
import random as rd
import numpy as np
import soundfile as sf

def fake_sound_and_noise_func(time):
    snr_range = [50, 110] # foram valores mandados um pouco ao calhas
    noise_type_range = [1, 2, 3, 4, 5]
    sound_sample_time = 7 # numero de segundos que cada segmento de som com ruido constante vai ter
    file_time = 0
    fake_sound = []
    
    while file_time < time:
        snr = rd.randint(snr_range[0], snr_range[1])
        noise_type = rd.choice(noise_type_range)
    
        sound_sample, sample_rate = fake_sound_maker(sound_sample_time, snr, noise_type)
        fake_sound = np.append(fake_sound, sound_sample)
        file_time = len(fake_sound)/sample_rate
        print(str(round((file_time/time)*100, 2)) + '%')
    
    return fake_sound, sample_rate

def main():
    print("insert the minimum time for the fake file (seconds, type integer > 0)")
    time = int(input()) # depois vai iterar até fazer um ficheiro maior que este tempo

    fake_sound, sr = fake_sound_and_noise_func(time)
    print(sr)

    name = 'variable_noise_urban_sounds_t-' + str(time) + '.wav'

    sf.write(name, fake_sound, sr) # okay e isto vai me guardar o ficheiro

if __name__ == "__main__":
   main()

#!/usr/bin/python3.9
# objetivo:
# da load de um ficheiro de som para um array
# poe o tamanho do ficheiro numa variavel
# da load do label e poe numa variavel
# ve se da para calcular os mfcc
    # se dar ativa uma flag e guarda num variavel
# guarda as tres num dic e recomeca

# depois e analise estatistica disto

# histogramas tempo
# numero de ficheiros total
# numero de ficheiros utilizavel

import matplotlib.pyplot as plt
import librosa as lb
import numpy as np
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1
from path_searcher import search_files

def label_extractor(path):
    splitted_path = path.split("-")  # divide o nome do fichero
    label = int(splitted_path[1])  # tira aquilo que e o resultado certo
    #print('label ', label)
    return label

def mfcc_matrix(path, n_mfcc):
    print('mfcc matrix func')
    len_win = 188 # numero de samples para o que correspondem a 93 milisegundos a 2016 Hz 
    len_hop = 47

    audio_file, sample_rate = lb.load(path, sr=None) # da o load do ficheiro audios
    #print('audio sample len 44100 ', len(audio_file))
    audio_sample_rate = 2016
    audio_file = lb.resample(audio_file, orig_sr=sample_rate, target_sr=audio_sample_rate) # passa para 2016 samples por segundo

    # vai retirando elementos do audio file ate ser multiplo do tamanho do stride
    len_audio = len(audio_file)
    remainer = len_audio % len_hop # so para inicializar
    while (remainer != 0):
        audio_file = audio_file[:-1] # retira um elemento, o final
        len_audio = len(audio_file)
        remainer = len_audio % len_hop # so para inicializar
        #print('audio sample len 2016 ', len_audio)

    audio_file = audio_file[:-1] # retira um elemento, o final
    audio_file = normalizer_av0_max1(audio_file)
    #audio_file = audio_file[:(len_win-1)] # para cortar para 1 mfcc # so mesmo para representacoes
    
    #plot_single(audio_file, 'Audio file', 'sample index', 'intensity')
    #plt_show()
    #plt_save()

    mfcc = lb.feature.mfcc(y=audio_file, sr=audio_sample_rate, n_mfcc=n_mfcc, win_length=len_win, hop_length=len_hop) # okay aqui vou ter uma funcao que me calcula os mfcc dum audio. tem uma janela igual a defenida em cima e anda num stride igual a janela.
    #mfcc = mfcc.astype('float16')
    #print('mfcc type ', mfcc.dtype)
    #print('shape mfcc ', mfcc.shape)

    # remover os 3 ultimos MFCC porque nao sao relevantes
    mfcc = mfcc[:,:-3]
    #print('shape mfcc ', mfcc.shape)
     
    #heat_map(mfcc, 'MFCC intensity', 'time steps', 'MFCC index')
    #plt_show()
    #plt_save()

    label = label_extractor(path)
    return mfcc, label, len_audio

def plt_show():
    #plt.rc('font', size=12)
    plt.show()
    plt.clf()

def plt_save(fig_name):
    #plt.rc('font', size=12)
    #fig_name = input('say the file name ')
    fig_name += '.pdf'
    plt.savefig(fig_name, format='pdf')
    plt.clf()

def histogram(data, bins, title, label_x, label_y):
    print('histogram_func')
    hist_values, bins_edge, patches = plt.hist(data, bins=bins, range=(min(data), max(data)+1))
    plt.title(title)
    plt.ylabel(label_y)
    plt.xlabel(label_x)
    return(hist_values, bins_edge)


def n_files_func(features):
    print('n_approved_files_func')
    n_files = len(features)
    n_approved_files = 0
    for i in features:
        feature = features[i]
        #print(features[i])
        if (feature[3] == True):
            n_approved_files += 1
            #print(n_approved_files)
    return n_files, n_approved_files

def label_and_lenght(features):
    print('label_and_lenght_func')
    label_array = []
    lenght_array = []
    approved_label_array = [] 
    for i in features:
        #print(features[i])
        lenght = features[i][1]
        label = features[i][2]
        approved = features[i][3]
        if (approved == True): approved_label_array.append(label)

        label_array.append(label)
        lenght_array.append(lenght)
    
    return label_array, lenght_array, approved_label_array

def statistical_anal(features):
    print('statistical_anal_func')
    n_files, n_approved_files = n_files_func(features)
    print('total number of files', n_files)
    print('total number of approved files', n_approved_files)

    label_array, lenght_array, approved_label_array = label_and_lenght(features)

    print('label histogram')
    hist_values, bins_edge = histogram(label_array, 10, 'labels histogram', 'label', 'number of occurrences')
    plt_save('total_labels_histogram')
    print('histogram values ', hist_values)
    print('bins edge ', bins_edge)

    print('approved label histogram')
    hist_values, bins_edge = histogram(approved_label_array, 10, 'labels histogram', 'label', 'number of occurrences')
    plt_save('approved_labels_histogram')
    print('histogram values ', hist_values)
    print('bins edge ', bins_edge)

    print('audio lenght histogram')
    hist_values, bins_edge = histogram(lenght_array, 20, 'audio length histogram', 'audio samples', 'number of occurrences')
    plt.yscale("log")
    plt_save('audio_length_histogram')
    print('histogram values ', hist_values)
    print('bins edge ', bins_edge)

def main():
    print('main_func')
    path = '/home/parente/Documents/tese/urban_sounds/data/UrbanSound8K/audio' # caminho ate ao ficheiro que tem os ficheiros audio
    extension = '.wav'
    paths_to_files = search_files(path, extension)
    n_time_steps = 168
    n_mfcc = 30
    first_loop = True
    loop = 0
    features = {}
    max_loop = 40
    for i in paths_to_files:
        
        mfcc, label, len_audio = mfcc_matrix(i, n_mfcc)
        #print('type mfcc ', mfcc.dtype)
        #print('mfcc time steps ', mfcc.shape[1])
        approved = False
        if (mfcc.shape[1] == n_time_steps): # isto e para garantir que tem o valor e significativo e que todos trabalham com o mesmo numero de dados
            approved = True

        loop += 1
        #print('loop ', loop)
        #if (loop > max_loop): break
        #print('type features ', features.dtype)
        #print('features ', features)
        features[str(loop)] = [loop, len_audio, label, approved]
        #print(features)
        #if (loop == max_loop): break
    
    statistical_anal(features)    

if __name__ == "__main__":
    main()


#!/usr/bin/python3.9
# o objetivo deste codigo e ter varios metodos de extracao de MFCC
# o input vai ser uma pasta com os ficheiros audio e o output os MFCC
# considerar tambem que vai haver uma entrada do utilizador para o tipo de MFCC que e extraido
# entao nos urbans sounds os ficheiros tem 4s entao nao sao divididos
# e urban ssounds cada ficheiro vai ter um e so um label

import librosa as lb
import numpy as np
import os
import matplotlib.pyplot as plt
import seaborn as sns
import sys
sys.path.insert(0, '/home/parente/Documents/tese/seagull_heart_beats/code/')
from normalizer_av0_max1 import normalizer_av0_max1
from path_searcher import search_files

path = '/home/parente/Documents/tese/urban_sounds/data/UrbanSound8K/audio' # caminho ate ao ficheiro que tem os ficheiros audio
audio_sample_rate = 2016

def plt_show():
    #plt.rc('font', size=12)
    plt.show()
    plt.clf()

def plt_save(fig_name):
    #plt.rc('font', size=12)
    #fig_name = input('say the file name ')
    fig_name += '.pdf'
    plt.savefig(fig_name, format='pdf')
    plt.clf()

def plot_double(data1, data2, title, label_x, label_y, legend): # array array str str str array
    plt.plot(data1)
    plt.plot(data2)
    plt.title(title)
    plt.ylabel(label_y)
    plt.xlabel(label_x)
    plt.legend(legend, loc='upper left')

def plot_single(data, title, label_x, label_y):
    plt.plot(data)
    plt.title(title)
    plt.ylabel(label_y)
    plt.xlabel(label_x)

def heat_map(data, title, label_x, label_y):
    ax = sns.heatmap(data, square=False, annot=True, annot_kws={"size": 10}, fmt='.0f')
    ax.invert_yaxis()
    plt.title(title)
    plt.ylabel(label_y)
    plt.xlabel(label_x)
    plt.yticks(rotation=0)

def label_extractor(path):
    splitted_path = path.split("-")  # divide o nome do fichero
    label = int(splitted_path[1])  # tira aquilo que e o resultado certo
    print('label ', label)
    return label

def mfcc_matrix(path, n_mfcc):
    print('mfcc matrix func')
    len_win = 188 #188 # numero de samples para o que correspondem a 93 milisegundos a 2016 Hz 
    len_hop = 47 #47

    audio_file, sample_rate = lb.load(path, sr=None) # da o load do ficheiro audios
    #print('audio sample len 44100 ', len(audio_file))
    audio_file = lb.resample(audio_file, orig_sr=sample_rate, target_sr=audio_sample_rate) # passa para 2016 samples por segundo

    # vai retirando elementos do audio file ate ser multiplo do tamanho do stride
    len_audio = len(audio_file)
    remainer = len_audio % len_hop # so para inicializar
    while (remainer != 0):
        audio_file = audio_file[:-1] # retira um elemento, o final
        len_audio = len(audio_file)
        remainer = len_audio % len_hop # so para inicializar
        #print('audio sample len 2016 ', len_audio)
    #

    audio_file = audio_file[:-1] # retira um elemento, o final
    audio_file = normalizer_av0_max1(audio_file)
    #audio_file = audio_file[:(len_win-1)] # para cortar para 1 mfcc # so mesmo para representacoes
    
    #plot_single(audio_file, 'Audio file', 'sample index', 'intensity')
    #plt_show()
    #plt_save()

    mfcc = lb.feature.mfcc(y=audio_file, sr=audio_sample_rate, n_mfcc=n_mfcc, win_length=len_win, hop_length=len_hop) # okay aqui vou ter uma funcao que me calcula os mfcc dum audio. tem uma janela igual a defenida em cima e anda num stride igual a janela.
    #mfcc = mfcc.astype('float16')
    #print('mfcc type ', mfcc.dtype)
    #print('shape mfcc ', mfcc.shape)

    # remover os 3 ultimos MFCC porque nao sao relevantes
    mfcc = mfcc[:,:-3]
    print('shape mfcc ', mfcc.shape)
     
    #heat_map(mfcc, 'MFCC intensity', 'time steps', 'MFCC index')
    #plt_show()
    #plt_save()

    label = label_extractor(path)
    return mfcc, label

def mfcc_total(mfcc, label): # aqui o input e path ate ao ficheiro audio completo
    print('mfcc total func')
    mfcc_flatten = mfcc.flatten() # trasnforma o array numa matriz de uma dimensao
    #print('shape flatten mfcc ', mfcc_flatten.shape)

    features_label = np.hstack((mfcc_flatten, label))
    #print('shape features and label ', features_label.shape)
    #print('features array ', features_label)

    return features_label

def mfcc_averages(mfcc, label): # aqui o input e path ate ao ficheiro audio completo
    print('mfcc averages func')
    mfcc_mean = np.mean(mfcc.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
    #print('shape mean mfcc ', mfcc_mean.shape)

    #heat_map(mfcc_mean.reshape(len(mfcc_mean),1), 'MFCC mean', 'Mean', 'MFCC index')
    #plt.xticks(color='w') # para tirar a marcacao tipo numeros e assim
    #plt_show()
    #plt_save()

    features_label = np.hstack((mfcc_mean, label))
    #print('shape features and label ', features_label.shape)
    #print('features and label array ', features_label)

    return features_label

def mfcc_deviations(mfcc, label): # aqui o input e path ate ao ficheiro audio completo
    print('mfcc deviations func')
    mfcc_std = np.std(mfcc.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
    #print('shape mean mfcc ', mfcc_mean.shape)

    #heat_map(mfcc_mean.reshape(len(mfcc_mean),1), 'MFCC mean', 'Mean', 'MFCC index')
    #plt.xticks(color='w') # para tirar a marcacao tipo numeros e assim
    #plt_show()
    #plt_save()

    features_label = np.hstack((mfcc_std, label))
    #print('shape features and label ', features_label.shape)
    #print('features and label array ', features_label)

    return features_label

def mfcc_averages_deviations(mfcc, label): # aqui o input e path ate ao ficheiro audio completo
    print('mfcc avg std func')
    mfcc_mean = np.mean(mfcc.T, axis=0)  # faz a media dos coeficientes de mel ao longo duma freq
    #print('shape mean mfcc ', mfcc_mean.shape)
    mfcc_std = np.std(mfcc.T, axis=0)  # desvio padrao
    #print('shape std mfcc ', mfcc_std.shape)
    
    #mfcc_representation = np.vstack((mfcc_mean, mfcc_std))
    #heat_map(mfcc_representation.T, 'MFCC mean and standard deviation', 'mean and standard deviation', 'MFCC index')
    #plt.xticks(color='w') # para tirar a marcacao tipo numeros e assim
    #plt_show()
    #plt_save()

    mfcc_joint = np.concatenate([mfcc_mean, mfcc_std])

    #print('shape joint mfcc ', mfcc_joint.shape)
    #print('mfcc mean type ', mfcc_mean.dtype)
    #print('mfcc std type ', mfcc_std.dtype)
    #print('mfcc joint type ', mfcc_joint.dtype)

    features_label = np.hstack((mfcc_joint, label))
    #print('features and label shape ', features_label.shape)
    #print('features array ', features_label)

    return features_label

def main():
    extension = '.wav'
    paths_to_files = search_files(path, extension)
    n_time_steps = 168 #168
    n_mfcc = int(input("enter mfcc: "))
    extractor = int(input("enter the extractor, full mfcc (1), averages (2), standard deviations (3), averages and standard deviation (4): "))
    first_loop = True
    features = []
    max_loop = 2
    loop = 0
    feature_type = '' # e so para saber que tipo de feature estou a retirar

    for i in paths_to_files:
        print('')
        print('')
        mfcc, label = mfcc_matrix(i, n_mfcc)
        #print('type mfcc ', mfcc.dtype)
        print('mfcc time steps ', mfcc.shape[1])
        if (mfcc.shape[1] == n_time_steps): # isto e para garantir que tem o valor e significativo e que todos trabalham com o mesmo numero de dados
            if (extractor == 1):
                temp = mfcc_total(mfcc, label)
                feature_type = str(n_mfcc) + '_full'
            if (extractor == 2):
                temp = mfcc_averages(mfcc, label)
                feature_type = str(n_mfcc) + '_averages'
            if (extractor == 3):
                temp = mfcc_deviations(mfcc, label)
                feature_type = str(n_mfcc) + '_standard_deviations'
            if (extractor == 4):
                temp = mfcc_averages_deviations(mfcc, label)
                feature_type = str(n_mfcc) + '_averages_' + str(n_mfcc) + '_deviations'
            #temp = temp.astype('float16') # muda para float 16 para ser mais rapido
            #print('shape temp array ', temp.shape) 
            #print('type temp ', temp.dtype)
             
            if first_loop:
                features = temp
                first_loop = False
            else:
                features = np.vstack((features, temp))

        loop += 1
        print('loop ', loop)
        #if (loop > max_loop): break
        #print('type features ', features.dtype)
        #print('features ', features)
        print('shape features ', features.shape)

    return features, feature_type
        
if __name__ == "__main__":
    main()



# funcao que pode dar jeito mas que afinal fiz para nada.
# tbh acho que esta funcao ja estava feita noutro lado qualquer

def sound_splitter(path):
    print('path ', path)
    window = 5 # tamanho da janela em s
    audio_file, sample_rate = lb.load(path, sr=None) # da o load do ficheiro audios
    print('original audio file shape ', audio_file.shape)
    audio_file = lb.resample(audio_file, orig_sr=sample_rate, target_sr=audio_sample_rate) # passa para 2016 samples por segundo # funciona
    print('down sampled audio file shape ', audio_file.shape)
    len_file = len(audio_file) # comprimento do ficheiro
    print('len audio file ', len_file)
    len_sample = int(window * audio_sample_rate) # comprimento em samples da janela
    print('len window ', len_sample)
    n_iter = int(len_file / len_sample) - 1 # numero de iteracoes que e preciso para dividir o som em janelas de 5. menos uma para ter a certeza que nao ha janelas que acabam a meio.
    print('n iter ', n_iter)
    splitted_sound = np.zeros((n_iter, len_sample), dtype=float16) # tamanho do array
    print('split sound shape ', splitted_sound.shape)

    for i in range(n_iter):
        start_sample = len_sample*i
        end_sample = len_sample*(i+1)
        temp_audio = audio_file[start_sample:end_sample]
        temp_audio = normalizer_av0_max1(temp_audio)
        splitted_sound[i, :] = temp_audio
    
    return splitted_sound # devolve o som dividido em janelas de 5s

#!/usr/bin/python3.9
# objetivo e ter um codigo que tenha como input dois np arrays um com dados outro com results
# output ter uma iamgem da rede 

import tensorflow as tf
import keras
import matplotlib.pyplot
import numpy
import sklearn
from sklearn.model_selection import train_test_split
import seaborn
import pandas
from audio_mfcc_extractor_several import main as extractor
from keras.utils.vis_utils import plot_model
from keras_visualizer import visualizer
import sys
sys.path.insert(0, '/home/parente/Documents/tese/urban_sounds/code/feature_testing/')
from audio_mfcc_extractor_several import plot_double, plt_exhibition, heat_map

def data_shower(data, results):
    len_loop = 10
    print('shape data ', data.shape)
    print('shape results ', results.shape)
    for i in range(len_loop):
        data_sample = data[i, :]
        data_sample = data_sample.reshape(len(data_sample),1)
        result_sample = results[i]
        print('shape data sample ', data_sample.shape)
        heat_map(data_sample, str(result_sample), 'feature', 'intensity')
        
def data_spliter(data, results, split):
    n_inputs = data.shape[1]
    n_cat = results.shape[1]
    #print('n cat ', n_cat)

    
    training_data, validation_data, training_results, validation_results = train_test_split(
        data,
        results,
        test_size=split,
        random_state=22
    )
    #print('shape training data ', training_data.shape)
    #print('shape validation data ', validation_data.shape)
    #print('shape training results ', training_results.shape)
    #print('shape validation results ', validation_results.shape)
    
    return training_data, validation_data, training_results, validation_results

def model_creator():


def network_train(data, results):
    
    # isto cria o modelo # para este como sao muitos dados de entrada estou a fazer um funil
    model = keras.Sequential()
    model.add(keras.layers.Input(shape=n_inputs))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.core.Activation(keras.activations.relu))
    model.add(keras.layers.core.Dense(250))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.core.Activation(keras.activations.relu))
    model.add(keras.layers.core.Dense(250))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.core.Activation(keras.activations.softmax))
    model.add(keras.layers.core.Dense(n_cat))

    # vizualizacao
    print(model.summary())
    #plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    #visualizer(model, view=True) # nao esta a funcionar...
        
    # vai compilar tudo
    tf.keras.optimizers.SGD(
        learning_rate=0.5,
        momentum=0.0,
        nesterov=True,
        name="SGD"
    )
    
    model.compile(
        optimizer='SGD',
        loss='mean_squared_error',  # categorical_crossentropy mean_squared_error
        metrics=['accuracy']
    )
    
    # da o fit
    history = model.fit(
        x=training_data,
        y=training_results,
        validation_data=(validation_data, validation_results),
        batch_size=32,
        epochs=5000,
        verbose=1
    )
    
    # summarize history for accuracy
    train_accuracy = history.history['accuracy']
    validation_accuracy = history.history['val_accuracy']
    plot_double(train_accuracy, validation_accuracy, 'Model accuracy', 'Epoch', 'Accuracy', ['train', 'test'])
    plt_exhibition()

    # summarize history for loss
    train_loss = history.history['loss']
    validation_loss = history.history['val_loss']
    plot_double(train_loss, validation_loss, 'Model loss', 'Epoch', 'Loss', ['train', 'test'])
    plt_exhibition()

    # confusion matrix training
    predictions = model.predict(training_data)
    confusion_matrix_training = sklearn.metrics.confusion_matrix(
        training_results.argmax(axis=1),
        predictions.argmax(axis=1)
    )
    print('cof matrix train ', confusion_matrix_training)
    
    heat_map(confusion_matrix_training, 'Confusion matrix training data', 'predicted label', 'expected label')
    plt_exhibition()

    # confusion matrix validation
    predictions = model.predict(validation_data)
    confusion_matrix_validation = sklearn.metrics.confusion_matrix(
        validation_results.argmax(axis=1),
        predictions.argmax(axis=1)
    )
    print('cof matrix val ', confusion_matrix_validation)
    
    heat_map(confusion_matrix_training, 'Confusion matrix validation data', 'predicted label', 'expected label')
    plt_exhibition()

def main():
    data = extractor()
    print('')
    print('neural network')
    #print('shape data ', data.shape)
    temp = data.shape[1] - 1
    results = data[: ,temp]
    #results = results.reshape((data.shape[1], 1))
    data = data[:, :temp]
    
    data_shower(data, results)

    from keras.utils.np_utils import to_categorical
    #print(results)
    results = to_categorical(results,num_classes = int(max(results)+1))

    print('shape results', results.shape)
    print(results)
    print('shape data ', data.shape)
    print(data)
    input('press enter to move')
    network_train(data, results)

if __name__ == "__main__":
    main()

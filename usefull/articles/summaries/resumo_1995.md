# Objetivos do estudo
analizar como a presenca humana altera os niveis de stress dos pinguins  
analizar quao disruptiva e essa presenca  

# Como os dados foram adquiridos
dispositivos ECG externos  
transmissores de sinal cardiaco cirurgicamente implementados  
ovo artificial com sensores de infravermelhos com 100m de fio para ligar a uma central de controlo 

# Resultados
fora feitas duas linhas base  
* pinguins sem contacto humano  
* pinguins prestes a comer  

foram comparados com dois cenarios  
* pessoa a aproximar se lentamente e a parar de 5 em 5 metros  
* pessoa a aproximar se rapidamente  

Foi reparado que mesmo normalmente o ritmo cardiaco tem picos, isto e fica baixo uns minutos sobe durante uns segundos e depois volta a descer  
Quando estao a comer ficam mais stressados  
Quando a pessoa se aproxima lentamente atinge niveis de stress comparaveis  
Quando se aproxima rapidamente o ritmo cardiaco chega a duplicar e fica assim ate a pessoa se afastar  

# Conclusao do artigo
Ha stress mas nao e particularmente relevante se as pessoas seguirem certas regras  

# Pontos a retirar
A maneira como os dados foram obtidos foi bastante introziva algo que foi ate abordado no artigo  
por ser 1995 as limitacoes tecnologicas eram consideraveis  
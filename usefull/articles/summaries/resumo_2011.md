# Motivos do estudo
Na altura do estudo os metodos que existiam para medir o batimento cardiaco de aves sao muitos deles introsovos e todos caros.  
A propria construcao dos ovos falsos para medir os batimentos e cara e no caso do uso de sensores infravermelhos so da para ovos grandes  
Esta price tag pode se tornar limitativa para certas aplicacoes e estudos  

# Objetivos do estudo
Ver se dava para reduzir o preco e tamanho da construcao de ovos falsos com metodos de gravacao de batimentos cardiacos  

# Como foi feito o estudo

Entao foram feitos 2 ovos.  

* um com sistema bluetooth
* outro com um leitor MP3

Ambos foram testados e depois analizados os seus resultados

# construcao casca ovo
Ovo plastico cmo dimensoes aprocimadamente iguais as dos ocos das aves em questao  
fura se um buraco  
as duas metades do ovo foram cobertas com um balao  
tudo  exeto o buraco foi coberto com resina para dar resistencia e depois pintado para ficar parecido aos ovos reais  
ainda tem um peso colado numa das supreficies interiores para o microfone ficar sempre virado para cima  

# construcao ovo falso bluetooth
Foi arranjado um headset bluetooth foi retirado tudo exeto a bateria e eletronica.  
O microfone original foi dessoldado e foi soldado um novo com um fio  
O som era tranmitido por bluetooth e recebidos por um recetor que estava ligado a a um pc  

# construcao ovo falso mp3
foi arranjado um leitor mp3  
foi todo desmontado  
microfone dessoldado e soldado um fio longo com um microfone decente na ponta  
microfone colado no ovo  
leitor mp3 num zip bag para estar a prova de agua  

# resultados finais
os ovos bluetooth davam maus resultados quase todos inutilizaveis  
a conexao perdia se constantemente  
os mp3 funcionaram bem  

# como os batimentos foram analisados
Os batimentos foram identificados manualmente
Foi feita uma filtragem de frequencias
tambem foi feita uma indentificacao do S1 e S2
O ritmo cardiaco era retirado de segmentos de 20 seg
a analise como normalizacoes e filtragens eram feitas por matlab
Analise automatica e mencionada mas nao entram em detalhe
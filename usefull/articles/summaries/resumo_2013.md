*porra isto vai ser longo*  
**E ainda tenho de fazer outro hoje!**  *nao fiz o outro...*

# aqui vou fazer o resumo do artigo 2013 " Heart rate responses provide an objective evaluation of human disturbance stimuli in breeding birds"

## propostas  
Os investigadorse vao usar ovos falsos para medir o batimento cardiaco em pinguins  
Suprising init  
Chegaram a conclusao que a mera prsenca de um humano no angulo de visao do bicho e o suficiente para o stressar, 
tambem fazem a conclusao interessante que um humano a aproximar se lentamente e mais stressante do que um que se aproxima a passo rapido.  
A comparacao e que um fotografo que se aproxima lentamente durante 20 min causa mais stress que um investigador que vai la pega no bicho mexe no ninho e vai embora em 10 min.  
Isto tudo foi com o objetivo de provar que a interacao humana esta a afetar o numero de breeding pairs e a viabilidade das crias  

## introducao
okay isto comeca pelo classico humanos nao fazem nada de jeito, vou saltar essas partes nos todos ja sabemos isso.  
comecam por dizer porque e que escolheram ovos falsos. Isto e porque metodos como analise de comportamento e assim nao sao exatos.  
ritmo cardiaco esta diretamente relacionado com a energia que os bichos estao a gastar  
okay entao podemos ver a energia gasta para diferentes estimulos  

## metodos usados
foi usada a tecnica do ovo  falso  
okay foram concisos no material. usaram um microfone ligado a transmissor radio UHF que ia ate uma estacao de controlo. tambem haviam camaras que transmitiam o sinal video por um cabo coaxial ate a estacao de controlo.  
credo fizeram uma analise comportamental do pinguim  
software foi matlab 6.5  
depois passavam por um transformada de Fourier isto dava origem a uns quantos picos que correspondiam aos BPMs  
dados foram validados manualmente para ver se fazem sentido. se a margem de erro for menos de 10 bpms consideravam como sendo um dado valido  
como os batimentos cardiacos estao nas baixas frequencias o microfone foi escolhido especificamente para apanhar so isso  
muito ruido era eliminado assim.  
o sinal parava assim que o contacto do ovo com o bicho parava  
havia um peso de chumbo no lado oposto do ovo para o micro tar sempre para cima  
para os batimentos fizeram uma media movel de 12s minimo de 10 batimentos cardiacos  
daquilo que estou a compreender eles conseguiam descobrir o ritmo cardiaco dos pinguins a partir desta media movel  
ritmo varia com o clima  
consideraram o ritmo cardiaco base ser quando estavam em tarefas como limpar as penas e assim nao quando estavam paradas sem fazer nada  

***

### analise estatistica esta e a parte mais importante para mim  
calculada a circular-linera correlation para possiveis efeitos   
two tailed t-test para comparacao de dados  
ANOVA para comparacao de dados  
Tukey–Kramer honestly significant difference
a data foi square-root-transformed  
a homegenaidade das covariancias foram calculadas pelo teste de Levene  
para as correlacoes foi o teste de Pearson. Foram considerados dados diferentes se P<0.05
o erro e o desvio padrao dos dados

## resultados
o ritmo cardiaco chegou a subir 287% do normal  
os ovos nao alteraram o ciclo das aves  

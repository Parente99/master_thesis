*okay vamos ver se consigo fazer este kinda que rapido*

# motivo
A reposta psicologica das aves a intreferencias como avioes e drones e pouco conhecida  

# objetivos
analisar a influencia disto nas aves
pera eles mudam a meio para algo ainda melhor **determination of instant heart rate**

# como
ovo falso com microfone  

# introducao
entao comecam logo a dizer que todos os metotdos de analise dos  dados continua a deixar muito a desejar  
normalmente a tecnica usada e ouvir e marcar manualmente os batimentos  

# audio recordings
fizeram tudo em intervalos de 15 min *nos temos 20 entao ya pretty close*  
okay a seguir a isto ainda aplicaram um filtro passa banda 50-200 Hz  
usaram uma transformada de Hilbert para determinar o envelope  
ate certo ponto conseguem identificar S1 e S2  

# signal processing methods
## tipos de analize de artigos anteriores
time-frequency analysis  
fourier and wavelet analysis  
metodos de energia  
redes neuronais  
hidden markov models  
synchro-squeezing transform  

uso de fuzzy logic para remover picos

### short-time fourier transform
filtro passa banda 50 - 100 Hz  
ver a potencia espectral no intervalo de bpms de interesse  
muito simples mas menos exato  
mais eficas com coisas como uma janela deslizante.  
neste caso so deteta bpms medios nao apanhando picos e assim  

### sliding auto correlation
filtro passa banda 50 - 200 hz  
downsample para 200Hz  
sliding-autocorrelation com 2s  
*este metodo e muito promissor*  

###  synchro-squeezed transform
e computacionalmente pesado  
bons resultados

filtro passa banda 50 - 200 hz  
downsample para 200Hz  
transformada de hilbert para calcular o envelope.  
Using a (Amor) wavelet synchro-squeezed transform with 48 voices-per-octave.  
Extracting the highest amplitude within the frequency range corresponding to the lowest and highest expected beats-per-minute. 

### peak detection of envelope signal
hum nao gosto deste  
foca se em detetar o pico S1 com base em metodos classicos

filtro passa banda 50 - 100 hz  
transformada hilbert para o envelope  
e depois descobir os picos  

# comentario
*no meio disto tudo nao falaram das redes neuronais...*
# Estado da arte

A interferência humana no meio ambiente tanto por detritos antropogénicos como por poluição atmosférica tem um impacto negativo na fauna e flora existente no ambiente. Este impacto é visível em toda a fauna dos ecossistemas, mas um grupo especialmente afetado por estas alterações são as aves.  
Desde muito cedo por causa da sua fácil observação, extensiva documentação e alta sensibilidade aos fatores ambientais que esta classe de animais são monitorizados com o objetivo de medir a qualidade do ecossistema em que se inserem.  

Os meios de monitorização são sistemas complexos que podem ser divididos em **hardware** e **software**

## Hardware

Há muitas formas de fazer a monitorização das aves mas por simplicidade irei focar-me nos seguintes métodos:  

* Observação  
* Implantes ECG  
* Ovos falsos  
* * ECG
* * Infravermelho
* * Microfone

A observação, tal como o nome indica, consiste em observar a ave e tentar inferir o seu estado físico e psicológico.  
Como é esperado este método, não sendo uma ciência exata está sujeito a erros consideráveis.[2018]  

Os implantes ECG dão dados de ritmo cardíaco que são muito mais exatos do que todos os outros [1995] e destes é fácil deduzir fatores como stress e consumo energético [2018].  
As desvantagens deste método começam com os problemas éticos de implantar um sensor dentro do corpo de um animal e também invalida alguns dados porque depois os humanos ficam associados a todo o trauma que estes animais passaram para esses dispositivos serem implantados. Dados exatos mas não fiáveis.  

A terceira opção é a mais equilibrada. Um ovo falso com um sensor de pulsação dentro deste. Este método é pouco invasivo, demorando apenas uns minutos a colocar [2011] transformando-o no método de eleição para a análise da influência humana em aves [1996].  
Dentro deste ramo dos ovos falsos existem várias maneiras de obter a pulsação da ave. Por sensores ECG [1995], infravermelhos [1996] ou com microfones dedicados [2011].  
Os sensores infravermelhos foram usados essencialmente só no início deste ramo de investigação pelas limitações que havia na tecnologia dos microfones, embora sendo estes também muito limitados. Tem de estar sempre em contacto com o corpo da ave, não podendo haver penas entre o sensor e a pele e precisam de janelas transparentes no ovo para poderem emitir e receber a luz infravermelha [1995]. Por causa destas limitações esta tecnologia só funciona em certas espécies de pinguins.  
Com os desenvolvimentos das tecnologias de som tornou-se mais exequível colocar microfones altamente sensíveis em pequenos ovos [2011] este tipo de análise é fácil rápida e robusta mas trás consigo a desvantagem de ser muito propensa a ruído. Mesmo assim tornou-se a forma mais popular pela sua baixa interferência nos hábitos das aves e fácil análise com programas como Matlab.  

O estudo desenvolvido pelos laboratórios LIP e MARE usa um ovo falso com um microfone, sistema de aquisição de dados, memória interna e baterias para gravar os sons emitidos pelas aves.

## Software

A análise dos dados de som obtidos pode ser feita de várias maneiras. A primeira e mais óbvia delas e por análise direta. Esta consiste em ouvir ou ver o ficheiro de som e registar manualmente os batimentos cardíacos. A partir daqui é possível extrapolar o ritmo cardíaco de onde se consegue retirar consumo energético e estado psicológico da ave. Por vezes consegue-se obter dois picos correspondentes a S1 e S2 com os quais dá para fazer análises muito mais detalhadas podendo se chegar a fazer a deteção de anomalias cardíacas.  
Esta maneira é demorada e cara. Isto torna-se um grande problema quando há várias centenas de horas de áudio para analisar. Por causa destes fatores há uma necessidade cada vez maior de automatizar este processo.

Podemos dividir o processo de automatização em duas fases, tratamento dos dados e análise destes. E duas famílias análise clássica e inteligente.  
A separação entre a fase de tratamento e análise é difusa e difícil de identificar.  
Neste tipo de dados é feito um tratamento estatístico extensivo para retirar o máximo de ruído possível e ficar só com o sinal.  
Para a automatização o software de eleição e o Matlab pela sua simplicidade e documentação. Um processo comum em todas as abordagens e um filtro passa banda para baixas frequências isto remove algum do ruído mais fácil de detetar.

A abordagem desenvolvida por [2013] envolveu a passagem do som por uma transformada de Fourier, fazer uma média móvel de 12 segundos e fazer uma correspondência de frequências a BPMs.  
Depois disto foram feitos múltiplos testes estatísticos para ver a qualidade das ilações e classificar em classes de dados. Entre estes estavam testes como *circular-linear correlations* para analisar a periodicidade do sinal, *two tailed t-test* para quantificar a diferença entre classes, *Tukey–Kramer honestly significant difference* para testar quão fidedignos eram os dados entre outros testes como teste de Levene e teste de Pearson.  

No artigo [2018] foram abordados múltiplos métodos. Todos começaram com uma filtragem de frequências e passagem por uma transformada de Hilbert para obter o envelope da onda sonora.  
De seguida os dados foram analisados por 4 métodos.  

* short time fourier transform
* sliding auto correlation
* synchrosqueezed transform
* peak detection of envelope signal  

Estes sistemas têm todos a desvantagem de terem margens de erro maiores que uma análise manual mas são muito mais rápidos.  

Para a análise dos dados recolhidos pelos ovos falsos do projeto LIP MARE vai ser usada uma técnica que embora não abordada nos artigos específicos de análise de batimentos cardíacos de aves já está a ser implementada na análise de batimentos cardíacos de humanos, as redes neuronais [2018_RNN]. Estas devido a sua capacidade de adaptação e aprendizagem tem a capacidade de analisar dados com maior exatidão que métodos clássicos.  
Para além disto redes neuronais como CNNs mas especialmente RNNs tem mostrado resultados promissores na área de análise de sinal e distinção de batimentos cardíacos, tornando-as na implementação lógica para aplicar no desenvolvimento desta área.
